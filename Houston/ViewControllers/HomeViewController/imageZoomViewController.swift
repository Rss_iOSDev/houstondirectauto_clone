//
//  imageZoomViewController.swift
//  Houston
//
//  Created by Reinforce on 13/09/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class imageZoomViewController: UIViewController {
    
    
    @IBOutlet weak var imgZoom: UIImageView!
    var strUrl = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        
        imgZoom.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
        imgZoom.setShowActivityIndicator(true)
        imgZoom.sd_setImage(with: URL.init(string: strUrl), placeholderImage: UIImage(named: ""))
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func clickOnBackBtn(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
   

}
