

import UIKit
import WebKit

class financeCalculatorVC: UIViewController {
    
        
    var vid_id = ""
    @IBOutlet weak var financeCalc_Webview : WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Globalfunc.showLoaderView(view: self.view)
       
       

    }
    
    
    func loadRequestUrl(){
        
        let usrStr = "https://www.houstondirectauto.com/finance-calculator/?vid=\(vid_id)"
        Globalfunc.print(object: usrStr)
        if let url = URL(string: usrStr) {
            let request = URLRequest(url: url)
            financeCalc_Webview.load(request)
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
""        
       // DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.loadRequestUrl()
       // })
        
    }
    
    
    @IBAction func clickOnBack(_ sender : UIButton){
        // self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    

    

}
