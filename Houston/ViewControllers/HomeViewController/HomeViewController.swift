//
//  HomeViewController.swift

import UIKit
import MapKit
import CoreLocation

var searchTag = ""
class HomeViewController: UIViewController , MKMapViewDelegate {

    
    @IBOutlet weak var mapView : MKMapView!
    
    var pickerviewMakes : UIPickerView!
    var pickerviewPayment : UIPickerView!
    
    @IBOutlet weak var txtSearchField: RCustomTextField!
    @IBOutlet weak var txtSearchbyMake: RCustomTextField!
    @IBOutlet weak var txtSearchByPayment: RCustomTextField!
    
    var annotationarray = [
        ["title":"Houston Direct Auto North","subtitle":"1901 Little York Houston, TX 77093","latitude":29.871850,"longitude":-95.356580],
        ["title":"Houston Direct Auto South","subtitle":"3740 Jeanetta St,Houston,Tx 77063, TX 77093","latitude":29.723940,"longitude":-95.525950]
    ]
    
    var isPickerSelected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

          let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 29.723940, longitude: -95.525950), latitudinalMeters: 25000, longitudinalMeters: 25000)
          mapView.setRegion(region, animated: true)
         self.createAnnotations(annotationarray)
          
        
        // Do any additional setup after loading the view.
    }
    
    func createAnnotations(_ locations: [[String:Any]])
    {
        for location in locations{
            
            let annotation = MKPointAnnotation()
            annotation.title = location["title"] as? String
            annotation.subtitle = location["subtitle"] as? String
            annotation.coordinate = CLLocationCoordinate2D(latitude: (location["latitude"] as? CLLocationDegrees)!, longitude: (location["longitude"] as? CLLocationDegrees)!)
            mapView.addAnnotation(annotation)

            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        searchTag = ""
        if UIApplication.isFirstLaunch() {
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                
                var postString = ""
                let email = userDefault.object(forKey: "email") as? String
                if (email == "" || email == nil){
                    postString = "username=\(Constant.username)&password=\(Constant.password)"
                }
                else{
                    let pass = userDefault.object(forKey: "password") as? String
                    postString = "username=\(email!)&password=\(pass!)"
                }
                self.callRequiredApiOnHomePAge(url: "\(Constant.Auth_Url)", postString: postString, strCheck: "token")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
            
        }else if let authToken = userDefault.value(forKey: "authToken") {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    let postString = "token=\(authToken)"
                    self.callRequiredApiOnHomePAge(url: "\(Constant.makelsList_Url)", postString: postString, strCheck: "make")
            }
        }
        else{
            let postString = "username=\(Constant.username)&password=\(Constant.password)"
            self.callRequiredApiOnHomePAge(url: "\(Constant.Auth_Url)", postString: postString, strCheck: "token")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }

    
    @IBAction func clickOnBottomTabs(_ sender : UIButton)
    {
        if(sender.tag == 10){
            
            guard let number = URL(string: "tel://(832) 263-2712") else { return }
            UIApplication.shared.open(number)

        }
        else if(sender.tag == 20){
            
            self.openGoogleMaps()
        }
        if(sender.tag == 30){

            let alert = UIAlertController(title: "Today's Hours", message: "Sales: 10:00 AM - 7:00 PM", preferredStyle: .actionSheet)
            self.present(alert, animated: true, completion: nil)

            // delays execution of code to dismiss
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
              alert.dismiss(animated: true, completion: nil)
            })
            
        }
    }
    
    func openGoogleMaps() {
        let coord = CLLocationCoordinate2D(latitude: 29.721560, longitude: -95.524840)
        let url = "http://maps.apple.com/maps?saddr=\(coord.latitude),\(coord.longitude)"
        UIApplication.shared.openURL(URL(string:url)!)
    }
   
    @IBAction func clickOnNearME(_ sender : UIButton){
        let nearme = self.storyboard?.instantiateViewController(withIdentifier: "nearMeViewController") as! nearMeViewController
        self.navigationController?.pushViewController(nearme, animated: true)
    }
    
    
    @IBAction func clickOnSearchAllInventoryBtn(_ sender : UIButton){
        searchTag = "3"
        self.tabBarController?.selectedIndex = 2
    }
    

    //Func to call Apis
    func callRequiredApiOnHomePAge(url:String, postString:String, strCheck:String){
        Globalfunc.hideLoaderView(view: self.view)
        webServices.callApiRequestforPost(postString, url: url) { (dict, error) in
            if(error == ""){
                
                if(strCheck == "token"){
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    if(status == "success"){
                        OperationQueue.main.addOperation {
                            let authToke = dict["token"] as! String
                            userDefault.set(authToke, forKey: "authToken")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                self.callRequiredApiOnHomePAge(url: "\(Constant.makelsList_Url)", postString: "token=\(authToke)", strCheck: "make")
                            }
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            let msg = dict["msg"] as! String
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                        }
                    }
                }
                    
                else if(strCheck == "make"){
                    let status = dict["status"] as! String
                    if(status == "success"){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Constant.pickerMakeList = dict["makes"] as! [String]
                            let authToke = dict["token"] as! String
                            userDefault.set(authToke, forKey: "authToken")
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            
                            let msg = dict["msg"] as! String
                            Globalfunc.print(object: msg)
                            if(msg == "Invalid or expired authentication token"){
                               
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                        UserDefaults.standard.set(false, forKey: "HasLaunchedOnce")
                                        userDefault.removeObject(forKey: "authToken")
                                        self.viewWillAppear(true)
                                    
                                    },{action2 in
                                    }, nil])
                                
                            }else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                           
                        }
                    }
                }
                
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
}

//Mark - Drop down
extension HomeViewController : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerviewMakes){
            return Constant.pickerMakeList.count
        }
        else if(pickerView == pickerviewPayment ){
            return Constant.pickerPaymentList.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerviewMakes){
            return Constant.pickerMakeList[row]
        }
        else if(pickerView == pickerviewPayment ){
            return Constant.pickerPaymentList[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerviewMakes){
            self.txtSearchbyMake.text = Constant.pickerMakeList[row]
        }
        else if(pickerView == pickerviewPayment ){
            self.txtSearchByPayment.text = Constant.pickerPaymentList[row]
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtSearchField){
            textField.returnKeyType = .search
        }
        else if(textField == self.txtSearchbyMake){
            self.pickUp(txtSearchbyMake)
        }
        else if(textField == self.txtSearchByPayment){
            self.pickUp(txtSearchByPayment)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txtSearchField){
            Globalfunc.print(object: "search")
            self.txtSearchField.resignFirstResponder()
             searchBarText = self.txtSearchField.text!
             searchTag = "0"
             self.tabBarController?.selectedIndex = 2
            txtSearchField.text = ""
            return true
        }
        return false
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtSearchbyMake){
            self.pickerviewMakes = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewMakes.delegate = self
            self.pickerviewMakes.dataSource = self
            self.pickerviewMakes.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewMakes
            isPickerSelected = "Make"
        }
        else if(textField == self.txtSearchByPayment){
            self.pickerviewPayment = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewPayment.delegate = self
            self.pickerviewPayment.dataSource = self
            self.pickerviewPayment.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewPayment
            isPickerSelected = "Payment"
        }
       
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(registrationViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(registrationViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        if(isPickerSelected == "Payment"){
            txtSearchByPayment.resignFirstResponder()
            let myStringArr = txtSearchByPayment.text?.components(separatedBy: "/")
            var str = myStringArr![0]
            str.remove(at: (str.startIndex))
            searchPaymentText = str
            searchTag = "2"
            self.tabBarController?.selectedIndex = 2
            self.txtSearchByPayment.text = ""
        }
        else if(isPickerSelected == "Make"){
            txtSearchbyMake.resignFirstResponder()
            makeString = "\(self.txtSearchbyMake.text!)"
            searchTag = "1"
            self.tabBarController?.selectedIndex = 2
            self.txtSearchbyMake.text = ""
        }
    }
    
    @objc func cancelClick() {
        txtSearchByPayment.text = ""
        txtSearchbyMake.text = ""
        txtSearchByPayment.resignFirstResponder()
        txtSearchbyMake.resignFirstResponder()
    }
    
}

extension HomeViewController {
    
    @IBAction func clickOnBtnHoustnNorth(_ sender: UIButton)
    {
        searchTag = "7"
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func clickOnBtnHoustn(_ sender: UIButton)
    {
        searchTag = "8"
        self.tabBarController?.selectedIndex = 2
    }

}
