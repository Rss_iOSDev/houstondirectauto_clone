
//
//  VehicleDetailViewController.swift
//  Houston
//
//  Created by Reinforce on 13/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit
import ImageSlideshow
import CoreLocation

class VehicleDetailViewController: UIViewController {
    
    @IBOutlet weak var tblVehDetail : UITableView!
    @IBOutlet weak var tblContentView: UIView!

        
    @IBOutlet weak var btnOverview: UIButton!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var btnEquipment: UIButton!
    @IBOutlet weak var lblEqupmnt: UILabel!
    
    @IBOutlet weak var lblViewEquipment: UILabel!
    @IBOutlet weak var viewOverview: UIView!
    
    
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblMake: UILabel!
    
    @IBOutlet weak var lblTrim: UILabel!
    
    @IBOutlet weak var lblTimeUntilReady: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var lblMiles: UILabel!
    
    @IBOutlet weak var imgCarFax : UIImageView!
    @IBOutlet weak var imgPendingSale : UIImageView!
    
    @IBOutlet weak var lblDescriptionOverview: UILabel!
    
    @IBOutlet weak var viewVehiclePopUp : UIView!
    @IBOutlet weak var lblAddress: UILabel!
    
    
    var vIn : String = ""
    var vID = ""
    var photo_count = ""
    
     
    @IBOutlet var slideshow: ImageSlideshow!
    
    
    //Overview outlets
    @IBOutlet weak var lblOverviewModel: UILabel!
    @IBOutlet weak var lblOverviewTrim: UILabel!
    @IBOutlet weak var lblOverviewBody: UILabel!
    @IBOutlet weak var lblOverviewExt_C: UILabel!
    @IBOutlet weak var lblOverviewInt_C: UILabel!
    @IBOutlet weak var lblOverviewDoor: UILabel!
    @IBOutlet weak var lblOverviewStock: UILabel!
    @IBOutlet weak var lblOverviewModel_h: UILabel!
    @IBOutlet weak var lblOverviewVin_h: UILabel!
    @IBOutlet weak var lblOverviewMilege: UILabel!
    @IBOutlet weak var lblOverviewEngine: UILabel!
    @IBOutlet weak var lblOverviewTransmission: UILabel!
    @IBOutlet weak var lblOverviewFuel: UILabel!
    @IBOutlet weak var lblOverviewDrive: UILabel!
    @IBOutlet weak var lblOverviewMpg: UILabel!
    @IBOutlet weak var lblOverviewUphoistery: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblViewEquipment.isHidden = true
        self.viewOverview.isHidden = false
        self.imgPendingSale.isHidden = true
        // Do any additional setup after loading the view.
        
        self.viewVehiclePopUp.alpha = 1.0
        
        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { timer in
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.viewVehiclePopUp.alpha = 0.0
            }) { (_) in
                timer.invalidate()
            }
        }
        
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))
        self.lblAddress.isUserInteractionEnabled = true
        self.lblAddress.addGestureRecognizer(labelTap)


    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.callVehicleDetailApi()
        self.tblVehDetail.isHidden = true
        
        
        slideshow.slideshowInterval = 3.0
        slideshow.contentScaleMode = .scaleAspectFill
        slideshow.activityIndicator = DefaultActivityIndicator()
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.clear
        pageControl.pageIndicatorTintColor = UIColor.clear
        slideshow.pageIndicator = pageControl
    
       // slideshow.delegate = self
        
    
    }
    
    
    @IBAction func clickOnHeaderBtn(_ sender : UIButton){
        
        if(sender.tag == 10){
            let test = self.storyboard?.instantiateViewController(withIdentifier: "testDriveViewController") as! testDriveViewController
            self.navigationController?.pushViewController(test, animated: true)
        }
        else if(sender.tag == 20){
            let form = self.storyboard?.instantiateViewController(withIdentifier: "formViewController") as! formViewController
            form.isFormType = "availability"
            self.navigationController?.pushViewController(form, animated: true)
            //self.present(form, animated: false, completion: nil)
        }
        else if(sender.tag == 30){
            let form = self.storyboard?.instantiateViewController(withIdentifier: "formViewController") as! formViewController
            form.isFormType = "offer"
            self.navigationController?.pushViewController(form, animated: true)
        }
        
        else if(sender.tag == 40){
            let finance = self.storyboard?.instantiateViewController(withIdentifier: "financeCalculatorVC") as! financeCalculatorVC
            finance.vid_id = vID
            self.navigationController?.pushViewController(finance, animated: true)
        }
    }
    
    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickOnTabs(_ sender : UIButton){
        
        if(sender.tag == 1){
            btnOverview.setTitleColor(UIColor.black, for: UIControl.State.normal)
            btnEquipment.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
            
            lblOverview.backgroundColor = UIColor.black
            lblEqupmnt.backgroundColor = UIColor.lightGray
            
            self.lblViewEquipment.isHidden = true
            self.viewOverview.isHidden = false
            
           let height = Globalfunc.heightForLabel(text: lblDescriptionOverview.text!, font: lblDescriptionOverview.font, width: lblDescriptionOverview.frame.width)
            self.setHeightOfTblView(height: height, btnType: "1")
            
            
        }
        if(sender.tag == 2){
            
            btnEquipment.setTitleColor(UIColor.black, for: UIControl.State.normal)
            btnOverview.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
            
            lblOverview.backgroundColor = UIColor.lightGray
            lblEqupmnt.backgroundColor = UIColor.black
            
            self.lblViewEquipment.isHidden = false
            self.viewOverview.isHidden = true
            
            let height = lblViewEquipment.attributedText?.height(withConstrainedWidth: lblViewEquipment.frame.width)
            self.setHeightOfTblView(height: height!, btnType: "2")
            
           // self.viewContentHeight.constant = 1500
            
            
            
        }
        
    }

    func callVehicleDetailApi(){
        
        Globalfunc.showLoaderView(view: self.view)
        
        if let authToken = userDefault.value(forKey: "authToken") {
            
            let url = "\(Constant.vehicle_details_Url)?token=\(authToken)&vin=\(vIn)"
            
            Globalfunc.print(object: url)
            webServices.callApiRequestForGet(url: url) { (dict, error) in
            Globalfunc.print(object: dict)
                
                if(error == ""){
                    let status = dict["status"] as! String
                    if(status == "success"){
                        
                        OperationQueue.main.addOperation {
                            self.tblVehDetail.isHidden = false
                            let authToke = dict["token"] as! String
                            userDefault.set(authToke, forKey: "authToken")
                            Globalfunc.hideLoaderView(view: self.view)
                            let vehdict = dict["veh"] as! NSDictionary
                            self.setDataOnPage(dict: vehdict)
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    
                                    },{action2 in
                                    }, nil])
                            }else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                                self.tblVehDetail.isHidden = true
                            }
                        }
                    }
                }
                else{
                }
            }
        }
    }

    @objc func imageTapped(_ tapGestureRecognizer: UITapGestureRecognizer)
    {
        let urlCarfax = "https://www.carfax.com/cfm/ccc_DisplayHistoryRpt.cfm?partner=WDB_0&vin=\(vIn)"
        guard let url = URL(string: urlCarfax) else { return }
        UIApplication.shared.open(url)
                
    }
   
    func setDataOnPage(dict: NSDictionary){
        
        if let year = dict["year"] as? String{
            self.lblYear.text = year                
        }
        
        if let make = dict["make"] as? String{
            self.lblMake.text = make
        }
        
        if let model = dict["model"] as? String{
            self.lblModel.text = model
        }
        
        if let series = dict["series"] as? String{
            self.lblTrim.text = series
        }
        
        if let odometer = dict["odometer"] as? String{
            self.lblMiles.text = odometer.milesFormatting()
        }
        
        var arrIn = [InputSource]()
        if let photos_count = dict["photos_count"] as? String{
            if(photos_count == "1"){
                photo_count = photos_count
                let vin = dict["vin"] as! String
                let imgUrl = "https://hda.s3.us-east-2.amazonaws.com/\(vin.lowercased())/\(1)_245x183.jpg"
                arrIn.append(SDWebImageSource(urlString: imgUrl)!)
            }
            else{
                for i in 1...Int(photos_count)! - 1{
                    
                    let vin = dict["vin"] as! String
                    let imgUrl = "https://hda.s3.us-east-2.amazonaws.com/\(vin.lowercased())/\(i)_245x183.jpg"
                    arrIn.append(SDWebImageSource(urlString: imgUrl)!)
                }
            }
            slideshow.setImageInputs(arrIn)
            
            
       }
        var refurb_status = ""
        var preclose_status = ""
        
        if let preclose = dict["preclose_status"] as? String{
            preclose_status = preclose
        }
        
        if let refurb = dict["refurb_status"] as? String{
            refurb_status = refurb
        }
        
        if(preclose_status == "deposit" || refurb_status == "rental"){
            self.imgPendingSale.isHidden = false
        }
            
        else{
            self.imgPendingSale.isHidden = true
        }
        
        if let description = dict["description"] as? String{
            self.lblDescriptionOverview.text = description
        }
        
        if let features = dict["features"] as? String{
            let arrFeatures = features.components(separatedBy: ",")
            self.lblViewEquipment.attributedText = Globalfunc.add(stringList: arrFeatures, font: lblViewEquipment.font, bullet: "•")
        }
        
        if let model = dict["model"] as? String{
            self.lblOverviewModel.text = model
        }
        
        if let series = dict["series"] as? String{
            self.lblOverviewTrim.text = series
        }
        
        if let body = dict["body"] as? String{
            self.lblOverviewBody.text = body
        }
        
        if let colour = dict["colour"] as? String{
            self.lblOverviewExt_C.text = colour
        }
        
        if let interior_color = dict["interior_color"] as? String{
            self.lblOverviewInt_C.text = interior_color
        }
        
        if let body_door_ct = dict["body_door_ct"] as? String{
            self.lblOverviewDoor.text = body_door_ct
        }
        
        if let model_number = dict["model_number"] as? String{
            self.lblOverviewModel_h.text = model_number
        }
        
        if let vin = dict["vin"] as? String{
            self.lblOverviewVin_h.text = vin
        }
        
        if let odometer = dict["odometer"] as? String{
            self.lblOverviewMilege.text = odometer.milesFormatting()
        }
        
        if let engine = dict["engine"] as? String{
            self.lblOverviewEngine.text = engine
        }
        
        if let transmission = dict["transmission"] as? String{
            self.lblOverviewTransmission.text = transmission
        }
        
        if let fuel = dict["fuel"] as? String{
            self.lblOverviewFuel.text = fuel
        }
        
        if let price = dict["price"] as? String{
            
            self.lblPrice.text = price.currencyFormatting()
        }
        
        if let stock = dict["stock"] as? String{
            self.lblOverviewStock.text = "\(stock)"
        }

        if let drivetrain_desc = dict["drivetrain_desc"] as? String{
            self.lblOverviewDrive.text = "\(drivetrain_desc)"
        }
        
        if let interior_material = dict["interior_material"] as? String{
            self.lblOverviewUphoistery.text = "\(interior_material)"
        }
        let city_mpg = "\(dict["city_mpg"] as! String) City"
        let highway_mpg = "\(dict["highway_mpg"] as! String) Highway"
        lblOverviewMpg.text = "\(city_mpg)/ \(highway_mpg)"
        
        let height = Globalfunc.heightForLabel(text: lblDescriptionOverview.text!, font: lblDescriptionOverview.font, width: lblDescriptionOverview.frame.width)
        self.setHeightOfTblView(height: height, btnType: "1")
        
        if let carfax_one_owner = dict["carfax_one_owner"] as? String{
            if(carfax_one_owner == "1"){
                imgCarFax.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                imgCarFax.setShowActivityIndicator(true)
                imgCarFax.sd_setImage(with: URL.init(string: "https:houstondirectauto.com/wp-content/themes/houstondirectauto/assets/images/carfax_1owner.jpg"), placeholderImage: UIImage(named: ""))
            }
            else{
                imgCarFax.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                imgCarFax.setShowActivityIndicator(true)
                imgCarFax.sd_setImage(with: URL.init(string: "https://s3.us-east-2.amazonaws.com/hda/assets/carfax-free.jpg"), placeholderImage: UIImage(named: ""))
            }
        }
        else{
            imgCarFax.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
            imgCarFax.setShowActivityIndicator(true)
            imgCarFax.sd_setImage(with: URL.init(string: "https://s3.us-east-2.amazonaws.com/hda/assets/carfax-free.jpg"), placeholderImage: UIImage(named: ""))

        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        imgCarFax.isUserInteractionEnabled = true
        imgCarFax.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func setHeightOfTblView(height : CGFloat ,btnType : String){
        
        if(btnType == "1"){
            var frame = self.tblContentView.frame
            frame.size.height =  height + 510 + self.viewOverview.frame.height
            self.tblContentView.frame = frame
            self.tblVehDetail.reloadData()
            
        }
        else if(btnType == "2"){
            var frame = self.tblContentView.frame
            frame.size.height = 510 + height
            self.tblContentView.frame = frame
            self.tblVehDetail.reloadData()
        }
    }
    
    @IBAction func clickOnImgZoomBtn(_ sender: UIButton){
        if(photo_count == "1"){
            let imgUrl = "https://hda.s3.us-east-2.amazonaws.com/\(vIn.lowercased())/\(1)_245x183.jpg"
            let zoom = self.storyboard!.instantiateViewController(withIdentifier: "imageZoomViewController") as! imageZoomViewController
            zoom.strUrl = imgUrl
            zoom.modalPresentationStyle = .overCurrentContext
            present(zoom, animated: true, completion: nil)
        }
        else{
            let imgUrl = "https://hda.s3.us-east-2.amazonaws.com/\(vIn.lowercased())/\(slideshow.currentPage+1)_245x183.jpg"
            let zoom = self.storyboard!.instantiateViewController(withIdentifier: "imageZoomViewController") as! imageZoomViewController
            zoom.strUrl = imgUrl
            zoom.modalPresentationStyle = .overCurrentContext
            present(zoom, animated: true, completion: nil)
        }
    }
}

extension VehicleDetailViewController {
    
    @IBAction func clickOnCloseViewBtn(_ sender : UIButton){
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.viewVehiclePopUp.alpha = 0.0
        }, completion: nil)
    }

    
    @objc func labelTapped(_ sender: UITapGestureRecognizer) {
        let coord = CLLocationCoordinate2D(latitude: 29.721560, longitude: -95.524840)
        let url = "http://maps.apple.com/maps?saddr=\(coord.latitude),\(coord.longitude)"
        UIApplication.shared.openURL(URL(string:url)!)

    }


}
