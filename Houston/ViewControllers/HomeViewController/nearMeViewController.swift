//
//  nearMeViewController.swift
//  Houston
//
//  Created by Reinforce on 12/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit
import CoreLocation
import AMPopTip
//import RealmSwift

class nearMeViewController: UIViewController , CLLocationManagerDelegate{

    
    @IBOutlet weak var tblList : UITableView!
    @IBOutlet weak var lblStatus : UILabel!
    
        
    var popTip = PopTip()
    
    
    var locationManager:CLLocationManager!
    
   // var currentItem:FavouriteListRealm?
    //let realm = try? Realm()
    
    var arrVehicles_Data : [NSDictionary] = []
    
    var arrFavSelectItem = [String]()
    
    //fav view
    @IBOutlet weak var viewAddFav : UIView!
    @IBOutlet weak var txtEmailFav : UITextField!
    @IBOutlet weak var txtPhoneFav : UITextField!
    @IBOutlet weak var btnSaveFav : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        tblList.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
""        
//        let favArr = userDefault.stringArray(forKey: "favArr") ?? [String]()
//        if(!favArr.isEmpty){
//            arrFavSelectItem = favArr
//        }
//        else{
//            arrFavSelectItem = []
//        }
        self.viewAddFav.isHidden = true
        let email = userDefault.value(forKey: "email_fav") as? String
        if(email == nil || email == ""){
            
        }else{
           self.callFavListApi(strEmail: email!)
        }
        
        self.determineMyCurrentLocation()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
   
    
    @IBAction func clickOnBack(_ sender : UIButton){
       // self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnCancelBtn(_ sender : UIButton){
        self.viewAddFav.isHidden = true
    }
    
    
    func callFavListApi(strEmail : String){
        
        if let authToken = userDefault.value(forKey: "authToken") {
               let postString = "\(Constant.favorite_List_Url)?token=\(authToken)&email=\(strEmail)"
                
                Globalfunc.print(object: postString)
                webServices.callApiRequestForGet(url: postString) { (dict, error) in
                    if(error == ""){
                        Globalfunc.print(object: dict)
                        OperationQueue.main.addOperation {
                            let arr = dict["fav_list"] as! [NSDictionary]
                            if arr.count > 0{
                                for i in 0...arr.count - 1 {
                                    let dict = arr[i]
                                    
                                    if let vin_no = dict["vin"] as? String{
                                        if(self.arrFavSelectItem.contains(vin_no) == false){
                                            self.arrFavSelectItem.append(vin_no)
                                        }
                                    }
                                }
                            }
                            else{
                                self.arrFavSelectItem = []
                            }
                        }
                    }
                }
            }
    }
    
    func determineMyCurrentLocation() {
        
        Globalfunc.showLoaderView(view: self.view)
        self.arrVehicles_Data = []
        self.tblList.isHidden = true
        self.lblStatus.isHidden = true
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
            let userLocation:CLLocation = locations[0] as CLLocation
            Globalfunc.print(object: userLocation.coordinate.latitude)
            Globalfunc.print(object: userLocation.coordinate.longitude)
           // self.callNearMEApi(strLat: userLocation.coordinate.latitude, strLong: userLocation.coordinate.longitude)
            self.callNearMEApi(strLat: 29.72401, strLong: -95.52659)
        
        
        
    }
    
    @IBAction func clickOnRefreshLocationBtn(_ sender: UIButton){
        self.determineMyCurrentLocation()
    }
    
    
    
    
    
    func callNearMEApi(strLat: Double, strLong: Double){
        
        self.locationManager.stopUpdatingLocation()
        
        let url =  "\(Constant.veh_near_me_Url)"
        if let authToken = userDefault.value(forKey: "authToken") {
                let postString  = "token=\(authToken)&latitude=\(strLat)&longitude=\(strLong)"
                Globalfunc.print(object: postString)
            
                webServices.callApiRequestforPost(postString, url: url) { (dict, error) in
                if(error == ""){
                    Globalfunc.print(object: dict)
                    
                    let status = dict["status"] as! String
                    if(status == "success"){
                        
                        OperationQueue.main.addOperation {
                            
                            Globalfunc.hideLoaderView(view: self.view)
                            let authToke = dict["token"] as! String
                            userDefault.set(authToke, forKey: "authToken")
                            self.arrVehicles_Data = dict["vehs"] as! [NSDictionary]
                            
                            Globalfunc.print(object: self.arrVehicles_Data.count)
                            
                            if(self.arrVehicles_Data.count == 0){
                                self.tblList.isHidden = true
                                self.lblStatus.isHidden = false
                                
                            }else{
                                self.tblList.isHidden = false
                                self.lblStatus.isHidden = true
                                self.tblList.reloadData()
                                
                                if(searchIndex != nil){
                                    let indexPath = NSIndexPath(row: 0, section: searchIndex)
                                    self.tblList.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                                    searchIndex = nil
                                }
                            }
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            Globalfunc.print(object: msg)
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    },{action2 in
                                    }, nil])
                            }else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }

                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
    }

}

extension nearMeViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrVehicles_Data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblList.dequeueReusableCell(withIdentifier: "tblNearListCell") as! tblNearListCell
        
        let dict = self.arrVehicles_Data[indexPath.section]
        
        
        
        cell.lblYear.text = "\(dict["year"] as! String) \(dict["make"] as! String) \(dict["model"] as! String)"
        cell.lblVinNo.text = (dict["vin"] as! String)
        cell.lblStock.text = "Stock: #\((dict["stock"] as! String))"
        cell.lblTrim.text = "Trim: \(dict["series"] as! String)"
        
        let strMiles = (dict["odometer"] as! String)
        cell.lblMiles.text = "Miles: \(strMiles.milesFormatting())"
        
        let PriceStr = (dict["price"] as! String)
        cell.lblPrice.text = "Price: \(PriceStr.currencyFormatting())"
        
        cell.imgVehicles.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
        cell.imgVehicles.setShowActivityIndicator(true)
        let vin = dict["vin"] as! String
        cell.imgVehicles.sd_setImage(with: URL.init(string: "https://hda.s3.us-east-2.amazonaws.com/\(vin.lowercased())/1_245x183.jpg"), placeholderImage: UIImage(named: ""))
        
        //cell.btnFavriote.tag = indexPath.section
        cell.btnFavriote.addTarget(self, action: #selector(clickOnFavBtn(_:)), for: UIControl.Event.touchUpInside)
        
        if(arrFavSelectItem.count > 0){
            let vin_no = dict["vin"] as! String
            if(arrFavSelectItem.contains(vin_no) == false){
                cell.btnFavriote.isChecked = false
            }else{
                cell.btnFavriote.isChecked = true
            }
        }
        else{
            cell.btnFavriote.isChecked = false
        }
        
        if let vrank = dict["vrank"] as? String{
                 let vrank_of = dict["vrank_of"] as! String
                 
                 Globalfunc.print(object: "vrank_of----\(vrank_of)")
                 
                 let vrank_cal = Int(vrank)! * 100 / Int(vrank_of)!
                 if(vrank == "1"){
                     cell.dealViewHeight.constant = 35
                     cell.lblDEal.text = "GREAT DEAL"
                     cell.lblDEal.textColor = UIColor.init(red: 6/255, green: 255/255, blue: 2/255, alpha: 1)
                     cell.imgDealArrow.image = UIImage(named: "great")
                     
                     // let popTip = PopTip()
                     // popTip.show(text: "FIND A BETTER PRICE IN TOWN FROM A COMPETITOR AND WE WILL BEAT IT.", direction: .up, maxWidth: 230, in: cell.contentView, from: cell.btnTap.frame)
                     cell.btnTap.isEnabled = true
                     cell.btnTap.tag = indexPath.row
                     cell.btnTap.addTarget(self, action: #selector(clickOn(_:)), for: UIControl.Event.touchUpInside)
                 }
                 else if(vrank != "0" && vrank_of != "0" &&  vrank_cal <= 15){
                     cell.dealViewHeight.constant = 35
                     cell.lblDEal.text = "GOOD DEAL"
                     cell.lblDEal.textColor = UIColor.init(red: 2/255, green: 144/255, blue: 1/255, alpha: 1)
                     cell.imgDealArrow.image = UIImage(named: "good")
                     cell.btnTap.isEnabled = false
                 }
                 else{
                     cell.dealViewHeight.constant = 0
                     cell.lblDEal.text = ""
                     cell.imgDealArrow.image = nil
                     cell.btnTap.isEnabled = false
                 }
                 /// set condition if vrank is more than 0 what should here come
             }
             else{
                 cell.dealViewHeight.constant = 0
                 cell.lblDEal.text = ""
                 cell.imgDealArrow.image = nil
                 cell.btnTap.isEnabled = false
             }
        
        let carfax_one_owner = dict["carfax_one_owner"] as! String
        if(carfax_one_owner == "1"){
                cell.imgCarFax.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                cell.imgCarFax.setShowActivityIndicator(true)
                cell.imgCarFax.sd_setImage(with: URL.init(string: "https:houstondirectauto.com/wp-content/themes/houstondirectauto/assets/images/carfax_1owner.jpg"), placeholderImage: UIImage(named: ""))
        }
        else{
                cell.imgCarFax.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                cell.imgCarFax.setShowActivityIndicator(true)
                cell.imgCarFax.sd_setImage(with: URL.init(string: "https://s3.us-east-2.amazonaws.com/hda/assets/carfax-free.jpg"), placeholderImage: UIImage(named: ""))
        }
        
        cell.imgCarFax.tag = indexPath.row
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        cell.imgCarFax.isUserInteractionEnabled = true
        cell.imgCarFax.addGestureRecognizer(tapGestureRecognizer)
        
        return cell
    }
    
    @objc func clickOn(_ sender: UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblList)
        let indexPath = self.tblList.indexPathForRow(at: buttonPosition)
        let currentCell = tblList.cellForRow(at: indexPath!)! as! tblNearListCell
        
        popTip.bubbleColor = UIColor.black
        // popTip.show(text: "FIND A BETTER PRICE IN TOWN FROM A COMPETITOR AND WE WILL BEAT IT.", direction: .none, maxWidth: 280, in: currentCell.viewBg, from: sender.frame)
        
        let cusView = UIView(frame: CGRect(x: 8, y: 150, width: currentCell.btnTap.frame.width, height: 55))
        cusView.backgroundColor = UIColor.black
        let lbl = UILabel(frame: CGRect(x: 2, y: 2, width: cusView.frame.width, height: cusView.frame.height))
        lbl.text = "FIND A BETTER PRICE IN TOWN FROM A COMPETITOR AND WE WILL BEAT IT."
        lbl.textColor = UIColor.white
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        cusView.addSubview(lbl)
        popTip.show(customView: cusView, direction: .down, in: currentCell.viewBg, from: sender.frame,duration: 2)
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer)
    {
        let imgView = sender.view as! UIImageView
        let dict = self.arrVehicles_Data[imgView.tag]
        let vin = dict["vin"] as! String
        let urlCarfax = "https://www.carfax.com/cfm/ccc_DisplayHistoryRpt.cfm?partner=WDB_0&vin=\(vin)"
        guard let url = URL(string: urlCarfax) else { return }
        UIApplication.shared.open(url)

    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.arrVehicles_Data[indexPath.section]
        searchIndex = indexPath.section
        
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "VehicleDetailViewController") as! VehicleDetailViewController
        detail.vIn = dict["vin"] as! String
        detail.vID = dict["vid"] as! String
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    @objc func clickOnFavBtn(_ sender : UIButton){
        //maintain session here
        let session = userDefault.object(forKey: "session") as? String
        if(session == "" || session == nil){
            
            let savedEmail = userDefault.value(forKey: "email_fav") as? String
            if(savedEmail == "" || savedEmail == nil){
                
                self.viewAddFav.isHidden = false
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tblList)
                let indexPath = self.tblList.indexPathForRow(at: buttonPosition)
                btnSaveFav.tag = indexPath!.section
            }
            else{
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tblList)
                let indexPath = self.tblList.indexPathForRow(at: buttonPosition)
                let dict = self.arrVehicles_Data[indexPath!.section]
                let vin = dict["vin"] as! String
                Globalfunc.print(object: vin)
                
                //                if (sender.image(for: .normal) == UIImage.init(named: "starfill")){
                //                    Globalfunc.print(object: "remove")
                //                }
                //                else{
                //                    Globalfunc.print(object: "add")
                //                }
                if(self.arrFavSelectItem.contains(vin) == false){
                    if let authToken = userDefault.value(forKey: "authToken") {
                        let email = userDefault.value(forKey: "email_fav") as! String
                        let phone = userDefault.value(forKey: "phone_fav") as! String
                        
                        var postString = ""
                        if let device_token = userDefault.value(forKey: "device_token") as? String{
                            postString  = "\(Constant.Add_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token=\(device_token)"
                        }
                        else{
                            postString = "\(Constant.Add_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token="
                        }
                        //   userDefault.value(forKey: "device_token") as! String
                        
                        self.callAddFavListApi(postString: postString)
                    }
                }
                else{
                    Globalfunc.print(object: "remove")
                    if let authToken = userDefault.value(forKey: "authToken") {
                        let email = userDefault.value(forKey: "email_fav") as! String
                        let phone = userDefault.value(forKey: "phone_fav") as! String
                        var postString = ""
                        if let device_token = userDefault.value(forKey: "device_token") as? String{
                            postString = "\(Constant.Remove_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token=\(device_token)"
                        }     //userDefault.value(forKey: "device_token") as! String
                        else{
                            postString = "\(Constant.Remove_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token="
                        }
                        Globalfunc.print(object: postString)
                        self.callFavListRemoveApi(postString: postString, vin_no: vin)
                    }
                }
            }
        }
    }
    
    @IBAction func clickOnSavebtn(sender: UIButton){
        //let buttonPosition = sender.convert(CGPoint.zero, to: self.tblSearchList)
        //let indexPath = self.tblSearchList.indexPathForRow(at: buttonPosition)
        let dict = self.arrVehicles_Data[btnSaveFav.tag]
        Globalfunc.print(object: dict)
        let vin = dict["vin"] as! String
        
        
        
        if(self.arrFavSelectItem.contains(vin) == false){
            if let authToken = userDefault.value(forKey: "authToken") {
                
                
                var postString = ""
                if let device_token = userDefault.value(forKey: "device_token") as? String{
                    postString  = "\(Constant.Add_favorite_Url)?token=\(authToken)&email=\(txtEmailFav.text!)&vin=\(vin)&phone=\(txtPhoneFav.text!)&ios_device_token=\(device_token)"
                }
                else{
                    postString = "\(Constant.Add_favorite_Url)?token=\(authToken)&email=\(txtEmailFav.text!)&vin=\(vin)&phone=\(txtPhoneFav.text!)&ios_device_token="
                }
                //"6e134fad148d35d03330ccd08f4ee8bd5d2a73189ae41889907a4c1430774bd8"   //userDefault.value(forKey: "device_token") as! String
                self.callAddFavListApi(postString: postString)
            }
        }
    }
    
    func callAddFavListApi(postString: String){
        
        webServices.callApiRequestForGet(url: postString) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object: dict)
                OperationQueue.main.addOperation {
                    
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "tblNearListCell") as! tblNearListCell
                    let arr = dict["fav_list"] as! [NSDictionary]
                    Globalfunc.print(object: arr)
                    self.viewAddFav.isHidden = true
                    
                    if(arr.count > 0){
                        for i in 0...arr.count - 1 {
                            let dict = arr[i]
                            let vin_no = dict["vin"] as! String
                            cell.btnFavriote.isChecked = true
                            self.arrFavSelectItem.append(vin_no)
                            userDefault.set(self.arrFavSelectItem, forKey: "favArr")
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Vehicle has been added to your favorite list successfully!")
                        }
                        //userDefault.set("session_fav", forKey: "session_fav")
                        
                        if(self.txtEmailFav.text != ""){
                            userDefault.set(self.txtEmailFav.text!, forKey: "email_fav")
                            userDefault.set(self.txtPhoneFav.text!, forKey: "phone_fav")
                        }
                        
                        
                    }
                    
                    Globalfunc.print(object: userDefault.value(forKey: "email_fav") as! String)
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
    
    func callFavListRemoveApi(postString: String , vin_no: String){
        
        webServices.callApiRequestForGet(url: postString) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object: dict)
                OperationQueue.main.addOperation {
                    
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "tblNearListCell") as! tblNearListCell
                    self.viewAddFav.isHidden = true
                    cell.btnFavriote.isChecked = false
                    
                    
                    let arr = dict["fav_list"] as! [NSDictionary]
                    Globalfunc.print(object: arr)
                    if(arr.count == 0){
                        self.arrFavSelectItem = []
                    }
                    else{
                        if(!self.arrFavSelectItem.isEmpty){
                            
                            for i in self.arrFavSelectItem{
                                if(i == vin_no){
                                    self.arrFavSelectItem = self.arrFavSelectItem.filter{$0 != i}
                                }
                            }
                            userDefault.set(self.arrFavSelectItem, forKey: "favArr")
                        }
                    }
                    self.tblList.reloadData()
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Vehicle has been removed from your favorite list successfully!")
                    
                    
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
}

class tblNearListCell: UITableViewCell {
    
    @IBOutlet weak var btnFavriote : CheckBoxButton!
    @IBOutlet weak var imgVehicles: UIImageView!
    @IBOutlet weak var imgCarFax: UIImageView!
    @IBOutlet weak var lblVinNo: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTrim: UILabel!
    @IBOutlet weak var lblMiles: UILabel!
    @IBOutlet weak var lblStock: UILabel!
    @IBOutlet weak var lblDEal: UILabel!
    @IBOutlet weak var imgDealArrow: UIImageView!
    @IBOutlet weak var dealViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var viewBg: UIView!
    
}



