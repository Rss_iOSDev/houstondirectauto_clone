//
//  homeViewController.swift
//  Houston
//
//  Created by Reinforce on 08/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit
import WebKit

class formViewController: UIViewController {
    
    @IBOutlet weak var txtSelectForm:       RCustomTextField!
    
  
    //Make offer form
    @IBOutlet weak var tblMakeOffer:        UITableView!
    @IBOutlet weak var offerTxtPrice:       RCustomTextField!
    @IBOutlet weak var offerTxtFname:       RCustomTextField!
    @IBOutlet weak var offerTxtLname:       RCustomTextField!
    @IBOutlet weak var offerTxtEmail:       RCustomTextField!
    @IBOutlet weak var offerTxtViewMsg:     RCustomTextView!
    @IBOutlet weak var offerTxtPhone:       RCustomTextField!
    
    //availibility form
    @IBOutlet weak var tblAvailability:     UITableView!
    @IBOutlet weak var avaiTxtViewComment:  RCustomTextView!
    @IBOutlet weak var availTxtEmail:       RCustomTextField!
    @IBOutlet weak var availTxtPhoneNo:     RCustomTextField!
    @IBOutlet weak var availTxtFname:       RCustomTextField!
    @IBOutlet weak var availTxtLname:       RCustomTextField!
    
    
    //Trade - in Form
    @IBOutlet weak var tblTradeIn:          UITableView!
    @IBOutlet weak var tradeTxtVin:         RCustomTextField!
    @IBOutlet weak var tradeTxtYear:        RCustomTextField!
    @IBOutlet weak var tradeTxtMake:        RCustomTextField!
    @IBOutlet weak var tradeTxtModel:       RCustomTextField!
    @IBOutlet weak var tradeTxtMiles:       RCustomTextField!
    @IBOutlet weak var tradeTxtPrice:       RCustomTextField!
    @IBOutlet weak var tradeTxtFirstName:   RCustomTextField!
    @IBOutlet weak var tradeTxtLAstNAme:    RCustomTextField!
    @IBOutlet weak var tradeTxtEmail:       RCustomTextField!
    @IBOutlet weak var tradeTxtPhone:       RCustomTextField!
    @IBOutlet weak var tradeTxtViewQuestions:  RCustomTextView!
    
    //Finance form
    //@IBOutlet weak var tblFinance:          UITableView!
    @IBOutlet weak var webviewFinance:      WKWebView!

    
    var signImage : UIImage!
    var pickerview : UIPickerView!
    var pickerData = ["Test Drive","Make an Offer","Check Availability","Finance Form","Value for Trade In"]
    
//    var pickerSalesList = ["Person1","Person2","Person3","Person4","Person5"]
//    var pickerviewSales : UIPickerView!
//
//    var pickerVinList = ["vin1","vin2","vin3","vin4","vin5"]
//    var pickerviewVin : UIPickerView!
    
    
    
    
    var pickerviewMAke : UIPickerView!
    var pickerviewModel : UIPickerView!
    var pickerModelList = [String]()
    
    var imgPickFrom = ""
    
    var imagePicker = UIImagePickerController()
    
    
    var isFormType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
       // self.tabBarController?.tabBar.isHidden = false
       // self.tabBarController?.tabBar.isTranslucent = false
       // self.tabBarController?.selectedIndex = 0
        
        offerTxtViewMsg.delegate = self
        avaiTxtViewComment.delegate = self
        tradeTxtViewQuestions.delegate = self
        
        offerTxtViewMsg.text = "Your Message.."
        offerTxtViewMsg.textColor = UIColor.black
        
        avaiTxtViewComment.text = "Your Questions/Request.."
        avaiTxtViewComment.textColor = UIColor.black
        
        tradeTxtViewQuestions.text = "Questions.."
        tradeTxtViewQuestions.textColor = UIColor.black
        
       
        if(isFormType == ""){
            txtSelectForm.text = ""
            
            tblMakeOffer.isHidden = true
            tblAvailability.isHidden = true
            webviewFinance.isHidden = true
            tblTradeIn.isHidden = true
            
            if let url = URL(string: "https://www.houstondirectauto.com/finance/") {
                let request = URLRequest(url: url)
                webviewFinance.load(request)
            }
            
        }
        else{
             if(isFormType == "availability"){
                self.txtSelectForm.text = "Check Availability"
                tblMakeOffer.isHidden = true
                webviewFinance.isHidden = true
                tblAvailability.isHidden = false
            }
            else if(isFormType == "offer"){
                self.txtSelectForm.text = "Make an Offer"
                tblMakeOffer.isHidden = false
                webviewFinance.isHidden = true
                tblAvailability.isHidden = true
            }
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSubmit(_ sender: UIButton) {
        
         Globalfunc.showLoaderView(view: self.view)
        
        if(txtSelectForm.text == "Make an Offer"){
            
            if(offerTxtFname.text == "" || offerTxtFname.text?.count == 0 || offerTxtFname.text == nil){
                offerTxtFname.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill first name field.")
            }
            else if(offerTxtLname.text == "" || offerTxtLname.text?.count == 0 || offerTxtLname.text == nil){
                offerTxtLname.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill last name field.")
                
            }
            else if(offerTxtEmail.text == "" || offerTxtEmail.text?.count == 0 || offerTxtEmail.text == nil){
                offerTxtEmail.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill email field.")
                
            }
            else if(!(Globalfunc.isValidEmail(testStr: offerTxtEmail.text!))){
                offerTxtEmail.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill valid email field.")
                
            }
           
            else if(offerTxtPrice.text == "" || offerTxtPrice.text?.count == 0 || offerTxtPrice.text == nil){
                offerTxtPrice.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill price field.")
                
            }
            else if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                
                let param = ["first_name":"\(offerTxtFname.text!)",
                    "offer_price":"\(offerTxtPrice.text!)",
                    "last_name":"\(offerTxtLname.text!)",
                    "phone":"\(offerTxtPhone.text!)",
                    "email":"\(offerTxtEmail.text!)",
                    "message":"\(offerTxtViewMsg.text!)"]
                let url = "\(Constant.forms_Url)?action=makeanofferformsubmit"
                self.callFormSubmitApi(actionType: "offer", param: param, url: url)
            }
            else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
            
        else if(txtSelectForm.text == "Check Availability"){
            
            let param = ["comments":"\(avaiTxtViewComment.text!)",
                "first_name":"\(availTxtFname.text!)",
                "last_name":"\(availTxtLname.text!)",
                "phone":"\(availTxtPhoneNo.text!)",
                "email":"\(availTxtEmail.text!)"]
            let url = "\(Constant.forms_Url)?action=caformsubmit"
            self.callFormSubmitApi(actionType: "avail", param: param, url: url)
            
        }
        
       
        
        else if(txtSelectForm.text == "Value for Trade In"){
            
            //
            
            tradeTxtVin.text = "WDDXJ7JB7FA006570"
            
            let param = [ "vin" : "\(tradeTxtVin.text!)",
                "year":"\(tradeTxtYear.text!)",
                "make":"\(tradeTxtMake.text!)",
                "model":"\(tradeTxtModel.text!)",
                "miles":"\(tradeTxtMiles.text!)",
                "price":"\(tradeTxtPrice.text!)",
                "comments":"\(tradeTxtViewQuestions.text!)",
                "first_name":"\(tradeTxtFirstName.text!)",
                "last_name":"\(tradeTxtLAstNAme.text!)",
                "phone":"\(tradeTxtEmail.text!)",
                "email":"\(tradeTxtPhone.text!)"]
                let url = "\(Constant.forms_Url)?action=vtfformsubmit"
                self.callFormSubmitApi(actionType: "trade", param: param, url: url)
        }
    }
    
   
    @IBAction func clickOnHeaderView(_ sender : UIButton){
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(home, animated: true)
    }
}


//form submit api
extension formViewController : UIWebViewDelegate{
    
    func callFormSubmitApi(actionType :String ,param : [String:String] ,url: String){
    
        webServices.onResponsePostData(url: url, parms: param) { (dict, error) in
            if(error == ""){
                
                Globalfunc.print(object: dict)
                let status = dict["status"] as! String
                let msg = dict["msg"] as! String
                
                if(actionType == "offer"){
                    
                    if(status == "success"){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                           
                            let alert = UIAlertController(title: Constant.AppName, message: msg.htmlToString, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .
                                default:
                                    self.txtSelectForm.text = ""
                                    self.tblMakeOffer.isHidden = true
                                    self.tblAvailability.isHidden = true
                                    self.tblTradeIn.isHidden = true
                                    
                                    self.offerTxtViewMsg.text = ""
                                    self.offerTxtEmail.text = ""
                                    self.offerTxtPhone.text = ""
                                    self.offerTxtLname.text = ""
                                    self.offerTxtPrice.text = ""
                                    self.offerTxtFname.text = ""
                                    
                                case .cancel:
                                    Globalfunc.print(object: "cancel")
                                    
                                case .destructive:
                                    Globalfunc.print(object: "destructive")
                                    
                                    
                                }}))
                            self.present(alert, animated: true, completion: nil)
                           
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.callHomeView()
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                }
                else if(actionType == "avail"){
                    
                    if(status == "success"){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            
                            let alert = UIAlertController(title: Constant.AppName, message: msg.htmlToString, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    self.txtSelectForm.text = ""
                                    self.tblMakeOffer.isHidden = true
                                    self.tblAvailability.isHidden = true
                                    self.tblTradeIn.isHidden = true
                                    
                                    
                                    self.avaiTxtViewComment.text = ""
                                    self.availTxtFname.text = ""
                                    self.availTxtLname.text = ""
                                    self.availTxtPhoneNo.text = ""
                                    self.availTxtEmail.text = ""
                                    
                                    
                                case .cancel:
                                    Globalfunc.print(object: "cancel")
                                    
                                case .destructive:
                                    Globalfunc.print(object: "destructive")
                                    
                                    
                                }}))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    
                                },{action2 in
                                }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                            
                        }
                    }
                }
                
                
                else if(actionType == "trade"){
                    
                    if(status == "success"){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            
                            let alert = UIAlertController(title: Constant.AppName, message: msg.htmlToString, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    self.txtSelectForm.text = ""
                                    self.tblMakeOffer.isHidden = true
                                    self.tblAvailability.isHidden = true
                                    self.tblTradeIn.isHidden = true
                                    
                                    
                                    self.tradeTxtPrice.text = ""
                                    self.tradeTxtMiles.text = ""
                                    self.tradeTxtVin.text = ""
                                    self.tradeTxtYear.text = ""
                                    self.tradeTxtModel.text = ""
                                    self.tradeTxtMake.text = ""
                                    self.tradeTxtFirstName.text = ""
                                    self.tradeTxtLAstNAme.text = ""
                                    self.tradeTxtEmail.text = ""
                                    self.tradeTxtPhone.text = ""
                                    self.tradeTxtViewQuestions.text = ""
                                    
                                    
                                case .cancel:
                                    Globalfunc.print(object: "cancel")
                                    
                                case .destructive:
                                    Globalfunc.print(object: "destructive")
                                    
                                    
                                }}))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    
                                    },{action2 in
                                    }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                            
                        }
                    }
                }
                

                
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        Globalfunc.print(object: "Strat Loading")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Globalfunc.print(object: "Finish Loading")
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Globalfunc.print(object: error.localizedDescription)
    }
}

extension formViewController {
    
    func callModelLisApi(makeName : String){
        
        let url =  "\(Constant.modelsList_Url)"
        let authToken = userDefault.value(forKey: "authToken") as! String
        let postString = "token=\(authToken)&make=\(makeName)"
        
        webServices.callApiRequestforPost(postString, url: url) { (dict, error) in
            if(error == ""){
                
                Globalfunc.print(object: dict)
                let status = dict["status"] as! String
                if(status == "success"){
                    
                    OperationQueue.main.addOperation {
                        self.pickerModelList = dict["models"] as! [String]
                        Globalfunc.print(object: "list----\(self.pickerModelList)")
                        
                        let authToke = dict["token"] as! String
                        userDefault.set(authToke, forKey: "authToken")
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        let msg = dict["msg"] as! String
                        Globalfunc.print(object: msg)
                        if(msg == "Invalid or expired authentication token"){
                            self.callHomeView()
                        }else{
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                        }
                        
                    }
                }
                
            }else{
                  OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
            }
        }
        
    }
}


//Mark - Drop down
extension formViewController : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerview){
            return pickerData.count
        }
//        else if(pickerView == pickerviewSales ){
//            return pickerSalesList.count
//        }
//        else if(pickerView == pickerviewVin){
//            return pickerVinList.count
//        }
        else if(pickerView == pickerviewMAke){
            return Constant.pickerMakeList.count
        }
        else if(pickerView == pickerviewModel){
            return self.pickerModelList.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerview){
            return self.pickerData[row]
        }
//        else if(pickerView == pickerviewSales ){
//            return self.pickerSalesList[row]
//        }
//        else if(pickerView == pickerviewVin){
//            return  self.pickerVinList[row]
//        }
        else if(pickerView == pickerviewMAke){
            return  Constant.pickerMakeList[row]
        }
        else if(pickerView == pickerviewModel){
            return  self.pickerModelList[row]
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerview){
            self.txtSelectForm.text = self.pickerData[row]
        }
        else if(pickerView == pickerviewMAke){
            
            self.tradeTxtMake.text = Constant.pickerMakeList[row]
            self.callModelLisApi(makeName: Constant.pickerMakeList[row])
        }
        else if(pickerView == pickerviewModel){
            self.tradeTxtModel.text = self.pickerModelList[row]
        }
        
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtSelectForm){
            self.pickUp(txtSelectForm)
        }
        else if(textField == self.tradeTxtMake){
            self.tradeTxtMake.text = ""
            self.pickUp(tradeTxtMake)
        }
        else if(textField == self.tradeTxtModel){
            if(self.tradeTxtMake.text == ""){
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Select Make First")
            }else{
                self.pickUp(tradeTxtModel)
            }
        }
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtSelectForm){
            self.pickerview = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerview.delegate = self
            self.pickerview.dataSource = self
            self.pickerview.backgroundColor = UIColor.white
            textField.inputView = self.pickerview
        }
        else if(textField == self.tradeTxtMake){
            self.pickerviewMAke = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewMAke.delegate = self
            self.pickerviewMAke.dataSource = self
            self.pickerviewMAke.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewMAke
        }
        else if(textField == self.tradeTxtModel){
            self.pickerviewModel = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewModel.delegate = self
            self.pickerviewModel.dataSource = self
            self.pickerviewModel.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewModel
        }
        
        // UIPickerView
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(registrationViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(registrationViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtSelectForm.resignFirstResponder()
        tradeTxtMake.resignFirstResponder()
        tradeTxtModel.resignFirstResponder()
        
        if(self.txtSelectForm.text == "Test Drive"){
           
            let test = self.storyboard?.instantiateViewController(withIdentifier: "testDriveViewController") as! testDriveViewController
            self.navigationController?.pushViewController(test, animated: true)
        }
        if(self.txtSelectForm.text == "Car Appraisal"){
            tblMakeOffer.isHidden = true
            tblAvailability.isHidden = true
            webviewFinance.isHidden = true
            tblTradeIn.isHidden = true
        }
        if(self.txtSelectForm.text == "Make an Offer"){
            tblMakeOffer.isHidden = false
            tblAvailability.isHidden = true
            webviewFinance.isHidden = true
            tblTradeIn.isHidden = true
        }
        if(self.txtSelectForm.text == "Check Availability"){
            tblMakeOffer.isHidden = true
            webviewFinance.isHidden = true
            tblAvailability.isHidden = false
            tblTradeIn.isHidden = true
        }
        
        if(self.txtSelectForm.text == "Finance Form"){
            tblMakeOffer.isHidden = true
            tblAvailability.isHidden = true
            webviewFinance.isHidden = false
            tblTradeIn.isHidden = true
            
        }
        
        if(self.txtSelectForm.text == "Value for Trade In"){
            tblMakeOffer.isHidden = true
            tblAvailability.isHidden = true
            webviewFinance.isHidden = true
            tblTradeIn.isHidden = false
            
        }
        
    }
    @objc func cancelClick() {
        txtSelectForm.text = ""
        tradeTxtMake.text = ""
        tradeTxtModel.text = ""
        
        tblMakeOffer.isHidden = true
        tblAvailability.isHidden = true
        webviewFinance.isHidden = true
        tblTradeIn.isHidden = true
        
        
        txtSelectForm.resignFirstResponder()
        tradeTxtMake.resignFirstResponder()
        tradeTxtModel.resignFirstResponder()
    }
    
}


extension formViewController : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView == offerTxtViewMsg || textView == avaiTxtViewComment || textView == tradeTxtViewQuestions){
            if textView.textColor == UIColor.black {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        }
       
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if(textView == offerTxtViewMsg){
            if textView.text.isEmpty {
                textView.text = "Your Message.."
                textView.textColor = UIColor.black
            }
        }
        
        if(textView == avaiTxtViewComment){
            if textView.text.isEmpty {
                textView.text = "Your Questions/Request.."
                textView.textColor = UIColor.black
            }
        }
        if(textView == tradeTxtViewQuestions){
            if textView.text.isEmpty {
                textView.text = "Questions.."
                textView.textColor = UIColor.black
            }
        }
    }
}
