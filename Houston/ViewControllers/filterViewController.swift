
import UIKit
import YSRangeSlider



var makeMultipleSelectArray = [String]()
var modelMultipleSelectArray = [String]()
var trimMultipleSelectArray = [String]()
var colorMultipleSelectArray = [String]()
var extColorMultipleSelectArray = [String]()
var bodyMultipleSelectArray = [String]()
var engineMultipleSelectArray = [String]()
var transMultipleSelectArray = [String]()
var featuresMultipleSelectArray = [String]()
var milesMultipleSelectArray = [String]()
var sliderParams = ""


class filterViewController: UIViewController , YSRangeSliderDelegate{
    
    
    
    
        
    @IBOutlet weak var txtMake : UITextField!
    @IBOutlet weak var txtModel: RCustomTextField!
    @IBOutlet weak var txtTrim: RCustomTextField!
    @IBOutlet weak var txtInterior_Color: RCustomTextField!
    @IBOutlet weak var txtExterior_Color: RCustomTextField!
    @IBOutlet weak var txtBodystyle: RCustomTextField!
    @IBOutlet weak var txtEngine: RCustomTextField!
    @IBOutlet weak var txtTransmisson: RCustomTextField!
    
    
    @IBOutlet weak var yearSlider: YSRangeSlider!
    @IBOutlet weak var lblYearMin : UILabel!
    @IBOutlet weak var lblYearMax : UILabel!
    
     @IBOutlet weak var priceSlider: YSRangeSlider!
    @IBOutlet weak var lblPriceMin : UILabel!
    @IBOutlet weak var lblPriceMax : UILabel!
    
    @IBOutlet weak var mileageSlider: YSRangeSlider!
    @IBOutlet weak var lblMileMAx: UILabel!
    @IBOutlet weak var lblMileMin: UILabel!
    
    var strPriceMin = ""
    var strPriceMax = ""
    
    
    var pickerViewMAke: UIPickerView!
    var pickerViewModel: UIPickerView!
    var pickerViewTrim: UIPickerView!
    var pickerViewInterior_Color: UIPickerView!
    var pickerViewExterior_Color: UIPickerView!
    var pickerViewEngine: UIPickerView!
    var pickerViewTransmission: UIPickerView!
    var pickerViewBodystyle: UIPickerView!
    
    
    var pickerModelList = [String]()
   
   
    
    var isChooseSlider = ""
    var callModelApiOnce = ""
    
    
    var isChoosePicker = ""
    var maxMileageVal = 1000000
    
   
    
    
    var makeParam = ""
    
    
    var arrUrl = ["\(Constant.seriesList_Url)" ,
        "\(Constant.body_stylesList_Url)",
        "\(Constant.exterior_colorsList_Url)",
        "\(Constant.interior_colorsList_Url)",
        "\(Constant.enginesList_Url)",
        "\(Constant.transmissionsList_Url)"]
    
    
    
    var selectedMakeArray = [String]()
    var selectedModelArray = [String]()
    var selectedTrimArray = [String]()
    var selectedInterior_ColorArray = [String]()
    var selectedExterior_ColorArray = [String]()
    var selectedEngineArray = [String]()
    var selectedTransmissionArray = [String]()
    var selectedBodystyleArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.callAlList()
        yearSlider.delegate = self
        priceSlider.delegate = self
        mileageSlider.delegate = self
        
        if(makeString != ""){
            selectedMakeArray.append(makeString)
            
            makeMultipleSelectArray = selectedMakeArray
            
            self.txtMake.text = selectedMakeArray.joined(separator: "")
            let makeParam =  "make[]=\(makeString)"
            self.callModelLisApi(makeName: makeParam)
            
        }else{
            selectedMakeArray = []
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
""    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnBack(_ sender : UIButton){
        //  self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
   
    func rangeSliderDidChange(_ rangeSlider: YSRangeSlider, minimumSelectedValue: CGFloat, maximumSelectedValue: CGFloat) {
        
        if(rangeSlider == yearSlider){
            let minVal = Int(minimumSelectedValue)
            self.lblYearMin.text = "\(minVal)"
            
            let maxVal = Int(maximumSelectedValue)
            self.lblYearMax.text = "\(maxVal)"
            
        }
        
        if(rangeSlider == priceSlider){
            
            let minVal = Int(minimumSelectedValue)
            self.lblPriceMin.text = "$\(minVal)"
            
            let maxVal = Int(maximumSelectedValue)
            self.lblPriceMax.text = "$\(maxVal)"
            
            strPriceMin = "\(minVal)"
            strPriceMax = "\(maxVal)"
            
        }
       
        if(rangeSlider == mileageSlider){
            let minVal = Int(minimumSelectedValue)
            self.lblMileMin.text = "\(minVal)"
            
             maxMileageVal = Int(maximumSelectedValue)
            self.lblMileMAx.text = "\(maxMileageVal)"
            
        }

        ///&year_from=1999&year_to=2018&price_from=7000&price_to=54501&mileage_from=11000&mileage_to=99791
    }
    
    
    @IBAction func clickOnSearchBTn(_ sender : UIButton){
            searchTag = "5"
            sliderParams = "year_from=\(self.lblYearMin.text!)&year_to=\(self.lblYearMax.text!)&price_from=\(strPriceMin)&price_to=\(strPriceMax)&mileage_from=\(self.lblMileMin.text!)&mileage_to=\(maxMileageVal)"
        
            self.navigationController?.popViewController(animated: true)
    }
}



extension filterViewController : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      //  if component == 0 {
        if pickerView == pickerViewMAke{
            return Constant.pickerMakeList.count
        }
        else if(pickerView == pickerViewModel){
            return self.pickerModelList.count
        }
        
        else if(pickerView == pickerViewTrim){
            return Constant.arrSeries.count
        }
        
        else if(pickerView == pickerViewInterior_Color){
            return Constant.arrInteriorColor.count
        }
        else if(pickerView == pickerViewExterior_Color){
            return Constant.arrExteriorColor.count
        }
        
        else if(pickerView == pickerViewBodystyle){
            return Constant.dataBStyle.count
        }
        
        else if(pickerView == pickerViewEngine){
            return Constant.dataEngine.count
        }
        else if(pickerView == pickerViewTransmission){
            return Constant.dataTransmission.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSMutableAttributedString()
        if pickerView == pickerViewMAke{
            let string = Constant.pickerMakeList[row]
            let attrString = NSAttributedString(string: Constant.pickerMakeList[row] , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in  selectedMakeArray{
                if item == string {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "tick")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
        else if pickerView == pickerViewModel{
            let string = self.pickerModelList[row]
            let attrString = NSAttributedString(string: self.pickerModelList[row] , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedModelArray {
                if item == string {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "tick")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
        
        else if(pickerView == pickerViewTrim){
            let string = Constant.arrSeries[row]
            let attrString = NSAttributedString(string: Constant.arrSeries[row] , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedTrimArray {
                if item == string {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "tick")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewInterior_Color){
            let string = Constant.arrInteriorColor[row]
            let attrString = NSAttributedString(string: Constant.arrInteriorColor[row] , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedInterior_ColorArray {
                if item == string {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "tick")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
        else if(pickerView == pickerViewExterior_Color){
            let string = Constant.arrExteriorColor[row]
            let attrString = NSAttributedString(string: Constant.arrExteriorColor[row] , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedExterior_ColorArray {
                if item == string {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "tick")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewBodystyle){
            
            let string = Constant.dataBStyle[row]
            let attrString = NSAttributedString(string: Constant.dataBStyle[row] , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedBodystyleArray {
                if item == string {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "tick")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
            
        }
            
        else if(pickerView == pickerViewEngine){
            let string = Constant.dataEngine[row]
            let attrString = NSAttributedString(string: Constant.dataEngine[row] , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedEngineArray {
                if item == string {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "tick")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewTransmission){
            let string = Constant.dataTransmission[row]
            let attrString = NSAttributedString(string: Constant.dataTransmission[row] , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedTransmissionArray {
                if item == string {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "tick")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerViewMAke){
            
            if selectedMakeArray.contains(Constant.pickerMakeList[row]) {
                var index = 0
                for item in selectedMakeArray {
                    if item == Constant.pickerMakeList[row] {
                        selectedMakeArray.remove(at: index)
                        makeParam = ""
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedMakeArray.append(Constant.pickerMakeList[row])
            }
            
            makeMultipleSelectArray = selectedMakeArray
            self.txtMake.text = selectedMakeArray.joined(separator: ", ")    //selectedArray is array has selected item
            
            
            if selectedMakeArray.count > 0{
                
                for value in selectedMakeArray {
                    makeParam +=  "\("make[]")=\(value)&"
                }
                makeParam = String(makeParam.dropLast())
                
            }
            
            callModelApiOnce = "true"
            self.pickerViewMAke.reloadAllComponents()
        }
        
        else if(pickerView == pickerViewModel){
            
            if selectedModelArray.contains(self.pickerModelList[row]) {
                var index = 0
                for item in selectedModelArray {
                    if item == self.pickerModelList[row] {
                        selectedModelArray.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedModelArray.append(self.pickerModelList[row])
            }
            modelMultipleSelectArray = selectedModelArray
            self.txtModel.text = selectedModelArray.joined(separator: ", ")
            self.pickerViewModel.reloadAllComponents()
        }
        
        else if(pickerView == pickerViewTrim){
            if selectedTrimArray.contains(Constant.arrSeries[row]) {
                var index = 0
                for item in selectedTrimArray {
                    if item == Constant.arrSeries[row] {
                        selectedTrimArray.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedTrimArray.append(Constant.arrSeries[row])
            }
            trimMultipleSelectArray = selectedTrimArray
            self.txtTrim.text = selectedTrimArray.joined(separator: ", ")
            self.pickerViewTrim.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewInterior_Color){
            if selectedInterior_ColorArray.contains(Constant.arrInteriorColor[row]) {
                var index = 0
                for item in selectedInterior_ColorArray {
                    if item == Constant.arrInteriorColor[row] {
                        selectedInterior_ColorArray.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedInterior_ColorArray.append(Constant.arrInteriorColor[row])
            }
            
            colorMultipleSelectArray = selectedInterior_ColorArray
            self.txtInterior_Color.text = selectedInterior_ColorArray.joined(separator: ", ")
            self.pickerViewInterior_Color.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewExterior_Color){
            if selectedExterior_ColorArray.contains(Constant.arrExteriorColor[row]) {
                var index = 0
                for item in selectedExterior_ColorArray {
                    if item == Constant.arrExteriorColor[row] {
                        selectedExterior_ColorArray.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedExterior_ColorArray.append(Constant.arrExteriorColor[row])
            }
            
            extColorMultipleSelectArray = selectedExterior_ColorArray
            self.txtExterior_Color.text = selectedExterior_ColorArray.joined(separator: ", ")
            self.pickerViewExterior_Color.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewBodystyle){
            if selectedBodystyleArray.contains(Constant.dataBStyle[row]) {
                var index = 0
                for item in selectedBodystyleArray {
                    if item == Constant.dataBStyle[row] {
                        selectedBodystyleArray.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedBodystyleArray.append(Constant.dataBStyle[row])
            }
            bodyMultipleSelectArray = selectedBodystyleArray
            self.txtBodystyle.text = selectedBodystyleArray.joined(separator: ", ")
            self.pickerViewBodystyle.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewEngine){
            if selectedEngineArray.contains(Constant.dataEngine[row]) {
                var index = 0
                for item in selectedEngineArray {
                    if item == Constant.dataEngine[row] {
                        selectedEngineArray.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedEngineArray.append(Constant.dataEngine[row])
            }
            engineMultipleSelectArray = selectedEngineArray
            self.txtEngine.text = selectedEngineArray.joined(separator: ", ")
            self.pickerViewEngine.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewTransmission){
            if selectedTransmissionArray.contains(Constant.dataTransmission[row]) {
                var index = 0
                for item in selectedTransmissionArray {
                    if item == Constant.dataTransmission[row] {
                        selectedTransmissionArray.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedTransmissionArray.append(Constant.dataTransmission[row])
            }
            transMultipleSelectArray = selectedTransmissionArray
            self.txtTransmisson.text = selectedTransmissionArray.joined(separator: ", ")
            self.pickerViewTransmission.reloadAllComponents()
        }
    }
    
   
    //modelListData Api
    func callModelLisApi(makeName : String){
        
        
        let authToken = userDefault.value(forKey: "authToken") as! String
        
        let url =  "\(Constant.modelsList_Url)?token=\(authToken)&\(makeName)"
        
        Globalfunc.print(object: url)
        
        
        //webServices.callApiRequestforPost(postString, url: url) { (dict, error) in
            webServices.callApiRequestForGet(url: url) { (dict, error) in
            if(error == ""){
                
                Globalfunc.print(object: dict)
                let status = dict["status"] as! String
                if(status == "success"){
                    
                    OperationQueue.main.addOperation {
                        self.pickerModelList = dict["models"] as! [String]
                        self.callModelApiOnce = ""
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        let msg = dict["msg"] as! String
                        Globalfunc.print(object: msg)
                    }
                }
            }else{
                  OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
            }
        }
        
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtMake){
            self.txtModel.text = ""
            self.pickUp(txtMake)
        }
        else if(textField == self.txtModel){
            if(self.txtMake.text == ""){
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Select Make First")
            }else{
                self.pickUp(txtModel)
            }
        }
        else if(textField == self.txtTrim){
            self.pickUp(txtTrim)
        }
        else if(textField == self.txtInterior_Color){
            self.pickUp(txtInterior_Color)
        }
        else if(textField == self.txtExterior_Color){
            self.pickUp(txtExterior_Color)
        }
        else if(textField == self.txtBodystyle){
            self.pickUp(txtBodystyle)
        }
        else if(textField == self.txtEngine){
            self.pickUp(txtEngine)
        }
        else if(textField == self.txtTransmisson){
            self.pickUp(txtTransmisson)
        }
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        
        if(textField == self.txtMake){
            self.pickerViewMAke = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewMAke.delegate = self
            self.pickerViewMAke.dataSource = self
            self.pickerViewMAke.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewMAke
            
            isChoosePicker = "make"
            
        }
        else if(textField == self.txtModel){
            self.pickerViewModel = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewModel.delegate = self
            self.pickerViewModel.dataSource = self
            self.pickerViewModel.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewModel
        }

        else if(textField == self.txtTrim){
            self.pickerViewTrim = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewTrim.delegate = self
            self.pickerViewTrim.dataSource = self
            self.pickerViewTrim.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewTrim
        }
        else if(textField == self.txtInterior_Color){
            
            self.pickerViewInterior_Color = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewInterior_Color.delegate = self
            self.pickerViewInterior_Color.dataSource = self
            self.pickerViewInterior_Color.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewInterior_Color
            
        }
        else if(textField == self.txtExterior_Color){
            
            self.pickerViewExterior_Color = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewExterior_Color.delegate = self
            self.pickerViewExterior_Color.dataSource = self
            self.pickerViewExterior_Color.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewExterior_Color
            
        }
        else if(textField == self.txtBodystyle){
            self.pickerViewBodystyle = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewBodystyle.delegate = self
            self.pickerViewBodystyle.dataSource = self
            self.pickerViewBodystyle.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewBodystyle
        }
        else if(textField == self.txtEngine){
            self.pickerViewEngine = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewEngine.delegate = self
            self.pickerViewEngine.dataSource = self
            self.pickerViewEngine.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewEngine
        }
        else if(textField == self.txtTransmisson){
            
            self.pickerViewTransmission = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewTransmission.delegate = self
            self.pickerViewTransmission.dataSource = self
            self.pickerViewTransmission.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewTransmission
            
        }

        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(registrationViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(registrationViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        
        if (callModelApiOnce == "true"){
            self.callModelLisApi(makeName: makeParam)
        }
        
        
        txtMake.resignFirstResponder()
        txtModel.resignFirstResponder()
        txtTrim.resignFirstResponder()
        txtInterior_Color.resignFirstResponder()
        txtExterior_Color.resignFirstResponder()
        txtBodystyle.resignFirstResponder()
        txtEngine.resignFirstResponder()
        txtTransmisson.resignFirstResponder()
    }
    
    
    @objc func cancelClick() {
        
        if(isChoosePicker == "make"){
            txtMake.resignFirstResponder()
        }
        
        txtModel.text = ""
        txtTrim.text = ""
        txtInterior_Color.text = ""
        txtExterior_Color.text = ""
        txtBodystyle.text = ""
        txtEngine.text = ""
        txtTransmisson.text = ""
        
        
        txtModel.resignFirstResponder()
        txtTrim.resignFirstResponder()
        txtInterior_Color.resignFirstResponder()
        txtExterior_Color.resignFirstResponder()
        txtBodystyle.resignFirstResponder()
        txtEngine.resignFirstResponder()
        txtTransmisson.resignFirstResponder()
    }
    
}

extension filterViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}


//filters list
extension filterViewController {
    
    func callAlList() {
        let group = DispatchGroup() // initialize
        
        arrUrl.forEach { obj in
            
            // Here is an example of an asynchronous request which use a callback
            group.enter() // wait
            
            if let authToken = userDefault.value(forKey: "authToken") {
                
                let postString = "token=\(authToken)"
                webServices.callApiRequestforPost(postString, url: obj) { (dict, error) in
                    if(error == ""){
                        
                        Globalfunc.print(object: dict)
                        let status = dict["status"] as! String
                        if(status == "success"){
                            
                            OperationQueue.main.addOperation {
                                
                                let authToke = dict["token"] as! String
                                userDefault.set(authToke, forKey: "authToken")
                                
                                
                                if let _ = dict["series"] {
                                    Constant.arrSeries = dict["series"] as! [String]
                                }
                                
                                if let _ = dict["engines"] {
                                    Constant.dataEngine = dict["engines"] as! [String]
                                }
                                
                                if let _ = dict["transmissions"] {
                                    Constant.dataTransmission = dict["transmissions"] as! [String]
                                }
                                
                                if let _ = dict["body_styles"] {
                                    Constant.dataBStyle = dict["body_styles"] as! [String]
                                }
                                
                                if let _ = dict["interior_colors"] {
                                    Constant.arrInteriorColor = dict["interior_colors"] as! [String]
                                }
                                
                                if let _ = dict["exterior_colors"] {
                                    Constant.arrExteriorColor = dict["exterior_colors"] as! [String]
                                }
                               
                                group.leave()
                            }
                        }
                        else{
                            OperationQueue.main.addOperation {
                                Globalfunc.hideLoaderView(view: self.view)
                                let msg = dict["msg"] as! String
                                if(msg == "Invalid or expired authentication token"){
                                    self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                        self.callHomeView()
                                    },{action2 in
                                    }, nil])
                                }
                                else{
                                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                                }
                                
                            }
                        }
                        
                    }else{
                          OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                    }
                }
            }
        }
        
        group.notify(queue: .main) {
            // do something here when loop finished
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
}
