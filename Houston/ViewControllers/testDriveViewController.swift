

import UIKit
import WebKit

class testDriveViewController: UIViewController, UIWebViewDelegate {
    
    //Test drive form
     @IBOutlet weak var wkTestdrive : WKWebView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadForm()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func loadForm(){
        
//        let url = NSURL(string: "http://18.188.223.120/hda-test-drive-mobile/")
//        let request = URLRequest(url: url! as URL)
//        self.webViewTestDrive = UIWebView(frame: self.viewTestDrive.frame)
//        self.webViewTestDrive.scrollView.bounces = false
//        self.webViewTestDrive.delegate = self
//        self.webViewTestDrive.loadRequest(request)
//
//        self.webViewTestDrive.scalesPageToFit = true
//        self.webViewTestDrive.contentMode = .scaleAspectFit
//
//        self.viewTestDrive.addSubview(self.webViewTestDrive)
        
        
        if let url = URL(string: "https://www.houstondirectauto.com/test-drive-mobile/") {
            let request = URLRequest(url: url) 
            wkTestdrive.load(request)
        }
        
    }

}
