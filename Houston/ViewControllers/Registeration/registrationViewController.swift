//
//  registrationViewController.swift
//  Houston
//
//  Created by Reinforce on 11/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class registrationViewController: UIViewController {
    
        
    @IBOutlet weak var txtFirstName: RCustomTextField!
    @IBOutlet weak var txtEmail: RCustomTextField!
    @IBOutlet weak var txtLastName: RCustomTextField!
    @IBOutlet weak var txtConfirmEmail: RCustomTextField!
    @IBOutlet weak var txtPassword: RCustomTextField!
    @IBOutlet weak var txtAddress: RCustomTextField!
    @IBOutlet weak var txtConfirmPasswrd: RCustomTextField!
    @IBOutlet weak var txtCity: RCustomTextField!
    @IBOutlet weak var txtState: RCustomTextField!
    @IBOutlet weak var txtPhoneNum: RCustomTextField!
    
    
    var pickerview : UIPickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
""    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }

    
    
    //Sign up button
    @IBAction func clickOnSubmitBtn(_ sender : UIButton){
        
        if(txtFirstName.text == "" || txtFirstName.text?.count == 0 || txtFirstName.text == nil){

            txtFirstName.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill first name field.")
        }
        else if(txtLastName.text == "" || txtLastName.text?.count == 0 || txtLastName.text == nil){
            txtLastName.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill last name field.")
            
        }
        else if(txtEmail.text == "" || txtEmail.text?.count == 0 || txtEmail.text == nil){
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill email field.")
            
        }
        else if(!(Globalfunc.isValidEmail(testStr: txtEmail.text!))){
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill valid email field.")
            
        }
        else if(txtConfirmEmail.text == "" || txtConfirmEmail.text?.count == 0 || txtConfirmEmail.text == nil){
            txtConfirmEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill email field again.")
            
        }
        else if(txtEmail.text != txtConfirmEmail.text){
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Email and Confirm email field do not match.")
            
        }
        else if(txtPassword.text == "" || txtPassword.text?.count == 0 || txtPassword.text == nil){
            txtPassword.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill password field.")
            
            
        }
        else if(txtConfirmPasswrd.text == "" || txtConfirmPasswrd.text?.count == 0 || txtConfirmPasswrd.text == nil){
            
            txtConfirmPasswrd.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill password field again.")
            
        }
        else if(txtPassword.text != txtConfirmPasswrd.text){
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr:  "")
            
            txtConfirmPasswrd.shake()
            txtPassword.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Password and Confirm password field do not match.")
            
        }
        else if(txtAddress.text == "" || txtAddress.text?.count == 0 || txtAddress.text == nil){
            txtAddress.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill address field.")
        }
            
        else if(txtCity.text == "" || txtCity.text?.count == 0 || txtCity.text == nil){
            txtCity.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill City field.")
        }
        else if(txtState.text == "" || txtState.text?.count == 0 || txtState.text == nil){
            txtState.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select State from the list.")
        }
        else if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
            self.callSignUpApi()
        }
        else {
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    
    func callSignUpApi(){
        
        let url =  "\(Constant.registration_Url)"
        if let authToken = userDefault.value(forKey: "authToken") {
            
            
            let param = ["token":"\(authToken)",
                "email":"\(txtEmail.text!)",
                "password":"\(txtPassword.text!)",
                "first_name":"\(txtFirstName.text!)",
                "last_name":"\(txtLastName.text!)",
                "verify_password":"\(txtConfirmPasswrd.text!)"
            ]
            
            Globalfunc.print(object: param)
            
            webServices.onResponsePostData(url: url, parms: param) { (dict, error) in
                if(error == ""){
                    
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    if(status == "success"){
                        
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            let authToke = dict["token"] as! String
                            userDefault.set(authToke, forKey: "authToken")
                            
                            let alert = UIAlertController(title: Constant.AppName, message: "Registration done successfully.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    
                                   self.navigationController?.popViewController(animated: true)
                                    
                                case .cancel:
                                    Globalfunc.print(object: "cancel")
                                    
                                case .destructive:
                                    Globalfunc.print(object: "destructive")
                                    
                                }}))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    
                                    },{action2 in
                                    }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                            
                        }
                    }
                    
                }else{
                      OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
            
            
        }
        
    }

}

extension registrationViewController : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Constant.pickerStatesData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let dict = Constant.pickerStatesData[row]
        return (dict["name"] as! String)
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dict = Constant.pickerStatesData[row]
        self.txtState.text = (dict["name"] as! String)
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUp(txtState)
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
            // UIPickerView
        self.pickerview = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.pickerview.delegate = self
        self.pickerview.dataSource = self
        self.pickerview.backgroundColor = UIColor.white
        textField.inputView = self.pickerview
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(registrationViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(registrationViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtState.resignFirstResponder()
    }
    @objc func cancelClick() {
        txtState.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if((textField == txtFirstName) || (textField == txtLastName)){
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = set.isSuperset(of: typedCharacterSet)
            return alphabet
        }
        return true
    }
    
}
