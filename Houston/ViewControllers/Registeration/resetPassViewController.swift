//
//  resetPassViewController.swift
//  Houston
//
//  Created by Reinforce on 30/03/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class resetPassViewController: UIViewController {

    @IBOutlet weak var headerImageHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var txtOtp : UITextField!
    @IBOutlet weak var txtNewPass : UITextField!
    @IBOutlet weak var txtVerifyPass : UITextField!
    
    var enail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func clickOnResendOtp(_ sender : UIButton){
        
    }
    
    @IBAction func clickOnVerifyBtn(_ sender: UIButton){
        
        if(txtOtp.text == "" || txtOtp.text?.count == 0 || txtOtp.text == nil){
            txtOtp.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill OTP field.")
        }
        else if(txtNewPass.text == "" || txtNewPass.text?.count == 0 || txtNewPass.text == nil){
            txtNewPass.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        else if(txtVerifyPass.text == "" || txtVerifyPass.text?.count == 0 || txtVerifyPass.text == nil){
            txtVerifyPass.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        else if(txtNewPass.text != txtVerifyPass.text){
            txtNewPass.shake()
            txtVerifyPass.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Passwords do not match.")
        }
        else if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
            self.callResetPass()
        } else {
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    
    func callResetPass(){
        let url =  "\(Constant.reset_password_Url)"
        
        if let authToken = userDefault.value(forKey: "authToken") {
            
            let param = ["token":"\(authToken)",
                "hash": txtOtp.text!,
                "password": txtNewPass.text!,
                "rpassword": txtVerifyPass.text!,
                "email" : enail
            ]
            
            webServices.onResponsePostData(url: url, parms: param) { (dict, error) in
                if(error == ""){
                    
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    if(status == "success"){
                        
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            self.popupAlert(title: Constant.AppName, message: "Password updated successfully.", actionTitles: ["OK"], actions: [{action1 in
                                
                                let authToke = dict["token"] as! String
                                userDefault.set(authToke, forKey: "authToken")
                                let login = self.storyboard?.instantiateViewController(withIdentifier: "profileViewController") as! profileViewController
                                self.navigationController?.pushViewController(login, animated: true)
                                },{action2 in
                                }, nil])
                            
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    
                                    },{action2 in
                                    }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                    
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
    }

}
