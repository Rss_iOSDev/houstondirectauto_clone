//
//  receiptViewController.swift
//  Houston
//
//  Created by Apple on 27/01/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit
import PDFGenerator

class receiptViewController: UIViewController {
    
    var dict : NSDictionary = [:]
    //receipt outlets
    @IBOutlet weak var lblRec_recieptId: UILabel!
    @IBOutlet weak var lblRec_TransDate: UILabel!
    @IBOutlet weak var lblRec_PaymentDate: UILabel!
    @IBOutlet weak var lblRec_CustNAme: UILabel!
    @IBOutlet weak var lblRec_Loan: UILabel!
    @IBOutlet weak var lblRec_Acct: UILabel!
    @IBOutlet weak var lblRec_Vin: UILabel!
    @IBOutlet weak var lblRec_Description1: UILabel!
    @IBOutlet weak var lblRec_Commnts: UILabel!
    @IBOutlet weak var lblRec_TransType: UILabel!
    @IBOutlet weak var lblRec_BalancDEsc: UILabel!
    @IBOutlet weak var lblRec_Amt: UILabel!
    
    @IBOutlet weak var lblRec_AmtPaid: UILabel!
    @IBOutlet weak var lblRec_AmtReceived: UILabel!
    @IBOutlet weak var lblRec_chngDue: UILabel!
    @IBOutlet weak var lblRec_PayMthd: UILabel!
    @IBOutlet weak var lblRec_Reference: UILabel!
    @IBOutlet weak var lblRec_guid: UILabel!
    
    @IBOutlet weak var scrollReceipt : UIScrollView!

    var lblOwnername = ""
    var pass_loan_no = ""
    var pass_strVinNo = ""
    var pass_strDescripyion = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpDataonReceipt()
        // Do any additional setup after loading the view.
    }
    
}

extension receiptViewController {
    
    
    func setUpDataonReceipt(){
        
        self.lblRec_recieptId.text = "Receipt Id: \(dict["Tran_ID"] as! Int)"
        self.lblRec_TransDate.text = "Transaction Date: \(dict["EffectDate"] as! String)"
        self.lblRec_PaymentDate.text = "Payment Date: \(dict["PostDate"] as! String)"
        self.lblRec_CustNAme.text = self.lblOwnername
        self.lblRec_Loan.text = "Loan: \(self.pass_loan_no)"
        self.lblRec_Acct.text = "Acount: \(dict["Acct_ID"] as! Int)"
        self.lblRec_Vin.text = "Vin: \(self.pass_strVinNo)"
        self.lblRec_Description1.text = "Description: \(self.pass_strDescripyion)"
        self.lblRec_Commnts.text = "\(dict["Comment"] as! String)"
        self.lblRec_TransType.text = "Regular Payment"
        self.lblRec_BalancDEsc.text = "Primary Loan"
        let amt = dict["TranCollected"] as! NSNumber
        if(amt == 0){
            self.lblRec_Amt.text = "$ 660.00"
            self.lblRec_AmtPaid.text = "$ 660.00"
            self.lblRec_AmtReceived.text = "$ 660.00"
        }
        else{
            let aString = "\(amt)"
            let newString = aString.replacingOccurrences(of: "-", with: "$", options: .literal, range: nil)
            self.lblRec_Amt.text = newString
            self.lblRec_AmtPaid.text = newString
            self.lblRec_AmtReceived.text = newString
        }
        
        self.lblRec_chngDue.text = "$0.00"
        self.lblRec_PayMthd.text = "\(dict["Form"] as! String)"
        self.lblRec_Reference.text = "\(dict["RefNumber"] as! String)"
        self.lblRec_guid.text = "\(dict["EPSTransaction_GUID"] as! String)"


    }
    
    @IBAction func clickOnGeneratePdf(_ sender : UIButton){
        
//        Globalfunc.showLoaderView(view: self.view)
   //     OperationQueue.main.addOperation {
            let dst = NSHomeDirectory() + "\(self.lblOwnername)_\(self.pass_loan_no).pdf"
            try! PDFGenerator.generate(self.scrollReceipt, to: dst)
            print(dst)
        
        
//            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
////                Globalfunc.hideLoaderView(view: self.view)
//                //self.viewReceipt.alpha = 0.0
//
//            }) { (_) in
                let detail = self.storyboard?.instantiateViewController(withIdentifier: "pdfViewController") as! pdfViewController
                detail.passStr = dst
                self.navigationController?.pushViewController(detail, animated: true)
//            }
       // }
    }
    
    @IBAction func clickOnCRossBtn(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
}
