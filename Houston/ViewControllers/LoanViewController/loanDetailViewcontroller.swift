//
//  loanDetailViewcontroller.swift
//  Houston
//
//  Created by Reinforce on 16/03/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit
import PDFGenerator

class loanDetailViewcontroller: UIViewController {
    
    @IBOutlet weak var collTab : UICollectionView!
    var arrTabList = ["snapshot","History","make payment"]
    
    var loan_no = ""
    var acc_no = ""
    var strVinNo = ""
    var strDescripyion = ""
    var strCardVal = ""
    
    var strIsDate = ""
    var isTypeSel = ""
    var isPickerSel = ""
    var cardType = 0
    var cc_type = ""
    
    
    var selectIndex = 0
    var deu_amt : NSNumber = 0
    var min_dueAmt : NSNumber = 0
    var payoff_amt : NSNumber = 0
    var isRadioSelect = ""
    
    var arrPaylmntMthdList : [NSDictionary] = []
    var settingDict : NSDictionary = [:]
    
    
    @IBOutlet weak var lblOwnerNAme: UILabel!
    @IBOutlet weak var lblHEaderLoanNo: UILabel!
    @IBOutlet weak var lblHEaderAcct: UILabel!
    @IBOutlet weak var lblHEaderStock: UILabel!
    
    
    //snapshot outlets
    @IBOutlet weak var viewSnapshot: UIView!
    @IBOutlet weak var lblSnapAcctBal: UILabel!
    @IBOutlet weak var lblSnapDueDate: UILabel!
    @IBOutlet weak var lblSnapLastPayDate: UILabel!
    @IBOutlet weak var lblSnapPastDueDay: UILabel!
    @IBOutlet weak var lblSnapPastDuePayment: UILabel!
    @IBOutlet weak var lblSnapDueAMt: UILabel!
    
    //Insurance outlets
    @IBOutlet weak var lblInsStatus: UILabel!
    @IBOutlet weak var lblInsPlicy: UILabel!
    @IBOutlet weak var lblInsCovgP: UILabel!
    @IBOutlet weak var lblInsCollDeduc: UILabel!
    @IBOutlet weak var lblInsCompDeduc: UILabel!
    @IBOutlet weak var lblInsAgent: UILabel!
    @IBOutlet weak var lblInsAgentPhn: UILabel!
    @IBOutlet weak var lblInsAgentFax: UILabel!
    @IBOutlet weak var lblInsCompany: UILabel!
    
    //History Outlets
    @IBOutlet weak var viewHistory: UIView!
    @IBOutlet weak var tblPaymentHistory : UITableView!
    var arrPaymnetHistry : NSMutableArray = []
    
    
    
    //payment outlets
    @IBOutlet weak var viewMakePayment: UIView!
    @IBOutlet weak var btnFirst: UIButton!
    @IBOutlet weak var btnSec: UIButton!
    @IBOutlet weak var btnThird: UIButton!
    @IBOutlet weak var btnFourth: UIButton!
    
    @IBOutlet weak var txtPaymentDate: RCustomTextField!
    @IBOutlet weak var txtPAyMinAmount: RCustomTextField!
    @IBOutlet weak var txtPayDueAmount: RCustomTextField!
    @IBOutlet weak var txtPayOtherAmount: RCustomTextField!
    @IBOutlet weak var txtPayOffBal: RCustomTextField!
    @IBOutlet weak var txtSelectPAymentMethods: RCustomTextField!
    @IBOutlet weak var txtAuthrise1: RCustomTextField!
    @IBOutlet weak var txtAuthrise2: RCustomTextField!
    var datePickrPaymnt : UIDatePicker!
    var pickerPaymntLis : UIPickerView!
    
    var responseDict : NSDictionary = [:]
    
    
    
    
    //add payment outlets
    @IBOutlet weak var addPaymntMthdView: UIView!
    @IBOutlet weak var viewCVV : UIView!
    @IBOutlet weak var txtCvv: RCustomTextField!
    @IBOutlet weak var txtAddPayTitle: RCustomTextField!
    @IBOutlet weak var txtAddPayType: RCustomTextField!
    var pickerviewAddMthod : UIPickerView!
    
    @IBOutlet weak var imgCC: UIImageView!
    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var txtCCNo: RCustomTextField!
    @IBOutlet weak var txtCCExp: RCustomTextField!
    @IBOutlet weak var txtNameOnCC: RCustomTextField!
    
    
    @IBOutlet weak var mainAddPayView : UIView!
    @IBOutlet weak var payViewHeight : NSLayoutConstraint!
    @IBOutlet weak var CCView : UIView!
    @IBOutlet weak var AchView : UIView!
    @IBOutlet weak var txtAchBankname: RCustomTextField!
    @IBOutlet weak var txtAchBankAccNo: RCustomTextField!
    @IBOutlet weak var txtAchRoutingType: RCustomTextField!
    @IBOutlet weak var txtAchAcctype: RCustomTextField!
    var pickerviewAccType : UIPickerView!
    
    var creditCardValidator: CreditCardValidator!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Globalfunc.showLoaderView(view: self.view)
        self.viewSnapshot.isHidden = true
        self.viewHistory.isHidden = true
        self.viewMakePayment.isHidden = true
        self.addPaymntMthdView.isHidden = true
        self.viewCVV.isHidden = true
        self.callSettingApi()
        creditCardValidator = CreditCardValidator()
        self.txtPayOtherAmount.addTarget(self, action: #selector(self.txtCalculatePercentage), for: .editingChanged)

    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
}


extension loanDetailViewcontroller{
    
    func callSettingApi(){
        
        
        if let authToken = userDefault.value(forKey: "authToken") {
            
            let url =  "\(Constant.payment_setting)?token=\(authToken)"
            
            Globalfunc.print(object: url)
            webServices.callApiRequestForGet(url: url) { (dict, error) in
                if(error == ""){
                    
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    if(status == "error"){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                },{action2 in
                                }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                    else if(status == "success"){
                        OperationQueue.main.addOperation {
                            
                            print(dict)
                            Globalfunc.hideLoaderView(view: self.view)
                            let authToke = dict["token"] as! String
                            userDefault.set(authToke, forKey: "authToken")
                            self.settingDict = dict["setting"] as! NSDictionary
                            DispatchQueue.main.asyncAfter(deadline: .now()) {
                                self.callLoanApi()
                            }
                        }
                    }
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
        
    }
    
    func callLoanApi(){
        do {
            let decoded  = userDefault.object(forKey: "userinfo") as! Data
            if let userDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary {
                
                let role = userDict["role"] as! String
                self.callLoanNoDetailApi(strRole: role)
                
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func callLoanNoDetailApi(strRole: String){
        let url =  "\(Constant.search_by_loan_acc_Url)"
        
        if let authToken = userDefault.value(forKey: "authToken") {
            let postString = "token=\(authToken)&loan_no=\(loan_no)&role=\(strRole)"
            Globalfunc.print(object: url)
            
            Globalfunc.print(object: postString)
            
            webServices.callApiRequestforPost(postString, url: url) { (dict, error) in
                if(error == ""){
                    
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    if(status == "error"){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                },{action2 in
                                }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                        
                    }else{
                        
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let apiData = dict["data"] as! NSDictionary
                            let verifacto = dict["verifacto"] as! NSDictionary
                            self.setDataOnViews(dict: apiData, verifactoDict: verifacto)
                        }
                    }
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
        
    }
    
    func setDataOnViews(dict : NSDictionary, verifactoDict : NSDictionary){
        
        self.responseDict = dict
        
        self.viewSnapshot.isHidden = false
        
        if let Collateral_StockNumber = dict["Collateral_StockNumber"] as? String
        {
            self.lblHEaderStock.text = Collateral_StockNumber
        }
        else{
            let Collateral_VIN = dict["Collateral_VIN"] as! String
            let last6 = String(Collateral_VIN.suffix(6))
            self.lblHEaderStock.text = last6
            
        }
        self.btnFirst.isSelected = true
        self.lblOwnerNAme.text = (dict["BorrowerName"] as! String).uppercased()
        self.lblHEaderLoanNo.text = (dict["LoanNumber"] as! String)
        self.lblHEaderAcct.text = "\((dict["Acct_ID"] as! Int))"
        acc_no = "\((dict["Acct_ID"] as! Int))"
        
        //set data on snapshot
        
        let accountSummaryDict = dict["AccountSummary"] as! NSDictionary
        self.lblSnapAcctBal.text = "$ \(accountSummaryDict["Acct_CurTotalBal"] as! NSNumber)"
        
        var due_date = dict["CurDueDate"] as! String
        var due_amt = dict["CurDueAmt"] as! NSNumber
        
        if((due_amt.intValue <= 0 || due_date == "")){
            due_date = dict["NextDueDate"] as! String
            due_amt = dict["NextDueAmt"] as! NSNumber
        }
        
        self.lblSnapDueDate.text = due_date
        self.lblSnapDueAMt.text = "$ \(due_amt)"
        
        
        let lastPaidDate =  accountSummaryDict["Acct_LastPaidDate"] as! String
        self.lblSnapLastPayDate.text = lastPaidDate
        
        let ActNumDaysPD =  dict["ActNumDaysPD"] as! NSNumber
        self.lblSnapPastDueDay.text = "\(ActNumDaysPD)"
        
        let ActNumPaymentsPD =  dict["ActNumPaymentsPD"] as! NSNumber
        self.lblSnapPastDuePayment.text = "\(ActNumPaymentsPD)"
        
        
        //set data on insurance
        
        
        if(!verifactoDict.allValues.isEmpty){
            print(verifactoDict)
            
            let ins_status = verifactoDict["policy_status"] as! String
            let policy_no = verifactoDict["policy_no"] as! String
            let start_date = verifactoDict["start_date"] as! String
            let end_date = verifactoDict["end_date"] as! String
            let collision_deductible = verifactoDict["collision_deductible"] as! String
            let comprehensive_deductible = verifactoDict["comprehensive_deductible"] as! String
            let agent = verifactoDict["agent"] as! String
            let agent_phone = verifactoDict["agent_phone"] as! String
            let agent_fax = verifactoDict["agent_phone"] as! String
            let insurer_name = verifactoDict["insurer_name"] as! String
            
            self.lblInsStatus.text = ins_status
            self.lblInsPlicy.text = policy_no
            self.lblInsCovgP.text = "\(start_date) to \(end_date)"
            self.lblInsCollDeduc.text = "$ \(collision_deductible)"
            self.lblInsCompDeduc.text = "$ \(comprehensive_deductible)"
            self.lblInsAgent.text = agent
            self.lblInsAgentPhn.text = agent_phone
            self.lblInsAgentFax.text = agent_fax
            self.lblInsCompany.text = insurer_name
            
        }
        else{
            print(dict)
            
            let ins_status = dict["InsStatusDesc"] as! String
            let policy_no = dict["PolicyNumber"] as! String
            let start_date = dict["EffDate"] as! String
            let end_date = dict["ExpDate"] as! String
            let collision_deductible = dict["ColDeductible"] as! String
            let comprehensive_deductible = dict["CompDeductible"] as! String
            var agent = dict["InsAgent_ID"] as! String
            
            let InsAgentName = dict["InsAgentName"] as! String
            if(InsAgentName != "N/A" && InsAgentName != "N/A"){
                agent += " " + InsAgentName
            }
            
            let agent_phone = dict["InsAgentPhone1"] as! String
            let agent_fax = dict["InsAgentFax1"] as! String
            let insurer_name = dict["InsCompanyName"] as! String
            
            self.lblInsStatus.text = ins_status
            self.lblInsPlicy.text = policy_no
            self.lblInsCovgP.text = "\(start_date) to \(end_date)"
            self.lblInsCollDeduc.text = "$ \(collision_deductible)"
            self.lblInsCompDeduc.text = "$ \(comprehensive_deductible)"
            self.lblInsAgent.text = agent
            self.lblInsAgentPhn.text = agent_phone
            self.lblInsAgentFax.text = agent_fax
            self.lblInsCompany.text = insurer_name
            
        }
        
        //set data on history
        var arrTrans = dict["TransactionHistory"] as! NSArray
        arrTrans = arrTrans.reversed() as NSArray
        for i in 0..<arrTrans.count{
            let dict = arrTrans[i] as! NSDictionary
            let TranCodeDesc = dict["TranCodeDesc"] as! String
            if(TranCodeDesc == "Payment" || TranCodeDesc == "Pay Reversal"){
                self.arrPaymnetHistry.add(dict)
            }
        }
        
        
        
        
        self.strVinNo = (dict["Collateral_VIN"] as! String)
        self.strDescripyion = "\(dict["Collateral_Year"] as! Int), \(dict["Collateral_Make"] as! String), \(dict["Collateral_Model"] as! String)."
        
        //set data on make payment
        
        payoff_amt = accountSummaryDict["Payoff_Balance"] as! NSNumber
        self.txtPayOffBal.text = "\(payoff_amt)"
        
        min_dueAmt = dict["FilteredDueAmt"] as! NSNumber
        self.txtPAyMinAmount.text = "$ \(min_dueAmt)"
        
        self.deu_amt = due_amt
        self.txtPayDueAmount.text = "$ \(deu_amt)"
        
        self.tblPaymentHistory.reloadData()
    }
    
    
    @IBAction func clikcOnRadioBtn(_ sender : UIButton){
        
        var conv_fee = 0.0
        let convenience_fee_unit = self.settingDict["convenience_fee_unit"] as! String
        let convenience_fee = self.settingDict["convenience_fee"] as! String
        
        if(sender.tag == 1){
            
            self.setImgOnBtn(btn1: self.btnFirst, btn2: self.btnSec, btn3: self.btnThird, btn4: self.btnFourth)
            self.isRadioSelect = "payment_amount_min"
            let val = Double(truncating: min_dueAmt)
            
            if(convenience_fee_unit == "percent"){
                conv_fee = (Double(convenience_fee)! * val) / 100
            }
            else if(convenience_fee_unit == "fixed"){
                conv_fee = Double(convenience_fee)!
            }
            
            self.txtAuthrise1.text = String(format:"%.2f", conv_fee)
            let total = (val) + conv_fee
            self.txtAuthrise2.text = String(format:"%.2f", total)
            
        }
        else if(sender.tag == 2){
            self.setImgOnBtn(btn1: self.btnSec, btn2: self.btnFirst, btn3: self.btnThird, btn4: self.btnFourth)
            
            self.isRadioSelect = "payment_amount_due"
            
            let val = Double(truncating: self.deu_amt)
            
            if(convenience_fee_unit == "percent"){
                conv_fee = (Double(convenience_fee)! * val) / 100
            }
            else if(convenience_fee_unit == "fixed"){
                conv_fee = Double(convenience_fee)!
            }
            
            self.txtAuthrise1.text = String(format:"%.2f", conv_fee)
            let total = (val) + conv_fee
            self.txtAuthrise2.text = String(format:"%.2f", total)
            
        }
        else if(sender.tag == 3){
            self.setImgOnBtn(btn1: self.btnThird, btn2: self.btnFirst, btn3: self.btnSec, btn4: self.btnFourth)
            self.isRadioSelect = "payment_amount_other"
        }
        else if(sender.tag == 4){
            self.setImgOnBtn(btn1: self.btnFourth, btn2: self.btnFirst, btn3: self.btnSec, btn4: self.btnThird)
            
            self.isRadioSelect = "payment_amount_payoff"
            
            let val = Double(truncating: payoff_amt)
            let calPer = (2*(val) / 100)
            
            self.txtAuthrise1.text = String(format:"%.2f", calPer)
            let total = (val) + calPer
            self.txtAuthrise2.text = String(format:"%.2f", total)
        }
        
    }
    
    func setImgOnBtn(btn1 : UIButton, btn2:UIButton, btn3:UIButton, btn4:UIButton){
        btn1.isSelected = true
        btn2.isSelected = false
        btn3.isSelected = false
        btn4.isSelected = false
    }
}

extension loanDetailViewcontroller : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPaymnetHistry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblPaymentHistory.dequeueReusableCell(withIdentifier: "historyTblCell") as! historyTblCell
        let dict = self.arrPaymnetHistry[indexPath.row] as! NSDictionary
        cell.lblPostDate.text = (dict["EffectDate"] as! String)
        cell.lblPaymnetMEthod.text = (dict["Form"] as! String)
        cell.lblTypes.text = (dict["TranCodeDesc"] as! String)
        cell.lblComments.text = (dict["Comment"] as! String)
        let amt = dict["TranCollected"] as! NSNumber
        if(amt == 0){
            cell.lblPaymentAmt.text = "$ 660.00"
        }
        else{
            let aString = "\(amt)"
            let newString = aString.replacingOccurrences(of: "-", with: "$", options: .literal, range: nil)
            cell.lblPaymentAmt.text = newString
            
        }
        cell.btnReceipt.tag = indexPath.row
        cell.btnReceipt.addTarget(self, action: #selector(clickOnOpenReceiptBtn(_:)), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    
    @objc func clickOnOpenReceiptBtn(_ sender : UIButton){
        
        let dict = self.arrPaymnetHistry[sender.tag] as! NSDictionary
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "receiptViewController") as! receiptViewController
        detail.dict = dict
        detail.lblOwnername = self.lblOwnerNAme.text!
        detail.pass_loan_no = self.loan_no
        detail.pass_strVinNo = self.strVinNo
        detail.pass_strDescripyion = self.strDescripyion
        self.navigationController?.pushViewController(detail, animated: true)

    }
    
   
    
    
//    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
//
//        Globalfunc.hideLoaderView(view: self.view)
//        return self.navigationController!//or use return self.navigationController for fetching app navigation bar colour
//    }
//
//    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
//
//        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
//            self.viewReceipt.alpha = 0.0
//        }, completion: nil)
//
//
//    }
    
}



extension loanDetailViewcontroller : UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTabList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collTab.dequeueReusableCell(withReuseIdentifier: "tabsItemCell", for: indexPath) as! tabsItemCell
        cell.lblName.text = arrTabList[indexPath.item]
        
        
        if(selectIndex == indexPath.item){
            cell.viewBg.backgroundColor = UIColor.init(red: 60/255.0, green: 131/255.0, blue: 195/255.0, alpha: 1)
            cell.lblName.textColor = UIColor.white
        }
        else{
            cell.viewBg.backgroundColor = UIColor.init(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1)
            cell.lblName.textColor = UIColor.black
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentCell = collTab.cellForItem(at: indexPath) as! tabsItemCell
        currentCell.viewBg.backgroundColor = UIColor.init(red: 60/255.0, green: 131/255.0, blue: 195/255.0, alpha: 1)
        currentCell.lblName.textColor = UIColor.white
        
        selectIndex = indexPath.item
        
        if(indexPath.item == 0){
            self.viewSnapshot.isHidden = false
            self.viewHistory.isHidden = true
            self.viewMakePayment.isHidden = true
        }
        else if(indexPath.item == 1){
            self.viewSnapshot.isHidden = true
            self.viewHistory.isHidden = false
            self.viewMakePayment.isHidden = true
        }
        else if(indexPath.item == 2){
            self.viewSnapshot.isHidden = true
            self.viewHistory.isHidden = true
            self.viewMakePayment.isHidden = false
            self.getPAymentMEthodList()
        }
        collTab.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        let selectedCell = collTab.cellForItem(at: indexPath)  as! tabsItemCell
        selectedCell.viewBg.backgroundColor = UIColor.init(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1)
        selectedCell.lblName.textColor = UIColor.black
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewSize = collectionView.frame.size.width
        return CGSize(width: collectionViewSize/3, height: collectionViewSize/3)
    }
}

class tabsItemCell : UICollectionViewCell{
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var viewBg : UIView!
}


class historyTblCell : UITableViewCell{
    
    @IBOutlet weak var lblPostDate : UILabel!
    @IBOutlet weak var lblPaymentAmt : UILabel!
    @IBOutlet weak var lblPaymnetMEthod : UILabel!
    @IBOutlet weak var lblTypes : UILabel!
    @IBOutlet weak var lblComments : UILabel!
    @IBOutlet weak var btnReceipt : UIButton!
    
}


//add payment method code
extension loanDetailViewcontroller : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    @IBAction func clickOnAddPaymentMEthodBtn(_ sender : UIButton){
        
        if(self.txtPayOtherAmount.text! < "\(deu_amt)"){
            let msg = "The minimum payment amount is $\(deu_amt). Please contact 281-581-0833 or email collections@houstondirectauto.com for any further questions."
            let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            self.addPaymntMthdView.isHidden = false
            self.CCView.isHidden = false
            self.AchView.isHidden = true
            self.payViewHeight.constant = 400
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerviewAddMthod){
            return Constant.pickerPaymentMthod.count
        }
        if(pickerView == pickerviewAccType){
            return Constant.pickerBankAccType.count
        }
        if(pickerView == pickerPaymntLis){
            return self.arrPaylmntMthdList.count
        }
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerviewAddMthod){
            return Constant.pickerPaymentMthod[row]
        }
        
        if(pickerView == pickerviewAccType){
            return Constant.pickerBankAccType[row]
        }
        if(pickerView == pickerPaymntLis){
            let dict = self.arrPaylmntMthdList[row]
            Globalfunc.print(object: dict)
            var cardno = ""
            if let card_no = dict["card_no"] as? String {
                cardno = String(card_no.suffix(4))
            }
            let card_exp = dict["card_exp"] as! String
            strCardVal = dict["id"] as! String
            return "xxxxxxxxxxxx\(cardno) / \(card_exp)"
        }
        
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerviewAddMthod){
            
            isPickerSel = "first"
            self.txtAddPayType.text = Constant.pickerPaymentMthod[row]
            if(row == 0){
                isTypeSel = "cc"
                cardType = 4
            }else{
                isTypeSel = "ach"
                cardType = 14
            }
        }
        
        if(pickerView == pickerviewAccType){
            isPickerSel = "sec"
            self.txtAchAcctype.text = Constant.pickerBankAccType[row]
            
        }
        
        if(pickerView == pickerPaymntLis){
            isPickerSel = "third"
            let dict = self.arrPaylmntMthdList[row]
            var cardno = ""
            if let card_no = dict["card_no"] as? String {
                cardno = String(card_no.suffix(4))
            }
            let card_exp = dict["card_exp"] as! String
            self.txtSelectPAymentMethods.text = "xxxxxxxxxxxx\(cardno) / \(card_exp)"
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtAddPayType){
            self.pickUp(txtAddPayType)
        }
        
        if(textField == self.txtPaymentDate){
            self.pickUp(txtPaymentDate)
        }
        
        if(textField == self.txtAchAcctype){
            self.pickUp(txtAchAcctype)
        }
        if(textField == self.txtSelectPAymentMethods){
            self.pickUp(self.txtSelectPAymentMethods)
        }
        
        
        
    }
    
    @objc func txtCalculatePercentage() {
        
        if(self.txtPayOtherAmount.text == ""){
        }
        else{
            
            var conv_fee = 0.0
            let convenience_fee_unit = self.settingDict["convenience_fee_unit"] as! String
            let convenience_fee = self.settingDict["convenience_fee"] as! String
            
            let val = Double(self.txtPayOtherAmount.text!)
            
            if(convenience_fee_unit == "percent"){
                conv_fee = (Double(convenience_fee)! * val!) / 100
            }
            else if(convenience_fee_unit == "fixed"){
                conv_fee = Double(convenience_fee)!
            }
            self.txtAuthrise1.text = String(format:"%.2f", conv_fee)
            let total = (val!) + conv_fee
            self.txtAuthrise2.text = String(format:"%.2f", total)
        }
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtAddPayType){
            self.pickerviewAddMthod = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewAddMthod.delegate = self
            self.pickerviewAddMthod.dataSource = self
            self.pickerviewAddMthod.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewAddMthod
            
        }
        
        if(textField == self.txtAchAcctype){
            self.pickerviewAccType = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewAccType.delegate = self
            self.pickerviewAccType.dataSource = self
            self.pickerviewAccType.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewAccType
            
        }
        
        if(textField == self.txtSelectPAymentMethods){
            self.pickerPaymntLis = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPaymntLis.delegate = self
            self.pickerPaymntLis.dataSource = self
            self.pickerPaymntLis.backgroundColor = UIColor.white
            
            if(self.arrPaylmntMthdList.count == 0){
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please add atleast one payment method.")
            }else{
                textField.inputView = self.pickerPaymntLis
            }
        }
        
        if(textField == self.txtPaymentDate){
            
            self.datePickrPaymnt = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.datePickrPaymnt.datePickerMode = UIDatePicker.Mode.date
            self.datePickrPaymnt.minimumDate = Date()
            self.txtPaymentDate.inputView = self.datePickrPaymnt
            strIsDate = "true"
        }
        
        
        // UIPickerView
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(loanDetailViewcontroller.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(loanDetailViewcontroller.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        if(strIsDate == "true"){
            
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            let myString = formatter.string(from: self.datePickrPaymnt.date)
            let yourDate = formatter.date(from: myString)
            formatter.dateFormat = "MM/dd/yyyy"
            self.txtPaymentDate.text = formatter.string(from: yourDate!)
            txtPaymentDate.resignFirstResponder()
        }
        
        if(isPickerSel == "sec"){
            txtAchAcctype.resignFirstResponder()
        }
        if(isPickerSel == "first"){
            
            if(isTypeSel == "cc"){
                txtAddPayType.resignFirstResponder()
                self.CCView.isHidden = false
                self.AchView.isHidden = true
                self.payViewHeight.constant = 400
                self.imgTick.isHidden = true
            }
            else{
                
                txtAddPayType.resignFirstResponder()
                self.CCView.isHidden = true
                self.AchView.isHidden = false
                self.payViewHeight.constant = 450
                self.imgTick.isHidden = true
            }
        }
        if(isPickerSel == "third"){
            txtSelectPAymentMethods.resignFirstResponder()
            self.viewCVV.isHidden = false
        }
        
    }
    
    @objc func cancelClick() {
        if(strIsDate == "true"){
            txtPaymentDate.text = ""
            txtPaymentDate.resignFirstResponder()
        }
        if(isPickerSel == "sec"){
            txtAchAcctype.text = ""
            txtAchAcctype.resignFirstResponder()
        }
        if(isPickerSel == "first"){
            if(isTypeSel == "cc"){
                txtAddPayType.text = ""
                txtAddPayType.resignFirstResponder()
            }
            else{
                txtAddPayType.text = ""
                txtAddPayType.resignFirstResponder()
            }
        }
        if(isPickerSel == "third"){
            txtSelectPAymentMethods.text = ""
            txtSelectPAymentMethods.resignFirstResponder()
        }
    }
    
    
    @IBAction func cardNumberDidChange(sender: UITextField) {
        if let number = sender.text {
            if number.isEmpty {
                self.imgCC.image = UIImage(named: "cc_placeholder")
                self.imgTick.isHidden = true
                Globalfunc.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Enter card number")
            } else {
                validateCardNumber(number: number)
                detectCardNumberImg(number: number)
                detectCardNumberType(number: number)
            }
        }
    }
    
    
    func validateCardNumber(number: String) {
        if creditCardValidator.validate(string: number) {
            self.imgTick.isHidden = false
            self.imgTick.image = imgTick.image?.withRenderingMode(.alwaysTemplate)
            self.imgTick.tintColor = UIColor.green
            
        } else {
            self.imgTick.isHidden = false
            self.imgTick.image = imgTick.image?.withRenderingMode(.alwaysTemplate)
            self.imgTick.tintColor = UIColor.red
        }
    }
    
    func detectCardNumberImg(number: String) {
        if let type = creditCardValidator.type(from: number) {
            self.imgCC.image = UIImage(named: type.img)
        } else {
            self.imgCC.image = UIImage(named: "cc_placeholder")
        }
    }
    
    func detectCardNumberType(number: String) {
        if let type = creditCardValidator.type(from: number) {
            self.cc_type = type.name
        } else {
            self.cc_type = ""
        }
    }
    
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton){
        
        //self.txtCCNo.text = "4802138686908997"
        
        if(self.txtAddPayTitle.text == "" || self.txtAddPayTitle.text?.count == 0){
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please enter payment title")
        }
        else if(isTypeSel == ""){
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please select payment type")
        }
        else{
            if(isTypeSel == "cc"){
                if(txtCCNo.text == ""){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please fill credit card no")
                }
                else if(txtCCExp.text == ""){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please fill card exp.")
                }
                else if(txtNameOnCC.text == ""){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please fill name on card")
                }
                else{
                    self.callApitoSavePaymentMethd()
                }
                
            }else if (isTypeSel == "ach"){
                
                if(txtAchBankAccNo.text == ""){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please fill bank acc. no")
                }
                else if(txtAchBankname.text == ""){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please fill name on bank acc.")
                }
                else if(txtAchRoutingType.text == ""){
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please fill bank routing number")
                }
                else{
                    self.callApitoSavePaymentMethd()
                }
            }
            
        }
        
    }
    
    @IBAction func clickOnCancelkBtn(_ sender : UIButton){
        self.addPaymntMthdView.isHidden = true
    }
    
    func callApitoSavePaymentMethd(){
        let url =  "\(Constant.save_payment_method_Url)"
        if let authToken = userDefault.value(forKey: "authToken") {
            let param = ["token":"\(authToken)",
                         "title":"\(self.txtAddPayTitle.text!)",
                         "card_no":"\(self.txtCCNo.text!)",
                         "card_exp":"\(self.txtCCExp.text!)",
                         "type":"\(cardType)",
                         "cc_type":"\(self.cc_type)",
                         "holder_name":"\(self.txtNameOnCC.text!)"]
            Globalfunc.print(object: param)
            self.arrPaylmntMthdList = []
            webServices.onResponsePostData(url: url, parms: param) { (dict, error) in
                if(error == ""){
                    
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    let msg = dict["msg"] as! String
                    if(status == "success"){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let alert = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                                            switch action.style{
                                                            case .default:
                                                                self.arrPaylmntMthdList = dict["payms"] as! [NSDictionary]
                                                                self.addPaymntMthdView.isHidden = true
                                                            case .cancel:
                                                                Globalfunc.print(object: "cancel")
                                                            case .destructive:
                                                                Globalfunc.print(object: "destructive")
                                                            }}))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                //                                let date = Date().addingTimeInterval(0)
                                //                                let timeRem = Timer(fireAt: date, interval: 0, target: self, selector: #selector(self.removeAuthTokenValue), userInfo: nil, repeats: false)
                                //                                RunLoop.main.add(timeRem, forMode: .common)
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                            
                        }
                    }
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
    }
    
    func getPAymentMEthodList(){
        let url =  "\(Constant.user_payments_Url)"
        
        if let authToken = userDefault.value(forKey: "authToken") {
            let param = ["token":"\(authToken)",
                         "acc_no": acc_no,
                         "loan_no": loan_no
            ]
            
            webServices.onResponsePostData(url: url, parms: param) { (dict, error) in
                if(error == ""){
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    if(status == "success"){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            self.arrPaylmntMthdList = dict["payms"] as! [NSDictionary]
                            Globalfunc.print(object: self.arrPaylmntMthdList)
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            
                            if(msg == "Invalid or expired authentication token"){
                                //                                let date = Date().addingTimeInterval(0)
                                //                                let timeRem = Timer(fireAt: date, interval: 0, target: self, selector: #selector(self.removeAuthTokenValue), userInfo: nil, repeats: false)
                                //                                RunLoop.main.add(timeRem, forMode: .common)
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
    }
}

//process paymnt api
extension loanDetailViewcontroller {
    
    @IBAction func clickOnPaymentProcessBtn(_ sender : UIButton){
        self.callPaymentProcesApi()
    }
    
    func callPaymentProcesApi(){
        let url =  "\(Constant.process_payment_Url)"
        if let authToken = userDefault.value(forKey: "authToken") {
            let param = ["token":"\(authToken)",
                         "acc_no": acc_no,
                         "loan_no": loan_no,
                         "payment_date" : "\(self.txtPaymentDate.text!)",
                         "payment_amount_min" : self.txtPAyMinAmount.text!,
                         "payment_amount_due" : "\(self.txtPayDueAmount.text!)",
                         "payment_amount_other" : "\(self.txtPayOtherAmount.text!)",
                         "payment_amount_payoff" : "\(self.txtPayOffBal.text!)",
                         "payment_method" : "\(strCardVal)",
                         "ccv" : "\(self.txtCvv.text!)",
                         "convenience_fee" : "\(self.txtAuthrise1.text!)",
                         "total_deductible" : self.txtAuthrise2.text!,
                         "radio-stacked" : self.isRadioSelect
            ]
            Globalfunc.print(object: param)
            webServices.onResponsePostData(url: url, parms: param) { (dict, error) in
                if(error == ""){
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    let msg = dict["msg"] as! String
                    if(status == "success"){
                        
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["OK"], actions:[{action1 in
                                self.navigationController?.popViewController(animated: true)
                            },{action2 in
                            }, nil])
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
    }
}
