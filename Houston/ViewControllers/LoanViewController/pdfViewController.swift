//
//  pdfViewController.swift
//  Houston
//
//  Created by Apple on 04/11/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import WebKit

class pdfViewController: UIViewController , WKNavigationDelegate{


    var passStr = ""
    
    
    @IBOutlet weak var webView: WKWebView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.navigationDelegate = self
        Globalfunc.showLoaderView(view: self.view)
        self.loadWebView()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func loadWebView()
    {
            let final_url = NSURL(fileURLWithPath: passStr) as URL
            webView.load(URLRequest(url: final_url))
            webView.allowsBackForwardNavigationGestures = true
            Globalfunc.hideLoaderView(view: self.view)

    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: loanDetailViewcontroller.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    

    
}
