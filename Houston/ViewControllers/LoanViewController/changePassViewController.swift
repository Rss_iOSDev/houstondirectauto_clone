//
//  changePassViewController.swift
//  Houston
//
//  Created by Reinforce on 16/03/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class changePassViewController: UIViewController {
    
    @IBOutlet weak var txtOldPass: RCustomTextField!
    @IBOutlet weak var txtNewPAss: RCustomTextField!
    @IBOutlet weak var txtConfrmPass: RCustomTextField!
    
    var strRole = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
           
        
        do {
            let decoded  = userDefault.object(forKey: "userinfo") as! Data
            if let userDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary {
                
                self.strRole = userDict["role"] as! String
                
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    
    
    
    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnUpdateVtb(_ sender: UIButton) {
        
        if(txtOldPass.text == "" || txtOldPass.text?.count == 0 || txtOldPass.text == nil){
            txtOldPass.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        else if(txtNewPAss.text == "" || txtNewPAss.text?.count == 0 || txtNewPAss.text == nil){
            txtNewPAss.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        else if(txtConfrmPass.text == "" || txtConfrmPass.text?.count == 0 || txtConfrmPass.text == nil){
            txtConfrmPass.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        
        else if(txtNewPAss.text != txtConfrmPass.text){
            txtConfrmPass.shake()
            txtNewPAss.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Passwords do not match.")
        }
        else if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
            self.callChangePassApi()
        } else {
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
        
    }
    
    
    func callChangePassApi(){
        let url =  "\(Constant.change_password_Url)"
        if let authToken = userDefault.value(forKey: "authToken") {
            
            
            let param = ["token":"\(authToken)",
                "old_password": txtOldPass.text!,
                "password": txtNewPAss.text!,
                "vpassword": txtConfrmPass.text!,
                "role": self.strRole
            ]
            
            webServices.onResponsePostData(url: url, parms: param) { (dict, error) in
                if(error == ""){
                    
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    if(status == "success"){
                        
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            self.popupAlert(title: Constant.AppName, message: "Password updated successfully.", actionTitles: ["OK"], actions: [{action1 in
                               
                                let authToke = dict["token"] as! String
                                userDefault.set(authToke, forKey: "authToken")
                                userDefault.set(self.txtNewPAss.text!, forKey: "password")
                                
                                self.txtConfrmPass.text = ""
                                self.txtNewPAss.text = ""
                                self.txtOldPass.text = ""
                                
                            },{action2 in
                            }, nil])
                            
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    
                                    },{action2 in
                                    }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                            
                        }
                    }
                    
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
            
            
        }
    }
}
