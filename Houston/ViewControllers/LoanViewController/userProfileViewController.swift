//
//  userProfileViewController.swift
//  Houston
//
//  Created by Reinforce on 16/03/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class userProfileViewController: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUserNAme()
    }
    
    @IBAction func clickOnHomeLogo(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
    }
    
    
    func setUserNAme(){
        
        do {
            let decoded  = userDefault.object(forKey: "userinfo") as! Data
            if let userDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary {
                
                let fname  = "\(userDict["first_name"] as! String)"
                let lname = "\(userDict["last_name"] as! String)"
                self.lblName.text = "Welcome: \(fname) \(lname)"
                
            }
        } catch {
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    
    
    
    @IBAction func clickOnLogoutBtn(_ sender : UIButton){
        
        userDefault.removeObject(forKey: "session")
        userDefault.removeObject(forKey: "userinfo")
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func clickOnBtns(sender: UIButton) {
        
        switch sender.tag {
        case 1:
            let loan = self.storyboard?.instantiateViewController(withIdentifier: "editProfileViewController") as! editProfileViewController
            self.navigationController?.pushViewController(loan, animated: true)
        case 2:
            let loan = self.storyboard?.instantiateViewController(withIdentifier: "loanListViewController") as! loanListViewController
            self.navigationController?.pushViewController(loan, animated: true)
        case 3:
            let loan = self.storyboard?.instantiateViewController(withIdentifier: "changePassViewController") as! changePassViewController
            self.navigationController?.pushViewController(loan, animated: true)
        default:
            Globalfunc.print(object: "Unknown language")
            return
        }
    }
    
}
