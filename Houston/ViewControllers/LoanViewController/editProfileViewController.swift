//
//  editProfileViewController.swift
//  Houston
//
//  Created by Reinforce on 16/03/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class editProfileViewController: UIViewController {
    
    
    
    @IBOutlet weak var txtFnsme: RCustomTextField!
    @IBOutlet weak var txtLane: RCustomTextField!
    @IBOutlet weak var txtEmail: RCustomTextField!
    
    
    var strField = ""
    var role = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        
           
        self.setProfileData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    
    
    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnUpdateBtn(_ sender: UIButton) {
        
        if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
            self.callUpdateProfileApi()
        } else {
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    
    func setProfileData(){
        
        do {
            let decoded  = userDefault.object(forKey: "userinfo") as! Data
            if let userDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary {
                
                self.txtFnsme.text = "\(userDict["first_name"] as! String)"
                self.txtLane.text = "\(userDict["last_name"] as! String)"
                self.txtEmail.text = "\(userDict["email"] as! String)"
                
                self.role = userDict["role"] as! String
                
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func callUpdateProfileApi(){
        let url =  "\(Constant.update_profile_Url)"
        
        if let authToken = userDefault.value(forKey: "authToken") {
            var param : [String:String] = [:]
            if(self.strField == "first"){
                param = ["token":"\(authToken)",
                    "field": "first_name",
                    "value": txtFnsme.text!,
                    "role": self.role
                ]
            }
            else{
                
                param = ["token":"\(authToken)",
                    "field": "last_name",
                    "value": txtLane.text!,
                    "role": self.role
                ]
            }

            webServices.onResponsePostData(url: url, parms: param) { (dict, error) in
                if(error == ""){
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    if(status == "success"){
                        
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            let authToke = dict["token"] as! String
                            userDefault.set(authToke, forKey: "authToken")
                            
                            self.popupAlert(title: Constant.AppName, message: "Profile updated successfully.", actionTitles: ["OK"], actions: [{action1 in
                                
                                do {
                                    let decoded  = userDefault.object(forKey: "userinfo") as! Data
                                    if let userDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary {
                                        
                                        userDict.setObject(self.txtFnsme.text!, forKey: "first_name" as NSCopying)
                                        userDict.setObject(self.txtLane.text!, forKey: "last_name" as NSCopying)
                                        
                                        Globalfunc.print(object: userDict)
                                        
                                        do {
                                            if #available(iOS 11.0, *) {
                                                let myData = try NSKeyedArchiver.archivedData(withRootObject: userDict, requiringSecureCoding: false)
                                                userDefault.set(myData, forKey: "userinfo")
                                            } else {
                                                let myData = NSKeyedArchiver.archivedData(withRootObject: userDict)
                                                userDefault.set(myData, forKey: "userinfo")
                                            }
                                            
                                        } catch {
                                            Globalfunc.print(object: "Couldn't write file")
                                        }
                                        
                                    }
                                } catch {
                                    Globalfunc.print(object: "Couldn't read file.")
                                }
                               
                                
                                },{action2 in
                                }, nil])
                            
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    
                                    },{action2 in
                                    }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                            
                        }
                    }
                    
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
            
            
        }
    }
    
    
}

extension editProfileViewController :  UITextFieldDelegate{
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtFnsme){
            self.strField = "first"
        }
        
        if(textField == self.txtLane){
            self.strField = "last"
        }
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if((textField == txtFnsme) || (textField == txtLane)){
            
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = set.isSuperset(of: typedCharacterSet)
            return alphabet
            
        }
        return true
    }
    
}
