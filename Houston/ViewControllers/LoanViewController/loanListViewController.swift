//
//  loanListViewController.swift
//  Houston
//
//  Created by Reinforce on 15/03/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class loanListViewController: UIViewController {

    var scrollIndex = 1
    
    @IBOutlet weak var tblLoanList: UITableView!
    
    
    @IBOutlet weak var lblLoanRecord: UILabel!
    @IBOutlet weak var viewFooter : UIView!
    
    @IBOutlet weak var registerLoanView : UIView!
    @IBOutlet weak var txtLoanNumber: UITextField!
    @IBOutlet weak var txtLastnameLoan: RCustomTextField!
    @IBOutlet weak var txtSSn: RCustomTextField!
    
    
    var isthereMoreData: Bool = false
    var role = ""
    
    //  var pickerSortData = ["","","","","","","Mileage(Low to High)","Mileage(High to Low)","",""]
    
    var arrLoanData : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.registerLoanView.isHidden = true
        self.lblLoanRecord.isHidden = true
        txtLastnameLoan.autocapitalizationType = .allCharacters
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
         self.arrLoanData = []
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDefault.object(forKey: "userinfo") as! Data
            if let userDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary {
                
                role = userDict["role"] as! String
                self.callLoanListApi(strRole: role)
                
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnRegisterBtn(_ sender : UIButton){
        UIView.animate(withDuration: 3.0, delay: 1.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.registerLoanView.isHidden = false
        }, completion: nil)
    }
    
   
    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
   
    
    @IBAction func clckOnSubmitLoanNoBtn(_ sender : UIButton){
       
        if(txtLastnameLoan.text == "" || txtLastnameLoan.text?.count == 0 || txtLastnameLoan.text == nil){
            txtLastnameLoan.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        else if(txtLoanNumber.text == "" || txtLoanNumber.text?.count == 0 || txtLoanNumber.text == nil){
            txtLoanNumber.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        else if(txtSSn.text == "" || txtSSn.text?.count == 0 || txtSSn.text == nil){
            txtSSn.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        else if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
            Globalfunc.showLoaderView(view: self.view)
            self.callLoanApi(strUrl: Constant.reg_loan_Url, strType: "register", loan_id: "")
        } else {
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    @IBAction func cliclOnCancel(_ sender: UIButton) {
        self.registerLoanView.isHidden = true
    }
    
}

//Api calling
extension loanListViewController {
    
    //1. Loan list api
    func callLoanListApi(strRole: String){
        let url =  "\(Constant.customer_loans_Url)"

        if let authToken = userDefault.value(forKey: "authToken") {
                let postString = "token=\(authToken)&role=\(strRole)"
            
            print(postString)

                webServices.callApiRequestforPost(postString, url: url) { (dict, error) in
                    if(error == ""){
                        Globalfunc.print(object: dict)
                        let status = dict["status"] as! String
                        if(status == "error"){
                            OperationQueue.main.addOperation {
                                 Globalfunc.hideLoaderView(view: self.view)
                                let msg = dict["msg"] as! String
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                        self.callHomeView()
                                },{action2 in
                                }, nil])
                            }
                        }else{
                            
                            OperationQueue.main.addOperation {
                                Globalfunc.hideLoaderView(view: self.view)
                                
                                let authToke = dict["token"] as! String
                                userDefault.set(authToke, forKey: "authToken")
                                
                                let arr = dict["loans"] as! Array<Any>
                                if(arr.count == 0){
                                    self.arrLoanData = []
                                    self.isthereMoreData = false
                                    self.lblLoanRecord.isHidden = false
                                    self.tblLoanList.reloadData()
                                }
                                else{
                                    for dict in arr {
                                        self.arrLoanData.add(dict as! NSDictionary)
                                    }
                                    self.lblLoanRecord.isHidden = true
                                    self.tblLoanList.isHidden = false
                                    self.tblLoanList.reloadData()
                                }
                            }
                        }
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
    }
    
    
    
    func callDeleteLoanApi(loan_id: String){
        let url =  "\(Constant.delete_loan_Url)"
        if let authToken = userDefault.value(forKey: "authToken") {
            let postString = "token=\(authToken)&id=\(loan_id)"
            webServices.callApiRequestforPost(postString, url: url) { (dict, error) in
                if(error == ""){
                    Globalfunc.print(object: dict)
                    OperationQueue.main.addOperation {
                        let msg = dict["msg"] as! String
                        let alert = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                self.lblLoanRecord.isHidden = true
                                self.tblLoanList.isHidden = false
                                self.tblLoanList.reloadData()
                            case .cancel:
                                Globalfunc.print(object: "cancel")
                            case .destructive:
                                Globalfunc.print(object: "destructive")
                            }}))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
    }
    
    func callLoanApi(strUrl: String, strType: String, loan_id: String){
        
        var postString = ""
        
        if(strType == "register"){
            if let authToken = userDefault.value(forKey: "authToken"){
                postString = "token=\(authToken)&loan_no=\(txtLoanNumber.text!)&last_name=\(txtLastnameLoan.text!)&last4_ssn=\(txtSSn.text!)&role=\(role)"
            }
        }
        
        print(postString)
        
        webServices.callApiRequestforPost(postString, url: strUrl) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object: dict)
                OperationQueue.main.addOperation{
                    
                    Globalfunc.hideLoaderView(view: self.view)
                    let status = dict["status"] as! String
                    let msg = dict["msg"] as! String
                    if(status == "error"){
                        self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                            
                            self.tabBarController?.selectedIndex = 0
                            UserDefaults.standard.set(false, forKey: "HasLaunchedOnce")
                            self.registerLoanView.isHidden = true
                            
                            },{action2 in
                            }, nil])
                    }
                    else{
                        let alert = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                let authToke = dict["token"] as! String
                                userDefault.set(authToke, forKey: "authToken")
                                self.registerLoanView.isHidden = true
                                self.txtSSn.text = ""
                                self.txtLastnameLoan.text = ""
                                self.txtLoanNumber.text = ""
                                self.viewWillAppear(true)
                            case .cancel:
                                Globalfunc.print(object: "cancel")
                            case .destructive:
                                Globalfunc.print(object: "destructive")
                            }}))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
        
        }
}



extension loanListViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLoanData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblLoanList.dequeueReusableCell(withIdentifier: "loanTblCell") as! loanTblCell
        let dict = self.arrLoanData[indexPath.row] as! NSDictionary
        cell.lblLoanNo.text = (dict["loan_no"] as! String)
        cell.lblBalance.text = "$ \(dict["balance"] as! String)"
        
        let next_dueamt = dict["next_due_amount"] as! String
        let next_dueDate = dict["next_due_date"] as! String
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let yourDate = formatter.date(from: next_dueDate)
        formatter.dateFormat = "yyyy-MM-dd"
        let current_DateTime = formatter.string(from: yourDate!)
        cell.lblNExtDue.text = "\(next_dueamt) (\(current_DateTime))"
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(clickOnDeleteBtn(_:)), for: UIControl.Event.touchUpInside)
        
        cell.btnPay.tag = indexPath.row
        cell.btnPay.addTarget(self, action: #selector(clickOnNxtBtn(_:)), for: UIControl.Event.touchUpInside)
        
        return cell
    }
    
    @objc func clickOnDeleteBtn(_ sender: UIButton){
        let dict = self.arrLoanData[sender.tag] as! NSDictionary
        let loanId = dict["id"] as! String
        self.callDeleteLoanApi(loan_id: loanId)
    }
    
     @objc func clickOnNxtBtn(_ sender : UIButton){
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "loanDetailViewcontroller") as! loanDetailViewcontroller
        let dict = self.arrLoanData[sender.tag] as! NSDictionary
        detail.loan_no = (dict["loan_no"] as! String)
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//
//        if let lastVisibleIndexPath = self.tblLoanList.indexPathsForVisibleRows?.last {
//            let lastElement = arrLoanData.count - 1
//            if lastVisibleIndexPath.row == lastElement {
//
//                self.lblLoanRecord.isHidden = true
//                self.spinner.isHidden = false
//                spinner.startAnimating()
//                //self.tblSearchList.tableFooterView = spinner
//                //self.tblSearchList.tableFooterView?.isHidden = false
//
//                self.perform(#selector(loadTable), with: nil, afterDelay: 3.0)
//
//            }
//
////            if let dataCount = arrLoanData.count {
////                let dif = abs(dataCount - lastVisibleIndexPath.row)
////
////                if dif == 0 {
////                    pageNo += 1
////                    fetchMoreData()
////                }
////            }
//        }
//
//    }
    
   
    
}

class loanTblCell : UITableViewCell{
    
    @IBOutlet weak var lblLoanNo : UILabel!
    @IBOutlet weak var lblBalance : UILabel!
    @IBOutlet weak var lblNExtDue : UILabel!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnPay : UIButton!
    
}
