
import UIKit

class aboutUsViewController: UIViewController {
    
    @IBOutlet weak var tblEmployeeList : UITableView!
    @IBOutlet weak var lblStatus : UILabel!
    
    
        
    var arrTeamData : [NSDictionary] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblStatus.isHidden = true
        self.tblEmployeeList.isHidden = true
        
        if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
           self.callEmployeeListApi()
        } else {
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
""    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnBack(_ sender : UIButton){
        //  self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func callEmployeeListApi(){
        
        let url =  "\(Constant.teamList_Url)"
        if let authToken = userDefault.value(forKey: "authToken") {
            
            let postString = "token=\(authToken)"
            webServices.callApiRequestforPost(postString, url: url) { (dict, error) in
                if(error == ""){
                    
                    Globalfunc.print(object: dict)
                    let status = dict["status"] as! String
                    if(status == "success"){
                        
                        OperationQueue.main.addOperation {
                            
                            let authToke = dict["token"] as! String
                            userDefault.set(authToke, forKey: "authToken")
                            
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            let teamDict = dict["team"] as! NSDictionary
                           // self.arrTeamData =
                            
                            
                            if let accounting = teamDict["finance"] as? [NSDictionary] {
                                self.arrTeamData = accounting
                            }
                            
                            if let buyers_department = teamDict["sales"] as? [NSDictionary] {
                                self.arrTeamData += buyers_department
                            }
                            
                            if let insurance_department = teamDict["titles"] as? [NSDictionary] {
                                self.arrTeamData += insurance_department
                            }
                            
                           
                            
                            
                            
                            print(self.arrTeamData)
                            
                            
                            if(self.arrTeamData.count > 0){
                                
                                self.lblStatus.isHidden = true
                                self.tblEmployeeList.isHidden = false
                                self.tblEmployeeList.reloadData()
                                
                            }else{
                                self.lblStatus.isHidden = false
                                self.tblEmployeeList.isHidden = true
                            }
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    
                                    },{action2 in
                                    }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                            
                        }
                    }
                    
                }else{
                      OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
        
    }
}

extension aboutUsViewController : UITableViewDelegate , UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrTeamData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblEmployeeList.dequeueReusableCell(withIdentifier: "tblEmployeeList") as! tblEmployeeList
        let dict = arrTeamData[indexPath.section]
        cell.lblNAme.text = "\(dict["name"] as! String)"
        cell.lblEmail.text = "\(dict["email"] as! String)"
        let phone_ext = (dict["phone_ext"] as! String)
        if(phone_ext == ""){
            cell.lblPhone.text = "\(dict["phone"] as! String)"
        }
        else{
            cell.lblPhone.text = "(\(phone_ext)) \(dict["phone"] as! String)"
        }
        cell.lblDepartment.text = "\(dict["department"] as! String)"
        let strUrl = (dict["img"] as! String)
        if(strUrl.isValidForUrl())
        {
            cell.imgProfile.sd_setImage(with: URL.init(string: strUrl), placeholderImage: UIImage(named: "team_placeholder"))
        }
        else{
             cell.imgProfile.sd_setImage(with: URL.init(string: "https://www.houstondirectauto.com\(strUrl)"), placeholderImage: UIImage(named: "team_placeholder"))
        }
        return cell
    }
}

class tblEmployeeList: UITableViewCell {
    
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var lblNAme : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var lblDepartment : UILabel!
}
