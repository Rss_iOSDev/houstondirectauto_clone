//
//  chatViewController.swift
//  Houston
//
//  Created by Reinforce on 14/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit
import MessageKit
import SwiftWebSocket

class chatViewController: UIViewController {
    

    @IBOutlet weak var txtFname: RCustomTextField!
    @IBOutlet weak var txtLastName: RCustomTextField!
    @IBOutlet weak var txtEmail: RCustomTextField!
    @IBOutlet weak var txtNumber: RCustomTextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.ConnectToSocket()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
       }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnBack(_ sender : UIButton){
        //  self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func clickOnsubmitBtn(_ sender : UIButton){
        if(txtFname.text == "" || txtFname.text?.count == 0 || txtFname.text == nil){
            txtFname.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill first name field.")
        }
        else if(txtLastName.text == "" || txtLastName.text?.count == 0 || txtLastName.text == nil){
                
            txtLastName.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill last name field.")
        }
        else if(txtEmail.text == "" || txtEmail.text?.count == 0 || txtEmail.text == nil){
            
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill email field.")
        }
        else if(!(Globalfunc.isValidEmail(testStr: txtEmail.text!))){
            
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill valid email field.")
            
        }
        else if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
            self.callSubmitChatForm()
        } else {
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
}

extension chatViewController {
    
    func callSubmitChatForm(){
        
        let param = ["name":"\(txtFname.text!) \(txtLastName.text!)",
            "phone":"\(txtNumber.text!)",
            "email":"\(txtEmail.text!)"]
        
        webServices.onResponsePostData(url: "\(Constant.start_chat)", parms: param) { (dict, error) in
            if(error == ""){
                
                Globalfunc.print(object: dict)
                let status = dict["status"] as! String
                
                
                    if(status == "success"){
                        OperationQueue.main.addOperation {
                            
                           
                            
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            let chatDict = dict["chat"] as! NSDictionary
                            userDefault.set(chatDict, forKey: "statstChat")
                          //  userDefault.setValue("chatForm", forKey: "chatForm")
                            
                            
                            
                            let chatData = dict["chat"] as! NSDictionary
                            
                            
                            //let senderName  = chatData["name"] as! String
                            
                            let uid = chatData["uid"] as! Int
                            userDefault.set(uid, forKey: "uid")
                            
                            userDefault.setValue(dict, forKey: "receiveJson")
                            
                           // userChatSender = Sender(id: "1", displayName: "\(senderName)")
                            
                            
                                    self.txtEmail.text = ""
                                    self.txtNumber.text = ""
                                    self.txtLastName.text = ""
                                    self.txtFname.text = ""

                                    self.sendDetailsToSocket(dict: chatData)
                                    
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                        }
                    }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
    func sendDetailsToSocket(dict : NSDictionary){
    
        let uid = dict["uid"] as! Int
        let name = dict["name"] as! String
         let email = dict["email"] as! String
        let jsonObject: [String: Any] = [
            "action":"subscribe",
            "room":"hdapay",
            "from": [
                "uid":uid,
                "type":"customer",
                "name":"\(name)",
                "email":"\(email)"
            ]
        ]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted)
            socket.send(jsonData)
            
            isOpenChatFirst = "first"
          //  let chat = self.storyboard?.instantiateViewController(withIdentifier: "supportChatViewController") as! supportChatViewController
          //  self.navigationController?.pushViewController(chat, animated: true)
            
        } catch let error as NSError {
            Globalfunc.print(object: error)
        }
    }
    
    func ConnectToSocket() {
        
        socket.event.open = {
            Globalfunc.print(object: "opened")
        }
        
        socket.event.close = { code, reason, clean in
            userDefault.removeObject(forKey: "uid")
            Globalfunc.print(object: "closed")
        }
        
        socket.event.error = { error in
            Globalfunc.print(object: "error \(error)")
        }
        socket.event.message = { message in
            if let text = message as? String {
                self.handleMessage(jsonString: text)
            }
        }
    }
    
    func handleMessage(jsonString:String){
        if let data = jsonString.data(using: String.Encoding.utf8){
            do {
                let JSON = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                Globalfunc.print(object: "We've successfully parsed the message into a Dictionary! Yay!\n\(JSON)")
                
                
            } catch let error{
                Globalfunc.print(object: "Error parsing json: \(error)")
            }
        }
    }
    
    
}
