import UIKit
import AMPopTip

class favriotesViewController: UIViewController {
    
    @IBOutlet weak var tblFavList : UITableView!
    @IBOutlet weak var lblStatus: UILabel!
    
     var arrFavItem : NSMutableArray = []
    var popTip = PopTip()
    
    //let realm = try? Realm()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
           self.callNotificationListApi()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }

    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func callNotificationListApi(){
        
        arrFavItem = []
        Globalfunc.showLoaderView(view: self.view)
        var postString = ""
      
        if let email = userDefault.object(forKey: "email_fav"){
            if let authToken = userDefault.value(forKey: "authToken") {
                postString = "\(Constant.favorite_List_Url)?token=\(authToken)&email=\(email)"
                Globalfunc.print(object: postString)
                webServices.callApiRequestForGet(url: postString) { (dict, error) in
                    if(error == ""){
                        Globalfunc.print(object: dict)
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            let arr = dict["fav_list"] as! [NSDictionary]
                            if(arr.count > 0){
                                for i in 0...arr.count - 1 {
                                    let dict = arr[i]
                                    self.arrFavItem.add(dict)
                                }
                                self.lblStatus.isHidden = true
                                self.tblFavList.isHidden = false
                                self.tblFavList.reloadData()
                                
                                if(searchIndex != nil){
                                    let indexPath = NSIndexPath(row: searchIndex, section: 0)
                                    self.tblFavList.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                                    searchIndex = nil
                                }
                                
                            }else{
                                self.lblStatus.isHidden = false
                                self.lblStatus.text = "No record found.."
                                self.tblFavList.isHidden = true
                            }
                        }
                    }
                }
            }
        }
        else{
            
            self.lblStatus.isHidden = true
            self.tblFavList.isHidden = true
            
            Globalfunc.hideLoaderView(view: self.view)
            
            let alertController = UIAlertController(title: Constant.AppName, message: "Add Favorite", preferredStyle: .alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Email"
            }
            let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
                let firstTextField = alertController.textFields![0] as UITextField
                let secondTextField = alertController.textFields![1] as UITextField
                
                userDefault.set(firstTextField.text!, forKey: "email_fav")
                userDefault.set(secondTextField.text!, forKey: "phone_fav")
                self.viewWillAppear(true)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (action : UIAlertAction!) -> Void in })
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Phone"
            }
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension favriotesViewController : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFavItem.count       //DB.Manager.sharedInstance.getDataFromDB().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblFavList.dequeueReusableCell(withIdentifier: "favTblCell") as! favTblCell
        let dict = arrFavItem[indexPath.row] as! NSDictionary
        cell.lblYear.text = "\(dict["year"] as! String) \(dict["make"] as! String) \(dict["model"] as! String)"
        
        
        let PriceStr = (dict["price"] as! String)
        cell.lblPrice.text = "Price: \(PriceStr.currencyFormatting())"
        cell.lblTrim.text = "Trim: \((dict["series"] as! String))"
        let strMiles = (dict["odometer"] as! String)
        cell.lblMiles.text = "Miles: \(strMiles.milesFormatting())"
        cell.lblVinNo.text = (dict["vin"] as! String)
        cell.lblStock.text = "Stock: #\((dict["stock"] as! String))"
        cell.imgVehicles.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
        cell.imgVehicles.setShowActivityIndicator(true)
        cell.imgVehicles.sd_setImage(with: URL.init(string: "https://hda.s3.us-east-2.amazonaws.com/\((dict["vin"] as! String).lowercased())/1_245x183.jpg"), placeholderImage: UIImage(named: ""))
        
        
        if let vrank = dict["vrank"] as? String{
            let vrank_of = dict["vrank_of"] as! String
            
            Globalfunc.print(object: "vrank_of----\(vrank_of)")
            
            if(Int(vrank_of)! > 0){
                let vrank_cal = Int(vrank)! * 100 / Int(vrank_of)!
                if(vrank == "1"){
                    cell.dealViewHeight.constant = 35
                    cell.lblDEal.text = "GREAT DEAL"
                    cell.lblDEal.textColor = UIColor.init(red: 6/255, green: 255/255, blue: 2/255, alpha: 1)
                    cell.imgDealArrow.image = UIImage(named: "great")
                    // let popTip = PopTip()
                    // popTip.show(text: "FIND A BETTER PRICE IN TOWN FROM A COMPETITOR AND WE WILL BEAT IT.", direction: .up, maxWidth: 230, in: cell.contentView, from: cell.btnTap.frame)
                    cell.btnTap.isEnabled = true
                    cell.btnTap.tag = indexPath.row
                    cell.btnTap.addTarget(self, action: #selector(clickOn(_:)), for: UIControl.Event.touchUpInside)
                }
                else if(vrank != "0" && vrank_of != "0" &&  vrank_cal <= 15){
                    cell.dealViewHeight.constant = 35
                    cell.lblDEal.text = "GOOD DEAL"
                    cell.lblDEal.textColor = UIColor.init(red: 2/255, green: 144/255, blue: 1/255, alpha: 1)
                    cell.imgDealArrow.image = UIImage(named: "good")
                    cell.btnTap.isEnabled = false
                }
                else{
                    cell.dealViewHeight.constant = 0
                    cell.lblDEal.text = ""
                    cell.imgDealArrow.image = nil
                    cell.btnTap.isEnabled = false
                }
            }
            /// set condition if vrank is more than 0 what should here come
        }
        else{
            cell.dealViewHeight.constant = 0
            cell.lblDEal.text = ""
            cell.imgDealArrow.image = nil
            cell.btnTap.isEnabled = false
        }

        

        let carfax_one_owner = dict["carfax_one_owner"] as! String
        if(carfax_one_owner == "1"){
                cell.imgCarFax.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                cell.imgCarFax.setShowActivityIndicator(true)
                cell.imgCarFax.sd_setImage(with: URL.init(string: "https:houstondirectauto.com/wp-content/themes/houstondirectauto/assets/images/carfax_1owner.jpg"), placeholderImage: UIImage(named: ""))
        }
        else{
                cell.imgCarFax.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                cell.imgCarFax.setShowActivityIndicator(true)
                cell.imgCarFax.sd_setImage(with: URL.init(string: "https://s3.us-east-2.amazonaws.com/hda/assets/carfax-free.jpg"), placeholderImage: UIImage(named: ""))
        }
        
        cell.imgCarFax.tag = indexPath.row
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        cell.imgCarFax.isUserInteractionEnabled = true
        cell.imgCarFax.addGestureRecognizer(tapGestureRecognizer)
        
        return cell
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer)
    {
        let imgView = sender.view as! UIImageView
        
        let dict = self.arrFavItem[imgView.tag] as! NSDictionary
        let vin = dict["vin"] as! String

        let urlCarfax = "https://www.carfax.com/cfm/ccc_DisplayHistoryRpt.cfm?partner=WDB_0&vin=\(vin)"
        guard let url = URL(string: urlCarfax) else { return }
        UIApplication.shared.open(url)

    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    @objc func clickOn(_ sender: UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblFavList)
        let indexPath = self.tblFavList.indexPathForRow(at: buttonPosition)
        let currentCell = tblFavList.cellForRow(at: indexPath!)! as! favTblCell
        
        popTip.bubbleColor = UIColor.black
        // popTip.show(text: "FIND A BETTER PRICE IN TOWN FROM A COMPETITOR AND WE WILL BEAT IT.", direction: .none, maxWidth: 280, in: currentCell.viewBg, from: sender.frame)
        
        let cusView = UIView(frame: CGRect(x: 8, y: 150, width: currentCell.btnTap.frame.width, height: 55))
        cusView.backgroundColor = UIColor.black
        let lbl = UILabel(frame: CGRect(x: 2, y: 2, width: cusView.frame.width, height: cusView.frame.height))
        lbl.text = "FIND A BETTER PRICE IN TOWN FROM A COMPETITOR AND WE WILL BEAT IT."
        lbl.textColor = UIColor.white
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        cusView.addSubview(lbl)
        popTip.show(customView: cusView, direction: .down, in: currentCell.viewBg, from: sender.frame,duration: 2)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete){

            let dict = arrFavItem[indexPath.row] as! NSDictionary
            let vin = dict["vin"] as! String
            
            if let authToken = userDefault.value(forKey: "authToken") {
                let email = userDefault.value(forKey: "email_fav") as! String
                let phone = userDefault.value(forKey: "phone_fav") as! String
                
                var postString = ""
                if let device_token = userDefault.value(forKey: "device_token") as? String{
                    postString = "\(Constant.Remove_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token=\(device_token)"
                }     //userDefault.value(forKey: "device_token") as! String
                else{
                    postString = "\(Constant.Remove_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token="
                }
                
                
                
                webServices.callApiRequestForGet(url: postString) { (dict, error) in
                    if(error == ""){
                        Globalfunc.print(object: dict)
                        OperationQueue.main.addOperation {
                            let arr = dict["fav_list"] as! [NSDictionary]
                            Globalfunc.print(object: arr)
                            if(arr.count == 0){
                                self.tblFavList.isHidden = true
                                self.lblStatus.isHidden = false
                                userDefault.removeObject(forKey: "favArr")
                            }
                            else{
                                self.arrFavItem = []
                                for i in 0...arr.count - 1 {
                                        let dict = arr[i]
                                        self.arrFavItem.add(dict)
                                }
                                    
                                self.lblStatus.isHidden = true
                                self.tblFavList.isHidden = false
                                self.tblFavList.reloadData()
                                //self.tblFavList.deleteRows(at: [indexPath], with: .fade)
                                
                            }
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Vehicle has been removed from your favorite list successfully!")
                            
                           // self.tblFavList.reloadData()
                        }
                        
                    }else{
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object: error)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrFavItem[indexPath.row] as! NSDictionary
        searchIndex = indexPath.row
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "VehicleDetailViewController") as! VehicleDetailViewController
        detail.vIn = dict["vin"] as! String
        self.navigationController?.pushViewController(detail, animated: true)
    }
}

class favTblCell: UITableViewCell{
    @IBOutlet weak var imgVehicles: UIImageView!
    @IBOutlet weak var imgCarFax: UIImageView!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTrim: UILabel!
    @IBOutlet weak var lblMiles: UILabel!
    @IBOutlet weak var lblVinNo: UILabel!
    @IBOutlet weak var lblStock: UILabel!
    @IBOutlet weak var lblDEal: UILabel!
    @IBOutlet weak var imgDealArrow: UIImageView!
    @IBOutlet weak var dealViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var viewBg: UIView!
}
