//
//  messageViewController.swift
//  Houston
//
//  Created by Reinforce on 04/03/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit
import SwiftWebSocket
import MessageKit
import MessageInputBar

class messageViewController: MessagesViewController {
    
    @IBOutlet weak var headerImageHeightConstraint : NSLayoutConstraint!
    
    
    var msgDetail : NSDictionary = [:]
    var messageList: [MockMessage] = []
    let refreshControl = UIRefreshControl()
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.configureMessageCollectionView()
        self.configureMessageInputBar()
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor(red: 60/255, green: 131/255, blue: 195/255, alpha: 1)
        let back = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(clickOnBack(_:)))
        navigationItem.leftBarButtonItem = back
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.ConnectToSocket()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @objc func clickOnBack(_ sender : UIButton){
        
       // socket.event.close = { code, reason, clean in
            //userDefault.removeObject(forKey: "uid")
           // userDefault.removeObject(forKey: "statstChat")
        //    Globalfunc.print(object: "closed")
      //  }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
  
    
    func callSupportMsgByUid(){

        var uIDD : String!
        if let uID = userDefault.object(forKey: "uid") as? String{
            uIDD = uID
        }
        if let uID = userDefault.object(forKey: "uid") as? Int{
            uIDD = "\(uID)"
        }
        let param = ["uid":"\(uIDD!)"]

        webServices.onResponsePostData(url: "\(Constant.URL_chats_by_uid)", parms: param) { (dict, error) in
            if(error == ""){

                Globalfunc.print(object: dict)
                let status = dict["status"] as! String


                if(status == "success"){
                    OperationQueue.main.addOperation {

                        Globalfunc.hideLoaderView(view: self.view)
                        
                        let support = dict["messages"] as! [NSDictionary]
                        self.getAllMessage(arr: support)
                        

                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)

                    }
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)

                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
    func getAllMessage(arr : [NSDictionary]) {
        
        for i in 0..<arr.count {
            
            let dict = arr[i]
            Globalfunc.print(object: dict)
            
            let chat_id = dict["chat_id"] as! String
            let id = dict["id"] as! String
            
            var msg : String!
            if let message = dict["message"] as? String {
                msg = message
            }else{
                msg = ""
            }
            
            let sender_name = dict["sender_name"] as! String
            
            let date_time = dict["date_time"] as! String
            let date = Globalfunc.dateAddingRandomTime()
            
            
            userChatReceiver = Sender(id: "2", displayName: "\(sender_name)")
            
            let messageL = MockMessage(text: "\(msg!)", sender: userChatReceiver, messageId: "\(id)", date: date, chat_id: chat_id, message: "\(msg!)", sender_name: "\(sender_name)", date_time: date_time)
                
            messageList.append(messageL)
                
                
           
           
            self.messagesCollectionView.reloadData()
            self.messagesCollectionView.scrollToBottom()
 
            
            
            
        }
    }
    

    
    @objc
    func loadMoreMessages() {
//        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 1) {
//            self.getAllMessage(count: 10) { messages in
//                DispatchQueue.main.async {
//                    self.messageList.insert(contentsOf: messages, at: 0)
//                    self.messagesCollectionView.reloadDataAndKeepOffset()
//                    self.refreshControl.endRefreshing()
//                }
//            }
//        }
    }
    
    func configureMessageCollectionView() {
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messageCellDelegate = self
        
        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false
        
        messagesCollectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(loadMoreMessages), for: .valueChanged)
    }
    
    func configureMessageInputBar() {
        messageInputBar.delegate = self
        messageInputBar.inputTextView.tintColor = .black
        messageInputBar.sendButton.tintColor = .blue
    }
    
    func ConnectToSocket() {
        
        socket.event.open = {
            Globalfunc.print(object: "opened")
        }
        
       
        socket.event.error = { error in
            Globalfunc.print(object: "error \(error)")
        }
        
        socket.event.message = { message in
            if let text = message as? String {
                self.handleMessage(jsonString: text)
            }
        }
        
        
    }

    
    func handleMessage(jsonString:String){
        if let data = jsonString.data(using: String.Encoding.utf8){
            do {
                let JSON = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                Globalfunc.print(object: "We've successfully parsed the message into a Dictionary! Yay!\n\(JSON)")
                
                let dict = JSON as! [String : Any]
                 let action = dict["action"] as! String
                Globalfunc.print(object: action)
                 
                if(action == "txt"){
                    
                   
                    
                    let fromDict = dict["from"] as! NSDictionary
                     let sender_name = fromDict["name"] as! String
                  //  sender = fromDict["type"] as! String
                    
                    
                    let msgid = dict["msgid"] as! Int
                    
                    var msg : String!
                    if let message = dict["msg"] as? String {
                        msg = message
                    }else{
                        msg = ""
                    }
                    
                    let toDict = dict["to"] as! NSDictionary
                    let chat_id = toDict["uid"] as! String
                   
                    
                    
                    let date_time = dict["date"] as! String
                    let date = Globalfunc.dateAddingRandomTime()
                    
                    
                    userChatReceiver = Sender(id: "2", displayName: "\(sender_name)")
                    let messageL = MockMessage(text: "\(msg!)", sender: userChatReceiver, messageId: "\(msgid)", date: date, chat_id: "\(chat_id)", message: "\(msg!)", sender_name: "\(sender_name)", date_time: date_time)
                        
                    messageList.append(messageL)
                        
                    self.messagesCollectionView.reloadData()
                    self.messagesCollectionView.scrollToBottom()
                    
                }
                
                if(action == "newSub"){
                    
                    let userDict = dict["user"] as! NSDictionary
                    var uidd : String!
                    if let UID = userDict["uid"] as? String{
                        uidd = UID
                    }
                    if let UID = userDict["uid"] as? Int{
                        uidd = "\(UID)"
                    }
                   
                    userDefault.set(uidd, forKey: "uid")
                    self.callSupportMsgByUid()
                    
                }
                
                else if(action == "imOffline"){
                   Globalfunc.print(object: "imOffline")
                    
                   // userDefault.removeObject(forKey: "uid")
                   // userDefault.removeObject(forKey: "statstChat")
                    
                 //   self.navigationController?.popViewController(animated: true)
                }
                
            } catch let error{
                Globalfunc.print(object: "Error parsing json: \(error)")
            }
        }
    }

}


extension messageViewController : MessagesDataSource , MessageCellDelegate{
   
    // MARK: - MessagesDataSource
    
    func currentSender() -> Sender {
        return SampleData.shared.currentSender
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section % 3 == 0 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
        return nil
    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
   
    
    //cell delegate
    func didTapAvatar(in cell: MessageCollectionViewCell) {
        Globalfunc.print(object: "Avatar tapped")
    }
    
    func didTapMessage(in cell: MessageCollectionViewCell) {
        Globalfunc.print(object: "Message tapped")
    }
    
    func didTapCellTopLabel(in cell: MessageCollectionViewCell) {
        Globalfunc.print(object: "Top cell label tapped")
    }
    
    func didTapMessageTopLabel(in cell: MessageCollectionViewCell) {
        Globalfunc.print(object: "Top message label tapped")
    }
    
    func didTapMessageBottomLabel(in cell: MessageCollectionViewCell) {
        Globalfunc.print(object: "Bottom label tapped")
    }
    
    func didTapAccessoryView(in cell: MessageCollectionViewCell) {
        Globalfunc.print(object: "Accessory view tapped")
    }
}


extension messageViewController: MessageLabelDelegate {
    
    func didSelectAddress(_ addressComponents: [String: String]) {
        Globalfunc.print(object: "Address Selected: \(addressComponents)")
    }
    
    func didSelectDate(_ date: Date) {
        Globalfunc.print(object: "Date Selected: \(date)")
    }
    
    func didSelectPhoneNumber(_ phoneNumber: String) {
        Globalfunc.print(object: "Phone Number Selected: \(phoneNumber)")
    }
    
    func didSelectURL(_ url: URL) {
        Globalfunc.print(object: "URL Selected: \(url)")
    }
    
    func didSelectTransitInformation(_ transitInformation: [String: String]) {
        Globalfunc.print(object: "TransitInformation Selected: \(transitInformation)")
    }
    
}

// MARK: - MessageInputBarDelegate

extension messageViewController: MessageInputBarDelegate {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        
        for component in inputBar.inputTextView.components {
            
            if let str = component as? String {
             
                //..have to mantain session for data from action txt
                
                let dict = userDefault.value(forKey: "receiveJson") as! [String:Any]
                Globalfunc.print(object: dict)
                  let chat = dict["chat"] as! NSDictionary
                
                var uidd : String!
                if let UID = chat["uid"] as? String{
                    uidd = UID
                }
                if let UID = chat["uid"] as? Int{
                    uidd = "\(UID)"
                }
                let name  = chat["name"] as! String
                
                let data = dict["data"] as! NSDictionary
                var msgid : String!
                if let MID = data["msgid"] as? String{
                    msgid = MID
                }
                if let MID = data["msgid"] as? Int{
                    msgid = "\(MID)"
                }
               

                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
                let myString = formatter.string(from: Date())
                let yourDate = formatter.date(from: myString)
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let current_DateTime = formatter.string(from: yourDate!)
                
                
                
                userChatSender = Sender(id: "1", displayName: "\(name)")
                
                let message = MockMessage(text: "\(str)", sender: currentSender(), messageId: "\(msgid!)", date: Date(), chat_id: "\(uidd!)", message: "\(str)", sender_name: "\(name)", date_time: current_DateTime)
                insertMessage(message)
                self.saveMessageToApi(txt: str, senderName: name)
               
            }
            
        }
        inputBar.inputTextView.text = String()
        self.messagesCollectionView.reloadData()
        messagesCollectionView.scrollToBottom(animated: true)
    }
    
    func saveMessageToApi(txt : String, senderName:String){
        
        var uid = ""
        if  let uID = userDefault.object(forKey: "uid") as? String {
            uid = uID
        }
        
        if  let uID = userDefault.object(forKey: "uid") as? NSNumber {
            uid = "\(uID)"
        }
        
       // let uID = userDefault.object(forKey: "uid") as! String
        let param = ["chat_id":"\(uid)",
                    "sender":"customer",
                    "sender_name":"\(senderName)",
                    "msg":"\(txt)"]
        
        
        webServices.onResponsePostData(url: "\(Constant.URL_chat_send)", parms: param) { (dict, error) in
            if(error == ""){
                
                Globalfunc.print(object: dict)
                let status = dict["status"] as! String
                
                if(status == "success"){
                    OperationQueue.main.addOperation {
                        
                        Globalfunc.hideLoaderView(view: self.view)
                        self.sendMsgToSocket(dictResponse: dict)
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                    }
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
    func sendMsgToSocket(dictResponse : [String:Any]){
        
        
        let dict = userDefault.value(forKey: "receiveJson") as! [String:Any]
        let chat = dict["chat"] as! NSDictionary
        let email = chat["email"] as! String
        
        
        let chatData = dictResponse["chat_data"] as! NSDictionary
        let msgid = dictResponse["msgid"] as! Int

        let uid  = chatData["chat_id"] as! String
        let name  = chatData["sender_name"] as! String
       // let sender  = chatData["sender"] as! String
    
        let message = chatData["message"] as! String

        let date = Date()
        
        let jsonObject: [String: Any] = [
            "action":"txt",
            "room":"hdapay",
            "from": [
                "uid":uid,
                "type":"customer",
                "name":"\(name)",
                "email":"\(email)"
            ],
            "msg" :"\(message)",
            "msgid" :"\(msgid)",
            "date": "\(date)" ]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted)
            socket.send(jsonData)
        } catch let error as NSError {
            Globalfunc.print(object: error)
        }
    }
    
    func insertMessage(_ message: MockMessage) {
        messageList.append(message)
        // Reload last section to update header/footer labels and insert a new one
        messagesCollectionView.performBatchUpdates({
            messagesCollectionView.insertSections([messageList.count - 1])
            if messageList.count >= 2 {
                messagesCollectionView.reloadSections([messageList.count - 2])
            }
        }, completion: { [weak self] _ in
            if self?.isLastSectionVisible() == true {
                self?.messagesCollectionView.scrollToBottom(animated: true)
            }
        })
    }
    
    func isLastSectionVisible() -> Bool {
        
        guard !messageList.isEmpty else { return false }
        let lastIndexPath = IndexPath(item: 0, section: messageList.count - 1)
        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }
    
    
}

