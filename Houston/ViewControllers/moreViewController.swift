//
//  moreViewController.swift
//  Houston
//
//  Created by Reinforce on 08/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class moreViewController: UIViewController {
    
    @IBOutlet weak var tblMenuBar : UITableView!
        
    var arrMenuList : NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   let dict1 = ["img":"home","name":"Home"]
        let dict2 = ["img":"cars","name":"Vehicles"]
        let dict3 = ["img":"chat","name":"Chat"]
        let dict4 = ["img":"aboutus","name":"About Us"]
        let dict5 = ["img":"starfill","name":"Favorite Car"]
        
       // arrMenuList.add(dict1)
        arrMenuList.add(dict2)
        arrMenuList.add(dict3)
        arrMenuList.add(dict4)
        arrMenuList.add(dict5)
        
        tblMenuBar.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
""    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    @IBAction func clickOnHomeLogo(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
    }
    
}

extension moreViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblMenuBar.dequeueReusableCell(withIdentifier: "menuTblCell") as! menuTblCell
        let dict = arrMenuList[indexPath.row] as! NSDictionary
        cell.lblItemName.text = (dict["name"] as! String)
        cell.imgItem.image = UIImage(named: dict["img"] as! String)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == 0){
            let chat = self.storyboard?.instantiateViewController(withIdentifier: "formViewController") as! formViewController
            self.navigationController?.pushViewController(chat, animated: true)
        }
        else if(indexPath.row == 1){
            
            let chat = self.storyboard?.instantiateViewController(withIdentifier: "podiumChatViewController") as! podiumChatViewController
            self.navigationController?.pushViewController(chat, animated: true)
            
//            let session = userDefault.object(forKey: "session") as? String
//            if(session == "" || session == nil){
//
//                if let dictt = userDefault.value(forKey: "statstChat") {
//
//                    let dataDict = dictt as! NSDictionary
//
//                    let name = dataDict["name"] as! String
//                    let email = dataDict["email"] as! String
//                    //let name = dataDict["name"] as! String
//
//                    let param = ["name":"\(name)",
//                        "phone":"",
//                        "email":"\(email)"]
//
//                    self.callStartChatApi(param: param)
//
//                    //let chat = self.storyboard?.instantiateViewController(withIdentifier: "supportChatViewController") as! supportChatViewController
//                    //self.navigationController?.pushViewController(chat, animated: true)
//                }
//                else{
//                    let chat = self.storyboard?.instantiateViewController(withIdentifier: "chatViewController") as! chatViewController
//                    self.navigationController?.pushViewController(chat, animated: true)
//                }
//            }
//            else{
//
//
//                if let _ = userDefault.value(forKey: "isSendDetails") {
//                    let chat = self.storyboard?.instantiateViewController(withIdentifier: "supportChatViewController") as! supportChatViewController
//                    self.navigationController?.pushViewController(chat, animated: true)
//                }
//                else{
//
//                    do {
//                        let decoded  = userDefault.object(forKey: "userinfo") as! Data
//                        if let userDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary {
//
//                            Globalfunc.print(object: userDict)
//                            let fname = userDict["first_name"] as! String
//                            let lname = userDict["last_name"] as! String
//
//                            let email = userDict["email"] as! String
//                            let phone = userDict["phone"] as! String
//
//                            let param = ["name":"\(fname) \(lname)",
//                                "phone":"\(phone)",
//                                "email":"\(email)"]
//                            self.callStartChatApi(param: param)
//                        }
//                    }
//                    catch{
//
//                    }
//                }
//
//
//                }
           
        }
        else if(indexPath.row == 2){
            let about = self.storyboard?.instantiateViewController(withIdentifier: "aboutUsViewController") as! aboutUsViewController
            self.navigationController?.pushViewController(about, animated: true)
        }
        else if(indexPath.row == 3){
            let about = self.storyboard?.instantiateViewController(withIdentifier: "favriotesViewController") as! favriotesViewController
            self.navigationController?.pushViewController(about, animated: true)
        }
    }
    
    
    func callStartChatApi(param : [String:String]){
        
        webServices.onResponsePostData(url: "\(Constant.start_chat)", parms: param) { (dict, error) in
            if(error == ""){
                
                Globalfunc.print(object: dict)
                let status = dict["status"] as! String
                
                
                if(status == "success"){
                    OperationQueue.main.addOperation {
                        
                        Globalfunc.hideLoaderView(view: self.view)
                        let chatData = dict["chat"] as! NSDictionary
                        let uid = chatData["uid"] as! Int
                        userDefault.set(uid, forKey: "uid")
                        
                        userDefault.set("true", forKey: "isSendDetails")
                        userDefault.setValue(dict, forKey: "receiveJson")
                        
                        self.sendDetailsToSocket(dict: chatData)
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                    }
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
    func sendDetailsToSocket(dict : NSDictionary){
        
        let uid = dict["uid"] as! Int
        let name = dict["name"] as! String
        let email = dict["email"] as! String
        let jsonObject: [String: Any] = [
            "action":"subscribe",
            "room":"hdapay",
            "from": [
                "uid":uid,
                "type":"customer",
                "name":"\(name)",
                "email":"\(email)"
            ]
        ]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted)
            socket.send(jsonData)
            
           // let chat = self.storyboard?.instantiateViewController(withIdentifier: "supportChatViewController") as! supportChatViewController
          //  self.navigationController?.pushViewController(chat, animated: true)
            
        } catch let error as NSError {
            Globalfunc.print(object: error)
        }
    }
}

class menuTblCell : UITableViewCell{
    
    @IBOutlet weak var lblItemName : UILabel!
    @IBOutlet weak var imgItem : UIImageView!
    
}
