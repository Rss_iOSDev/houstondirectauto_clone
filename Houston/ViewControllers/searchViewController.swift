import UIKit
import SDWebImage
import AMPopTip

class searchViewController: UIViewController {
    
    @IBOutlet weak var tblSearchList : UITableView!
    @IBOutlet weak var txtSearch : UITextField!
        @IBOutlet weak var lblSearchRecord: UILabel!
    @IBOutlet weak var viewFooter : UIView!
    
    @IBOutlet weak var viewAddFav : UIView!
    @IBOutlet weak var txtEmailFav : UITextField!
    @IBOutlet weak var txtPhoneFav : UITextField!
    @IBOutlet weak var btnSaveFav : UIButton!
    
    @IBOutlet weak var spinner : UIActivityIndicatorView!
    
    var popTip = PopTip()
    
    var scrollIndex = 1
    @IBOutlet weak var txtSort : UITextField!
    var pickerviewSort : UIPickerView!
    
    var arrSearchData : NSMutableArray = []
    var pickerSortData : NSMutableArray = []
    
    var isSortTypeVal = ""
    var sortDirection = ""
    
    
    // var currentItem:FavouriteListRealm?
    //let realm = try? Realm()
    var arrFavSelectItem = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Globalfunc.print(object: Realm.Configuration.defaultConfiguration.fileURL!)
        
        let dict1 = ["name":"Price(Low to High)","value":"price"]
        let dict2 = ["name":"Price(High to Low)","value":"price"]
        let dict3 = ["name":"Year(Low to High)","value":"year"]
        let dict4 = ["name":"Year(High to Low)","value":"year"]
        let dict5 = ["name":"MPG(Low to High)","value":"city_mpg"]
        let dict6 = ["name":"MPG(High to Low))","value":"city_mpg"]
        let dict7 = ["name":"Mileage(Low to High))","value":"odometer"]
        let dict8 = ["name":"Mileage(High to Low)","value":"odometer"]
        let dict9 = ["name":"Newest","value":"inventory_dt"]
        let dict10 = ["name":"Oldest","value":"inventory_dt"]
        
        pickerSortData.add(dict1)
        pickerSortData.add(dict2)
        pickerSortData.add(dict3)
        pickerSortData.add(dict4)
        pickerSortData.add(dict5)
        pickerSortData.add(dict6)
        pickerSortData.add(dict7)
        pickerSortData.add(dict8)
        pickerSortData.add(dict9)
        pickerSortData.add(dict10)
        
        self.arrSearchData = []
        self.viewAddFav.isHidden = true
        self.lblSearchRecord.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.arrSearchData = []
        self.tblSearchList.reloadData()
        
        //call get fav Api
        let email = userDefault.value(forKey: "email_fav") as? String
        if(email == nil || email == ""){
            
        }else{
            self.callFavListApi(strEmail: email!)
        }
        
        isSortTypeVal = "price"
        sortDirection = "desc"
        
        self.txtSort.text = "Price(High to Low)"
        if(searchTag == "") || (searchTag == "3"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                //self.callAllSearchListApi(pooltoRefresh: true)
                self.txtSearch.text=""
                self.callSearchApi(searchTag: searchTag, strCkeck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
        else if(searchTag == "0"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.txtSearch.text = searchBarText
                //self.callSearchApiOnType(pooltoRefresh: true)
                self.callSearchApi(searchTag: searchTag, strCkeck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
            
        }
        else if(searchTag == "1"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearchApi(searchTag: searchTag, strCkeck: "get")
                self.txtSearch.text=""
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
            
        }
        else if(searchTag == "2"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearchApi(searchTag: "2", strCkeck: "get")
                self.txtSearch.text=""
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
            
        }
        else if(searchTag == "5"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearcApiwithFilter(strCheck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
        else if(searchTag == "7"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearchApi(searchTag: "7", strCkeck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
        else if(searchTag == "8"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearchApi(searchTag: "8", strCkeck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnHomeLogo(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func clickOnFileteBtn(_ sender: UIButton) {
        makeMultipleSelectArray = []
        modelMultipleSelectArray = []
        trimMultipleSelectArray = []
        let about = self.storyboard?.instantiateViewController(withIdentifier: "filterViewController") as! filterViewController
        self.navigationController?.pushViewController(about, animated: true)
    }
    
    func callFavListApi(strEmail : String){
        if let authToken = userDefault.value(forKey: "authToken") {
            let postString = "\(Constant.favorite_List_Url)?token=\(authToken)&email=\(strEmail)"
            Globalfunc.print(object: postString)
            webServices.callApiRequestForGet(url: postString) { (dict, error) in
                if(error == ""){
                    Globalfunc.print(object: dict)
                    OperationQueue.main.addOperation {
                        let arr = dict["fav_list"] as! [NSDictionary]
                        if arr.count > 0{
                            for i in 0...arr.count - 1 {
                                let dict = arr[i]
                                
                                if let vin_no = dict["vin"] as? String{
                                    if(self.arrFavSelectItem.contains(vin_no) == false){
                                        self.arrFavSelectItem.append(vin_no)
                                    }
                                }
                            }
                        }
                        else{
                            self.arrFavSelectItem = []
                        }
                    }
                }
            }
        }
    }
}

extension searchViewController{
    
    func callSearchApi(searchTag :String ,strCkeck: String){
        if(strCkeck == "get"){
            scrollIndex = 1
            self.arrSearchData = []
        }
        
        var postString = ""
        if(searchTag == "0"){
            if let authToken = userDefault.value(forKey: "authToken") {
                postString  = "\(Constant.search_Url)?token=\(authToken)&q=\(searchBarText)&page=\(scrollIndex)&sort=\(isSortTypeVal)&dir=\(sortDirection)"
                print(postString)
            }
        }
        else if(searchTag == "") || (searchTag == "3"){
            if let authToken = userDefault.value(forKey: "authToken") {
                postString = "\(Constant.search_Url)?token=\(authToken)&page=\(scrollIndex)&sort=\(isSortTypeVal)&dir=\(sortDirection)"
            }
        }
        else if(searchTag == "2"){
            if let authToken = userDefault.value(forKey: "authToken") {
                postString = "\(Constant.search_Url)?token=\(authToken)&pmo=\(searchPaymentText)&page=\(scrollIndex)&sort=\(isSortTypeVal)&dir=\(sortDirection)"
                
                
            }
        }
        else if(searchTag == "7"){
            if let authToken = userDefault.value(forKey: "authToken") {
                postString = "\(Constant.search_Url)?token=\(authToken)&location[]=houston direct auto north"
            }
        }
        else if(searchTag == "8"){
                if let authToken = userDefault.value(forKey: "authToken") {
                    postString = "\(Constant.search_Url)?token=\(authToken)&location[]=houston direct auto"
                }
        }

        else if(searchTag == "1"){
            if let authToken = userDefault.value(forKey: "authToken") {
                postString = "\(Constant.search_Url)?token=\(authToken)&page=\(scrollIndex)&make[]=\(makeString)&sort=\(isSortTypeVal)&dir=\(sortDirection)"
            }
        }
        Globalfunc.print(object: postString)
        webServices.callApiRequestForGet(url: postString) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object: dict)
                OperationQueue.main.addOperation {
                    let arr = dict["data"] as! Array<Any>
                    if(strCkeck == "get"){
                        if(arr.count == 0){
                            self.lblSearchRecord.isHidden = false
                            self.spinner.isHidden = true
                            self.lblSearchRecord.text = "No record found.."
                            self.tblSearchList.isHidden = true
                        }
                        else{
                            for dict in arr {
                                self.arrSearchData.add(dict as! NSDictionary)
                                Globalfunc.print(object: self.arrSearchData)
                            }
                            
                            self.lblSearchRecord.isHidden = true
                            self.tblSearchList.isHidden = false
                            self.tblSearchList.reloadData()
                            
                            if(searchIndex != nil){
                                let indexPath = NSIndexPath(row: searchIndex, section: 0)
                                self.tblSearchList.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                                searchIndex = nil
                            }
                        }
                    }
                    else{
                        if (arr.count > 0)
                        {
                            Globalfunc.print(object: self.arrSearchData)
                            
                            for dict in arr {
                            self.arrSearchData.add(dict as! NSDictionary)
                            
                            }
                            self.lblSearchRecord.isHidden = true
                            self.tblSearchList.isHidden = false
                            self.tblSearchList.reloadData()
                            
                            if(searchIndex != nil){
                                let indexPath = NSIndexPath(row: searchIndex, section: 0)
                                self.tblSearchList.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                                searchIndex = nil
                            }
                            
                        }
                        else{
                            self.spinner.stopAnimating()
                            self.lblSearchRecord.isHidden = false
                            self.spinner.isHidden = true
                            self.lblSearchRecord.text = "No more record found.."
                        }
                    }
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
    //    func saveListinRealm(dict : NSDictionary){
    //        let item = FavouriteListRealm(value: dict)
    //        if(currentItem == nil) {
    //            item.vid = dict["vid"] as! String
    //        }
    //        DBManager.sharedInstance.addData(object: item)
    //    }
    
    
    func callSearcApiwithFilter(strCheck: String){
        if strCheck == "get" {
            scrollIndex = 1
            self.arrSearchData = []
        }
        if let authToken = userDefault.value(forKey: "authToken") {
            
            var fullString: String = ""
            var make = ""
            var model = ""
            var trim = ""
            
            if makeMultipleSelectArray.count > 0{
                for value in makeMultipleSelectArray {
                    make +=  "\("make[]")=\(value)&"
                }
                make = String(make.dropLast())
            }
            
           // print(make)
            
            if modelMultipleSelectArray.count > 0{
                for value in modelMultipleSelectArray {
                    model +=  "\("model[]")=\(value)&"
                }
                model = String(model.dropLast())
                
            }
            if trimMultipleSelectArray.count > 0{
                for value in trimMultipleSelectArray {
                    let formattedVal = value.replacingOccurrences(of: " ", with: "+")
                    trim +=  "\("series[]")=\(formattedVal)&"
                }
                trim = String(trim.dropLast())
            }
            
            fullString = "\(make)&\(model)&\(trim)"
            Globalfunc.print(object: sliderParams)
            
            Globalfunc.print(object: fullString)
            let url = "\(Constant.search_Url)?token=\(authToken)&page=\(scrollIndex)&pmo=\(searchPaymentText)&q=\(searchBarText)&\(fullString)&sort=\(isSortTypeVal)&dir=\(sortDirection)&\(sliderParams)"
            
            Globalfunc.print(object: url)
            webServices.callApiRequestForGet(url: url) { (dict, error) in
                if(error == ""){
                    Globalfunc.print(object: dict)
                    OperationQueue.main.addOperation {
                        let arr = dict["data"] as! Array<Any>
                        if(strCheck == "get"){
                            if(arr.count == 0){
                                self.lblSearchRecord.isHidden = false
                                self.spinner.isHidden = true
                                self.lblSearchRecord.text = "No record found.."
                                self.tblSearchList.isHidden = true
                            }
                            else{
                                for dict in arr {
                                    self.arrSearchData.add(dict as! NSDictionary)
                                    Globalfunc.print(object: self.arrSearchData)
                                }
                                self.lblSearchRecord.isHidden = true
                                self.tblSearchList.isHidden = false
                                self.tblSearchList.reloadData()
                            }
                        }
                        else{
                            
                            if(arr.count > 0){
                                for dict in arr {
                                    self.arrSearchData.add(dict as! NSDictionary)
                                    Globalfunc.print(object: self.arrSearchData)
                                }
                                self.lblSearchRecord.isHidden = true
                                self.tblSearchList.isHidden = false
                                self.tblSearchList.reloadData()
                            }
                            else{
                                self.spinner.stopAnimating()
                                self.lblSearchRecord.isHidden = false
                                self.spinner.isHidden = true
                                self.lblSearchRecord.text = "No more record found.."
                            }
                        }
                    }
                    
                }else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: error)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                    }
                }
            }
        }
    }
}

extension searchViewController : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSearchData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSearchList.dequeueReusableCell(withIdentifier: "searchTblCell") as! searchTblCell
        let dict = self.arrSearchData[indexPath.row] as! NSDictionary
        
        print(dict)
        
        cell.lblYear.text = "\(dict["year"] as! String) \(dict["make"] as! String) \(dict["model"] as! String)"
        
        let priceStr = (dict["price"] as! String)
        cell.lblPrice.text = "Price: \(priceStr.currencyFormatting())"
        cell.lblTrim.text = "Trim: \((dict["series"] as! String))"
        
        let strMiles = (dict["odometer"] as! String)
        cell.lblMiles.text = "Miles: \(strMiles.milesFormatting())"
        
        cell.lblVinNo.text = (dict["vin"] as! String)
        cell.lblStock.text = "Stock: #\((dict["stock"] as! String))"
        cell.imgVehicles.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
        cell.imgVehicles.setShowActivityIndicator(true)
        cell.imgVehicles.sd_setImage(with: URL.init(string: "https://hda.s3.us-east-2.amazonaws.com/\((dict["vin"] as! String).lowercased())/1_245x183.jpg"), placeholderImage: UIImage(named: ""))
        // cell.btnFavriote.tag = indexPath.row
        cell.btnFavriote.addTarget(self, action: #selector(clickOnFavBtn(_:)), for: UIControl.Event.touchUpInside)
        
       
        Globalfunc.print(object: "https://hda.s3.us-east-2.amazonaws.com/\((dict["vin"] as! String).lowercased())/1_245x183.jpg")
        
        if let vrank = dict["vrank"] as? String{
            let vrank_of = dict["vrank_of"] as! String
            
            Globalfunc.print(object: "vrank_of----\(vrank_of)")
            
            if(Int(vrank_of)! > 0){
                let vrank_cal = Int(vrank)! * 100 / Int(vrank_of)!
                if(vrank == "1"){
                    cell.dealViewHeight.constant = 35
                    cell.lblDEal.text = "GREAT DEAL"
                    cell.lblDEal.textColor = UIColor.init(red: 6/255, green: 255/255, blue: 2/255, alpha: 1)
                    cell.imgDealArrow.image = UIImage(named: "great")
                    // let popTip = PopTip()
                    // popTip.show(text: "FIND A BETTER PRICE IN TOWN FROM A COMPETITOR AND WE WILL BEAT IT.", direction: .up, maxWidth: 230, in: cell.contentView, from: cell.btnTap.frame)
                    cell.btnTap.isEnabled = true
                    cell.btnTap.tag = indexPath.row
                    cell.btnTap.addTarget(self, action: #selector(clickOn(_:)), for: UIControl.Event.touchUpInside)
                }
                else if(vrank != "0" && vrank_of != "0" &&  vrank_cal <= 15){
                    cell.dealViewHeight.constant = 35
                    cell.lblDEal.text = "GOOD DEAL"
                    cell.lblDEal.textColor = UIColor.init(red: 2/255, green: 144/255, blue: 1/255, alpha: 1)
                    cell.imgDealArrow.image = UIImage(named: "good")
                    cell.btnTap.isEnabled = false
                }
                else{
                    cell.dealViewHeight.constant = 0
                    cell.lblDEal.text = ""
                    cell.imgDealArrow.image = nil
                    cell.btnTap.isEnabled = false
                }
                
            }
            
            /// set condition if vrank is more than 0 what should here come
        }
        else{
            cell.dealViewHeight.constant = 0
            cell.lblDEal.text = ""
            cell.imgDealArrow.image = nil
            cell.btnTap.isEnabled = false
        }
        
        if(arrFavSelectItem.count > 0){
            let vin_no = dict["vin"] as! String
            if(arrFavSelectItem.contains(vin_no) == false){
                cell.btnFavriote.isChecked = false
            }else{
                cell.btnFavriote.isChecked = true
            }
        }
        else{
            cell.btnFavriote.isChecked = false
        }
        
        
        if let carfax_one_owner = dict["carfax_one_owner"] as? String
        {
            if(carfax_one_owner == "1"){
                cell.imgCarFax.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                cell.imgCarFax.setShowActivityIndicator(true)
                cell.imgCarFax.sd_setImage(with: URL.init(string: "https:houstondirectauto.com/wp-content/themes/houstondirectauto/assets/images/carfax_1owner.jpg"), placeholderImage: UIImage(named: ""))
            }
            else{
                cell.imgCarFax.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                cell.imgCarFax.setShowActivityIndicator(true)
                cell.imgCarFax.sd_setImage(with: URL.init(string: "https://s3.us-east-2.amazonaws.com/hda/assets/carfax-free.jpg"), placeholderImage: UIImage(named: ""))
            }
        }
        
        cell.imgCarFax.tag = indexPath.row
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        cell.imgCarFax.isUserInteractionEnabled = true
        cell.imgCarFax.addGestureRecognizer(tapGestureRecognizer)
        
        return cell
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer)
    {
        let imgView = sender.view as! UIImageView
        
        let dict = self.arrSearchData[imgView.tag] as! NSDictionary
        let vin = dict["vin"] as! String
        
        let urlCarfax = "https://www.carfax.com/cfm/ccc_DisplayHistoryRpt.cfm?partner=WDB_0&vin=\(vin)"
        guard let url = URL(string: urlCarfax) else { return }
        UIApplication.shared.open(url)
        
    }
    
    @objc func clickOn(_ sender: UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblSearchList)
        let indexPath = self.tblSearchList.indexPathForRow(at: buttonPosition)
        let currentCell = tblSearchList.cellForRow(at: indexPath!)! as! searchTblCell
        
        popTip.bubbleColor = UIColor.black
        // popTip.show(text: "FIND A BETTER PRICE IN TOWN FROM A COMPETITOR AND WE WILL BEAT IT.", direction: .none, maxWidth: 280, in: currentCell.viewBg, from: sender.frame)
        
        let cusView = UIView(frame: CGRect(x: 8, y: 150, width: currentCell.btnTap.frame.width, height: 55))
        cusView.backgroundColor = UIColor.black
        let lbl = UILabel(frame: CGRect(x: 2, y: 2, width: cusView.frame.width, height: cusView.frame.height))
        lbl.text = "FIND A BETTER PRICE IN TOWN FROM A COMPETITOR AND WE WILL BEAT IT."
        lbl.textColor = UIColor.white
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        cusView.addSubview(lbl)
        popTip.show(customView: cusView, direction: .down, in: currentCell.viewBg, from: sender.frame,duration: 2)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrSearchData[indexPath.row] as! NSDictionary
        Globalfunc.print(object: dict)
        searchIndex = indexPath.row
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "VehicleDetailViewController") as! VehicleDetailViewController
        detail.vIn = dict["vin"] as! String
        detail.vID = dict["vid"] as! String
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == arrSearchData.count - 1 {
            //let totalHt = cell.frame.height * 4
            if(self.arrSearchData.count > 4){
                self.scrollIndex += 1
                self.lblSearchRecord.isHidden = true
                self.spinner.isHidden = false
                spinner.startAnimating()
                self.perform(#selector(loadTable), with: nil, afterDelay: 3.0)
            }
            else{
                self.spinner.stopAnimating()
                self.lblSearchRecord.isHidden = false
                self.spinner.isHidden = true
                self.lblSearchRecord.text = "No more record found.."
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }
    
    @objc func loadTable() {
        if(searchTag == "0"){
            self.callSearchApi(searchTag: "0", strCkeck: "load")
        }
        else if(searchTag == "1"){
            self.callSearchApi(searchTag: "1", strCkeck: "load")
        }
        else if(searchTag == "2"){
            self.callSearchApi(searchTag: "2", strCkeck: "load")
        }
        else if(searchTag == "") || (searchTag == "3"){
            self.callSearchApi(searchTag: searchTag, strCkeck: "load")
        }
        else if(searchTag == "5"){
            self.callSearcApiwithFilter(strCheck: "load")
        }
    }
    
    @objc func clickOnFavBtn(_ sender : UIButton){
        //maintain session here
        
        let session = userDefault.object(forKey: "session") as? String
        if(session == "" || session == nil){
            
            let savedEmail = userDefault.value(forKey: "email_fav") as? String
            if(savedEmail == "" || savedEmail == nil){
                
                self.viewAddFav.isHidden = false
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tblSearchList)
                let indexPath = self.tblSearchList.indexPathForRow(at: buttonPosition)
                btnSaveFav.tag = indexPath!.row
            }
            else {
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tblSearchList)
                let indexPath = self.tblSearchList.indexPathForRow(at: buttonPosition)
                let dict = self.arrSearchData[indexPath!.row] as! NSDictionary
                let vin = dict["vin"] as! String
                Globalfunc.print(object: vin)
                
                if(self.arrFavSelectItem.contains(vin) == false){
                    if let authToken = userDefault.value(forKey: "authToken") {
                        let email = userDefault.value(forKey: "email_fav") as! String
                        let phone = userDefault.value(forKey: "phone_fav") as! String
                        //   userDefault.value(forKey: "device_token") as! String
                        var postString = ""
                        if let device_token = userDefault.value(forKey: "device_token") as? String{
                            postString  = "\(Constant.Add_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token=\(device_token)"
                        }
                        else{
                            postString = "\(Constant.Add_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token="
                        }
                        self.callAddFavListApi(postString: postString)
                    }
                }
                else{
                    Globalfunc.print(object: "remove")
                    if let authToken = userDefault.value(forKey: "authToken") {
                        let email = userDefault.value(forKey: "email_fav") as! String
                        let phone = userDefault.value(forKey: "phone_fav") as! String
                        var postString = ""
                        if let device_token = userDefault.value(forKey: "device_token") as? String{
                            postString = "\(Constant.Remove_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token=\(device_token)"
                        }     //userDefault.value(forKey: "device_token") as! String
                        else{
                            postString = "\(Constant.Remove_favorite_Url)?token=\(authToken)&email=\(email)&vin=\(vin)&phone=\(phone)&ios_device_token="
                        }
                        self.callFavListRemoveApi(postString: postString, vin_no: vin)
                    }
                }
            }
        }
    }
    
    @IBAction func clickOnCancelBtn(_ sender : UIButton){
        self.viewAddFav.isHidden = true
    }
    
    @IBAction func clickOnSavebtn(sender: UIButton){
        //let buttonPosition = sender.convert(CGPoint.zero, to: self.tblSearchList)
        //let indexPath = self.tblSearchList.indexPathForRow(at: buttonPosition)
        let dict = self.arrSearchData[btnSaveFav.tag] as! NSDictionary
        Globalfunc.print(object: dict)
        let vin = dict["vin"] as! String
        if(self.arrFavSelectItem.contains(vin) == false){
            if let authToken = userDefault.value(forKey: "authToken") {
                
                let device_token = userDefault.value(forKey: "device_token") as? String
                //"6e134fad148d35d03330ccd08f4ee8bd5d2a73189ae41889907a4c1430774bd8"   //userDefault.value(forKey: "device_token") as! String
                let postString = "\(Constant.Add_favorite_Url)?token=\(authToken)&email=\(txtEmailFav.text!)&vin=\(vin)&phone=\(txtPhoneFav.text!)&ios_device_token=6e134fad148d35d03330ccd08f4ee8bd5d2a73189ae41889907a4c1430774bd8"
                self.callAddFavListApi(postString: postString)
            }
        }
    }
    
    func callAddFavListApi(postString: String){
        
        webServices.callApiRequestForGet(url: postString) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object: dict)
                OperationQueue.main.addOperation {
                    
                    let cell = self.tblSearchList.dequeueReusableCell(withIdentifier: "searchTblCell") as! searchTblCell
                    let arr = dict["fav_list"] as! [NSDictionary]
                    Globalfunc.print(object: arr)
                    self.viewAddFav.isHidden = true
                    
                    if(arr.count > 0){
                        for i in 0...arr.count - 1 {
                            let dict = arr[i]
                            let vin_no = dict["vin"] as! String
                            cell.btnFavriote.isChecked = true
                            self.arrFavSelectItem.append(vin_no)
                            userDefault.set(self.arrFavSelectItem, forKey: "favArr")
                        }
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Vehicle has been added to your favorite list successfully!")
                        if(self.txtEmailFav.text != ""){
                            userDefault.set(self.txtEmailFav.text!, forKey: "email_fav")
                            userDefault.set(self.txtPhoneFav.text!, forKey: "phone_fav")
                        }
                    }
                    
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
    
    func callFavListRemoveApi(postString: String , vin_no: String){
        
        webServices.callApiRequestForGet(url: postString) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object: dict)
                OperationQueue.main.addOperation {
                    
                    let cell = self.tblSearchList.dequeueReusableCell(withIdentifier: "searchTblCell") as! searchTblCell
                    self.viewAddFav.isHidden = true
                    cell.btnFavriote.isChecked = false
                    
                    
                    let arr = dict["fav_list"] as! [NSDictionary]
                    Globalfunc.print(object: arr)
                    if(arr.count == 0){
                        self.arrFavSelectItem = []
                    }
                    else{
                        if(!self.arrFavSelectItem.isEmpty){
                            
                            for i in self.arrFavSelectItem{
                                if(i == vin_no){
                                    self.arrFavSelectItem = self.arrFavSelectItem.filter{$0 != i}
                                }
                            }
                            userDefault.set(self.arrFavSelectItem, forKey: "favArr")
                        }
                    }
                    self.tblSearchList.reloadData()
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Vehicle has been removed from your favorite list successfully!")
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
}

class searchTblCell: UITableViewCell {
    @IBOutlet weak var btnFavriote : CheckBoxButton!
    @IBOutlet weak var imgVehicles: UIImageView!
    @IBOutlet weak var imgCarFax: UIImageView!
    @IBOutlet weak var lblVinNo: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTrim: UILabel!
    @IBOutlet weak var lblMiles: UILabel!
    @IBOutlet weak var lblStock: UILabel!
    @IBOutlet weak var lblDEal: UILabel!
    @IBOutlet weak var imgDealArrow: UIImageView!
    @IBOutlet weak var dealViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var viewBg: UIView!
    
}

//Mark - Drop down
extension searchViewController : UIPickerViewDelegate ,UIPickerViewDataSource ,UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerviewSort){
            return pickerSortData.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerviewSort){
            let dict = self.pickerSortData[row] as! NSDictionary
            let strname = dict["name"] as! String
            return strname
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerviewSort){
            let dict = self.pickerSortData[row] as! NSDictionary
            self.txtSort.text = (dict["name"] as! String)
            isSortTypeVal = (dict["value"] as! String)
            if(row % 2 == 0){
                if(row == 8){
                    sortDirection = "desc"
                }else{
                    sortDirection = "asc"
                }
            }else{
                if(row == 9){
                    sortDirection = "asc"
                }else{
                    sortDirection = "desc"
                }
            }
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtSort){
            self.pickUp(txtSort)
        }
        if(textField == txtSearch){
            textField.returnKeyType = .search
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txtSearch){
            self.txtSearch.resignFirstResponder()
            searchBarText = self.txtSearch.text!
            searchTag = "0"
            self.callSearchApi(searchTag: searchTag, strCkeck: "get")
            return true
        }
        return false
    }
    
    
    func pickUp(_ textField : UITextField) {
        // UIPickerView
        if(textField == self.txtSort){
            self.pickerviewSort = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewSort.delegate = self
            self.pickerviewSort.dataSource = self
            self.pickerviewSort.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewSort
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(registrationViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(registrationViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtSort.resignFirstResponder()
        
        if(searchTag == "") || (searchTag == "3"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearchApi(searchTag: searchTag, strCkeck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please check your internet connection.")
            }
        }
        else if(searchTag == "0"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearchApi(searchTag: "0", strCkeck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please check your internet connection.")
            }
        }
        else if(searchTag == "1"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearchApi(searchTag: searchTag, strCkeck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please check your internet connection.")
            }
        }
            
        else if(searchTag == "2"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearchApi(searchTag: "2", strCkeck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please check your internet connection.")
            }
        }
        else if(searchTag == "5"){
            if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
                self.callSearcApiwithFilter(strCheck: "get")
            } else {
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Please check your internet connection.")
            }
        }
    }
    
    @objc func cancelClick(){
        txtSort.text = ""
        txtSort.resignFirstResponder()
    }
}

class CheckBoxButton: UIButton {
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(UIImage(named: "starfill"), for: UIControl.State.normal)
            } else {
                self.setImage(UIImage(named: "starempty"), for: UIControl.State.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(buttonClicked(sender:)), for: .touchUpInside)
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            if isChecked == true {
                isChecked = false
            }
            else {
                isChecked = true
            }
        }
    }
}
