//
//  scannerViewController.swift
//  Houston
//  Created by Reinforce on 08/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit
import AVFoundation

class scannerViewController: UIViewController {
        @IBOutlet weak var viewHead : UIView!
    
    var captureSession: AVCaptureSession!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    @IBOutlet weak var qrCodeFrameView: UIView!
    
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.face,
                                      AVMetadataObject.ObjectType.qr]
    
     let upca: AVMetadataObject.ObjectType = .init(rawValue: "org.gs1.UPC-A")
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadSacnner()
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
""        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnHomeLogo(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
    }

}


extension scannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    func loadSacnner(){
        // Get the back-facing camera for capturing videos
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = supportedCodeTypes
        } else {
            failed()
            return
        }
        
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                videoPreviewLayer?.frame = view.layer.bounds
                view.layer.addSublayer(videoPreviewLayer!)
                captureSession.startRunning()
        
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0).cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
       
//        } else {
//            // Fallback on earlier versions
//
//            let captureDevice = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: AVCaptureDevice.Position.back)
//
//            do {
//                // Get an instance of the AVCaptureDeviceInput class using the previous device object.
//                let input = try AVCaptureDeviceInput(device: captureDevice!)
//
//                // Set the input device on the capture session.
//                captureSession.addInput(input)
//
//                // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
//                let captureMetadataOutput = AVCaptureMetadataOutput()
//                captureSession.addOutput(captureMetadataOutput)
//
//                // Set delegate and use the default dispatch queue to execute the call back
//                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
//                captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
//                //            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
//
//            } catch {
//                // If any error occurs, simply print it out and don't continue any more.
//                OperationQueue.main.addOperation {
//                    Globalfunc.hideLoaderView(view: self.view)
//
//                    Globalfunc.print(object: error)
//                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
//                }
//                return
//            }
//
//            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
//            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
//            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
//            videoPreviewLayer?.frame = view.layer.bounds
//            view.layer.addSublayer(videoPreviewLayer!)
//
//            // Start video capture.
//            captureSession.startRunning()
//
//            // Move the message label and top bar to the front
//            view.bringSubviewToFront(viewHead)
//
//            // Initialize QR Code Frame to highlight the QR code
//
//            if let qrCodeFrameView = qrCodeFrameView {
//                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
//                qrCodeFrameView.layer.borderWidth = 2
//                view.addSubview(qrCodeFrameView)
//                view.bringSubviewToFront(qrCodeFrameView)
//            }
//        }
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        UIDevice.vibrate()
        captureSession.stopRunning()
        // Check if the metadataObjects array is not nil and it contains at least one object.
        

        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "No QR code is detected")
            return
        }
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            //qrCodeFrameView?.frame = readableObject.bounds
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
             Globalfunc.print(object: stringValue)
            
            let alert = UIAlertController(title: Constant.AppName, message: "Scan code--\(stringValue)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    let detail = self.storyboard?.instantiateViewController(withIdentifier: "VehicleDetailViewController") as! VehicleDetailViewController
                    detail.vIn = stringValue
                    self.navigationController?.pushViewController(detail, animated: true)
                    
                case .cancel:
                    Globalfunc.print(object: "cancel")
                    
                case .destructive:
                    Globalfunc.print(object: "destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        }

//        // Get the metadata object.
//        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
//
//        if supportedCodeTypes.contains(metadataObj.type) {
//            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
//            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
//            qrCodeFrameView?.frame = barCodeObject!.bounds
//
//            if metadataObj.stringValue != nil {
//                //launchApp(decodedURL: metadataObj.stringValue!)
//               // messageLabel.text = metadataObj.stringValue
//            }
//
//
//            guard !metadataObjects.isEmpty else { return }
//
//            guard
//                let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject,
//                var code = metadataObj.stringValue,
//                supportedCodeTypes.contains(metadataObj.type)
//                else { return }
//
//
//
//
//            var rawType = metadataObj.type.rawValue
//
//            // UPC-A is an EAN-13 barcode with a zero prefix.
//            // See: https://stackoverflow.com/questions/22767584/ios7-barcode-scanner-api-adds-a-zero-to-upca-barcode-format
//            if metadataObj.type == AVMetadataObject.ObjectType.ean13 && code.hasPrefix("0") {
//                code = String(code.dropFirst())
//                rawType = upca.rawValue
//            }
//
//            Globalfunc.print(object: rawType)
        //}
    }
}

