import UIKit
import WebKit


class podiumChatViewController: UIViewController, UIWebViewDelegate {

    
    @IBOutlet weak var wkChatView : WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadForm()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
       }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func loadForm(){
        if let url = URL(string: "https://www.houstondirectauto.com/podium-chat/") {
            let request = URLRequest(url: url)
            wkChatView.load(request)
        }
    }
    
    
    
    
}
