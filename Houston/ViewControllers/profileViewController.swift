//
//  profileViewController.swift
//  Houston
//
//  Created by Reinforce on 08/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class profileViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: RCustomTextField!
    @IBOutlet weak var txtPassword: RCustomTextField!
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var viewForgetPass: UIView!
    @IBOutlet weak var txtForgetEmail: RCustomTextField!
    
    override func viewDidLoad(){
        super.viewDidLoad()
       self.viewForgetPass.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        //txtEmail.text = "aapka.pavan@gmail.com"        //"ravendra@reinforcewebsol.com"
        //txtPassword.text = "02580258"
        let session = userDefault.object(forKey: "session") as? String
        if(session == "" || session == nil){
            self.viewLogin.isHidden = false
        }else{
            let loan = self.storyboard?.instantiateViewController(withIdentifier: "userProfileViewController") as! userProfileViewController
            self.navigationController?.pushViewController(loan, animated: false)
        }
    }
    
    @IBAction func clickOnHomeLogo(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickOnSignInBtn(_ sender : UIButton){
       if(txtEmail.text == "" || txtEmail.text?.count == 0 || txtEmail.text == nil){
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill email field.")
        }
        else if(!(Globalfunc.isValidEmail(testStr: txtEmail.text!))){
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill valid email field.")
        }
        else if(txtPassword.text == "" || txtPassword.text?.count == 0 || txtPassword.text == nil){
            txtPassword.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill password field.")
        }
        else if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
        
        print(Constant.login_Url)
        
            self.callUSerProfileModuleApi(strUrl: Constant.login_Url, strType: "login")
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    @IBAction func clickOnSignUP(_ sender : UIButton){
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "registrationViewController") as! registrationViewController
        self.navigationController?.pushViewController(signUp, animated: true)
        //self.present(signUp, animated: true, completion: nil)
    }
    
    
    @IBAction func clickOnForgotPass(_ sender : UIButton){
        self.viewForgetPass.isHidden = false
    }
  
    @IBAction func clickOnCancekBtn(_ sender : UIButton){
         self.viewForgetPass.isHidden = true
    }
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton){
        if(txtForgetEmail.text == "" || txtForgetEmail.text?.count == 0 || txtForgetEmail.text == nil){
            txtForgetEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill email field.")
        }
        else if (reach.connection != .none || reach.connection == .wifi || reach.connection == .cellular) {
            Globalfunc.showLoaderView(view: self.view)
            self.callUSerProfileModuleApi(strUrl: Constant.forget_password_Url, strType: "forgetpass")
           
        } else {
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    
    //Call Api
    func callUSerProfileModuleApi(strUrl: String, strType: String){
        
        var param = [String : String]()
        if(strType == "login"){
            if let authToken = userDefault.value(forKey: "authToken") {
                param = ["token":"\(authToken)",
                    "email":"\(txtEmail.text!)",
                    "password":"\(txtPassword.text!)"
                ]
            }
        }
        else if(strType == "forgetpass"){
            if let authToken = userDefault.value(forKey: "authToken") {
                param = ["token":"\(authToken)",
                    "email":"\(txtForgetEmail.text!)"
                ]
            }
        }
        webServices.onResponsePostData(url: strUrl, parms: param) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object: dict)
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    let status = dict["status"] as! String
                    
                    if(strType == "login"){
                        if(status == "success"){
                            let userDict = dict["user"] as! NSMutableDictionary
                            Globalfunc.print(object: userDict)
                            userDefault.set("session", forKey: "session")
                            let email = userDict["email"] as! String
                            self.callAuthTokenApii(strEmail: email, strPass: self.txtPassword.text!)
                            do {
                                if #available(iOS 11.0, *) {
                                    let myData = try NSKeyedArchiver.archivedData(withRootObject: userDict, requiringSecureCoding: false)
                                    userDefault.set(myData, forKey: "userinfo")
                                } else {
                                    let myData = NSKeyedArchiver.archivedData(withRootObject: userDict)
                                    userDefault.set(myData, forKey: "userinfo")
                                }
                            } catch {
                                Globalfunc.print(object: "Couldn't write file")
                            }
                            let loan = self.storyboard?.instantiateViewController(withIdentifier: "userProfileViewController") as! userProfileViewController
                            self.navigationController?.pushViewController(loan, animated: true)
                            ///////**************To retirve data************////////////
                            //                            do {
                            //                                let decoded  = userDefault.object(forKey: "userinfo") as! Data
                            //                                if let decodedTeams = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                            //                                    Globalfunc.print(object: decodedTeams)
                            //
                            //                                }
                            //                            } catch {
                            //                                Globalfunc.print(object: "Couldn't read file.")
                            //                            }
                        }
                        else{
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    },{action2 in
                                }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                       
                    }
                        
                    else if(strType == "forgetpass"){
                        if(status == "success"){
                            
                            let msg = dict["msg"] as! String
                            
                            let authToke = dict["token"] as! String
                            userDefault.set(authToke, forKey: "authToken")
                            
                            self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                
                                let reset = self.storyboard?.instantiateViewController(withIdentifier: "resetPassViewController") as! resetPassViewController
                                reset.enail = self.txtForgetEmail.text!
                                self.navigationController?.pushViewController(reset, animated: true)
                                
                                },{action2 in
                                }, nil])
                        }
                        else{
                            Globalfunc.hideLoaderView(view: self.view)
                            let msg = dict["msg"] as! String
                            if(msg == "Invalid or expired authentication token"){
                                self.popupAlert(title: Constant.AppName, message: msg, actionTitles: ["Ok"], actions:[{action1 in
                                    self.callHomeView()
                                    
                                    },{action2 in
                                    }, nil])
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
    
    
    //Auth token
    func callAuthTokenApii(strEmail : String, strPass : String){
        let url =  "\(Constant.Auth_Url)"
        let postString = "username=\(strEmail)&password=\(strPass)"
            // postString = "username=pawan1085&password=123456"
        webServices.callApiRequestforPost(postString, url: url) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object: dict)
                let status = dict["status"] as! String
                if(status == "success"){
                    OperationQueue.main.addOperation {
                        let authToke = dict["token"] as! String
                        userDefault.set(strEmail, forKey: "email")
                        userDefault.set(strPass, forKey: "password")
                        userDefault.set(authToke, forKey: "authToken")
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        let _ = dict["msg"] as! String
                    }
                }
            }else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: error)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "Internal server error.")
                }
            }
        }
    }
}
