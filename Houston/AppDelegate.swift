//
//  AppDelegate.swift
//  Houston
//
//  Created by Reinforce on 08/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit
import Reachability
import SwiftWebSocket
import MessageKit
import IQKeyboardManagerSwift
import UserNotifications

var userDefault = UserDefaults.standard
var reach : Reachability = Reachability()!
var searchBarText = ""
var searchPaymentText = ""
var makeString = ""
var uid : Int = 0
var isOpenChatFirst = ""
var userChatSender =  Sender(id: "1", displayName: "")
var userChatReceiver =  Sender(id: "2", displayName: "")
let socket = WebSocket(Constant.socket_Url)

var searchIndex: Int!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    OperationQueue.main.addOperation {
                        //UIApplication.shared.applicationIconBadgeNumber = 0
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                else{
                    //Do stuff if unsuccessful...
                }
            })
        } else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound];
            let setting = UIUserNotificationSettings(types: type, categories: nil);
            UIApplication.shared.registerUserNotificationSettings(setting);
            UIApplication.shared.registerForRemoteNotifications();
        }
        IQKeyboardManager.shared.enable = true
        UserDefaults.standard.set(true, forKey: "Text Messages")
        readJson()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //self.saveContext()
    }

    
    //Sate list json
    func readJson(){
        do {
            if let file = Bundle.main.url(forResource: "states_titlecase", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    Globalfunc.print(object: object)
                    
                } else if let object = json as? [NSDictionary] {
                    // json is an array
                    Constant.pickerStatesData = object
                } else {
                   Globalfunc.print(object:"JSON is invalid")
                }
            } else {
                Globalfunc.print(object:"no file")
            }
        } catch {
            Globalfunc.print(object: error.localizedDescription)
        }
    }
   
    
    //*************** Push Notification ******************//
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.hexString
        Globalfunc.print(object: deviceTokenString)
         userDefault.set(deviceTokenString, forKey: "device_token")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Globalfunc.print(object: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]){
        Globalfunc.print(object: "userinfo = \(userInfo)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //Globalfunc.print(object: "userinfo = \(userInfo)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    }
}

extension UIApplication {
    class func isFirstLaunch() -> Bool {
        if !UserDefaults.standard.bool(forKey: "HasLaunchedOnce"){
            UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
            UserDefaults.standard.synchronize()
            return true
        }
        return false
    }
}
