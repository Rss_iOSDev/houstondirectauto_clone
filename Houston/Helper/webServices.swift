//
//  webServices.swift
//  barCodeSanner
//
//  Created by Reinforce on 08/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import Foundation


class webServices {
    
    static func callApiRequestforPost(_ parameters: String , url : String, completionHandler: @escaping (_ result: NSDictionary, _ error: String) -> Void){
        //.. Code process
        
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = parameters
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                Globalfunc.print(object: "error=\(String(describing: error))")
                completionHandler(["":""], "\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                Globalfunc.print(object: "statusCode should be 200, but is \(httpStatus.statusCode)")
                Globalfunc.print(object: "response = \(String(describing: response))")
                completionHandler(["":""], "Status_Not_200")
                return
            }
            
            if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any]{
                completionHandler(parsedData as NSDictionary , "")
            }
            else{
                OperationQueue.main.addOperation {
                    Globalfunc.print(object: "false")
                }
            }
        }
        task.resume()
        
    }
    
    static func callApiRequestForGet(url : String, completionHandler: @escaping (_ result: NSDictionary, _ error: String) -> Void){
        
        var searchURL = NSURL()
        
        if let url = NSURL(string: "\(url)")
        {
            searchURL = url
        } else {
            
            let Nurl : NSString = url as NSString
            Globalfunc.print(object: Nurl)
            
            let urlStr : NSString = Nurl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            searchURL = NSURL(string: urlStr as String)!
            Globalfunc.print(object: searchURL)
        }
        
        
        
        
        var request = URLRequest(url: searchURL as URL)
        
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
            if error == nil && data != nil {
                do {
                    // Convert NSData to Dictionary where keys are of type String, and values are of any type
                    let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
                    
                    //do your stuff
                    
                    completionHandler(json as NSDictionary, "")
                    
                } catch {
                    completionHandler(["":""], "Status_Not_200")
                }
            }
            else if error != nil
            {
               completionHandler(["":""], "error")
            }
        }).resume()
        
        
        
    }
    
    
    
    
    static func onResponsePostData(url: String,parms: [String:String], completion: @escaping (_ res:[String:Any] , _ error : String) -> Void){
        
        let url: URL = URL(string: url)!
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = [
            "Accept" : "application/json",
            "Content-Type" : "application/x-www-form-urlencoded"
        ]
        let session = URLSession(configuration: config)
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

        request.encodeParameters(parameters: parms)
        
         let task = session.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                Globalfunc.print(object: "error=\(String(describing: error))")
                completion(["":""], "\(String(describing: error))")
                return
            }
            do {
                guard let json = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    Globalfunc.print(object: "Not containing JSON")
                    return
                }
                completion(json , "")
            }
        }
        task.resume()

    }
    
    
    
  
    
    
//    static func onResponsePostwithDictParam(url: String,parms: [String:String], completion: @escaping (_ res:[String:String] , _ error : String) -> Void) {
//
//        let configuration = URLSessionConfiguration.default
//        let session = URLSession(configuration: configuration)
//        let url = NSURL(string:"\(url)")
//        let request = NSMutableURLRequest(url: url! as URL)
//        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        request.httpMethod = "POST"
//
//        do {
//            request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
//
//            let task = session.dataTask(with: request as URLRequest) {
//                data, response, error in
//
//                if let httpResponse = response as? HTTPURLResponse {
//                    if httpResponse.statusCode != 200 {
//                        Globalfunc.print(object: "response was not 200: \(String(describing: response))")
//                        completion(["":""], "Status_Not_200")
//                        return
//                    }
//                }
//                if (error != nil) {
//                    Globalfunc.print(object: "error submitting request: \(String(describing: error))")
//                    completion(["":""], "\(String(describing: error))")
//                    return
//                }
//                do {
//                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? [String:String]
//                    Globalfunc.print(object: "result = \(String(describing: result))")
//                    completion(result! , "")
//                }
//                catch
//                {
//                }
//            }
//            task.resume()
//        }
//        catch
//        {
//        }
//    }
    
    
    static func createBodyWithParameters(parameters: [String:Any], imageData: [String], boundary: String) -> NSData {
        
        let body = NSMutableData();
        //if parameters != nil {
        for (key, value) in parameters {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
        }
        //}
        
        var i : Int = 0
        Globalfunc.print(object: imageData.count)
        for val in imageData {
            if !imageData[i].isEmpty {
                
                
                let name = "make[]"
                //define the data post parameter
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append("hi\r\n".data(using: String.Encoding.utf8)!)
                
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"\(name)\"; filename=\"\(val)\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
                i = i + 1
            }
        }
        return body
    }
    
     static func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
}


extension URLRequest {
    
    private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string
            .addingPercentEncoding(withAllowedCharacters: characterSet)!
            .replacingOccurrences(of: " ", with: "+")
            .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
    
    mutating func encodeParameters(parameters: [String : String]) {
        httpMethod = "POST"
        
        let parameterArray = parameters.map { (arg) -> String in
            let (key, value) = arg
            return "\(key)=\(self.percentEscapeString(value))"
        }
        
        httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
    }
}
