//
//  Constant.swift
//  Houston
//
//  Created by Reinforce on 16/02/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import Foundation

class Constant{
    
    static let deviceT = "6e134fad148d35d03330ccd08f4ee8bd5d2a73189ae41889907a4c1430774bd8"
    //Api list
    static let BaseURl = "http://www.houstondirectauto.com/api/"
    
    static let form_BaseURl = "http://houstondirectauto.com/wp-admin/admin-ajax.php"
    
    static let Auth_Url = "\(Constant.BaseURl)auth_token"
    
    static let teamList_Url = "\(Constant.BaseURl)hda/team"
    
    
    static let makelsList_Url = "\(Constant.BaseURl)hda/makes"
    
    static let login_Url = "\(Constant.BaseURl)hda/login"

    static let registration_Url = "\(Constant.BaseURl)hda/registration"  //token={token}
    
    static let modelsList_Url = "\(Constant.BaseURl)hda/models"
    
    static let scanVin_Url = "\(Constant.BaseURl)hda/veh"   //token={token}&vin={vin or stock}
    
    static let search_Url = "\(Constant.BaseURl)hda/search"
    
    static let vehicle_details_Url = "\(Constant.BaseURl)hda/veh"
    
    static let seriesList_Url = "\(Constant.BaseURl)hda/series"
    
    static let body_stylesList_Url = "\(Constant.BaseURl)hda/body_styles"
    
    static let exterior_colorsList_Url = "\(Constant.BaseURl)hda/exterior_colors"
    
    static let interior_colorsList_Url = "\(Constant.BaseURl)hda/interior_colors"
    
    static let enginesList_Url = "\(Constant.BaseURl)hda/engines"
    
    static let transmissionsList_Url = "\(Constant.BaseURl)hda/transmissions"
    
    static let customer_loans_Url = "\(Constant.BaseURl)hdapay/customer_loans"
    
    static let delete_loan_Url = "\(Constant.BaseURl)hdapay/delete_loan"
    
    static let reg_loan_Url = "\(Constant.BaseURl)hdapay/reg_loan"
    
    static let search_by_loan_acc_Url = "\(Constant.BaseURl)hdapay/search_by_loan_acc"
    
    static let payment_setting = "\(Constant.BaseURl)hdapay/payment_setting"
    
    static let save_payment_method_Url = "\(Constant.BaseURl)hdapay/save_payment_method"
    
    static let user_payments_Url = "\(Constant.BaseURl)hdapay/user_payments"
    
    static let process_payment_Url = "\(Constant.BaseURl)hdapay/process_payment"
    
    static let change_password_Url = "\(Constant.BaseURl)hda/change_password"
    
    static let update_profile_Url = "\(Constant.BaseURl)hda/update_profile"
    
    static let veh_near_me_Url = "\(Constant.BaseURl)hda/veh_near_me"
    
    static let forget_password_Url = "\(Constant.BaseURl)hda/forget_password"    //params: token, email
    
    static let reset_password_Url = "\(Constant.BaseURl)hda/reset_password"
    
    static let financeCalc_URL = "https://www.houstondirectauto.com/finance-calculator/"
    
    
    
    static let fuelsList_Url = "\(Constant.BaseURl)hda/fuels"
    
    
    static let forms_Url = "\(Constant.form_BaseURl)"
    
    static let Chat_BaseURl = "http://houstondirectauto.com/hdapay/hda/api/"
    
    static let start_chat = "\(Constant.Chat_BaseURl)start_chat"
    
    static let URL_chats_by_uid = "\(Constant.Chat_BaseURl)chats_by_uid"
    
    static let URL_chat_send = "\(Constant.Chat_BaseURl)chat_send"
    
    static let socket_Url = "wss://houstondirectauto.com/hdapchat"
    
    
    //favorites Api
    
    static let favorite_List_Url = "\(Constant.BaseURl)hda/favourite_vehicles"   //params: token, email
    
    static let Add_favorite_Url = "\(Constant.BaseURl)hda/favourite_vehicles/add"  //params: token, email, vin
    
    static let Remove_favorite_Url = "\(Constant.BaseURl)hda/favourite_vehicles/remove"  //params: token, email, vin
    
    //Array and variables
    static let AppName = "Houston"
    static let username = "hdamobileapi"
    static let password = "02580258"
    
    static var pickerMakeList = [String]()
    static var arrSeries = [String]()
    static var dataBStyle = [String]()
   
    static var arrInteriorColor = [String]()
    static var arrExteriorColor = [String]()
    
    static var dataEngine = [String]()
    static var dataTransmission = [String]()
    
    
    
    static var pickerPaymentList = ["$200/more or less",
                                     "$300/more or less",
                                     "$400/more or less",
                                     "$500/more or less",
                                     "$600/more or less",
                                     "$700/more or less",
                                     "$800/more or less",
                                     "$900/more or less",
                                     "1000/more or less",
                                     "$1100/more or less",
                                     "$1200/more or less",
                                     "$1300/more or less"]
    
    
    static var pickerStatesData = [NSDictionary]()
    
    static var pickerPaymentMthod = ["Credit Card"]
    
    static var pickerBankAccType = ["savings",
                                    "Checking",
                                    "Business Checking"]
    
}
